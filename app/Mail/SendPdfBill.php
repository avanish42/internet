<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendPdfBill extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $data;
    public function __construct($data)
    {
        //
        $this->data= $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $client= $this->data;
        return $this->markdown('emails.billgenerate',compact('client'))->attach($this->data->bill_path, [
            'as' =>"invoice-" .$this->data->customer_id.'.pdf',
            'mime' => 'application/pdf',
        ])->subject(date('M').'Bill');
    }

}

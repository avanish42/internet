<?php

namespace App;

use App\Models\Amount;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use App\Models\Zone;

class User extends Authenticatable
{
    use Notifiable,EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function zones()
    {
        return $this->belongsToMany(Zone::class, 'zone_users', 'user_id', 'zone_id');
    }

    public function payment()
    {
        return $this->hasMany(Amount::class,'exicutive_id','id');
    }
}

<?php

namespace App\Http\Controllers\SubAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Models\Zone;
use App\Models\Amount;
use App\Models\GenerateBill;
use Illuminate\Support\Facades\Auth;
use App\Models\ZoneUser;
use App\Models\PayBill;
use Response;


class   BillingController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function searchingPayment()
    {


         $data=ZoneUser::where('user_id',Auth::user()->id)->get();

            $zone_ids= [];
            foreach ($data as $k =>$v)

            {
                array_push($zone_ids,$v->zone_id);
            }





        $clients= Client::with('amount.generatebill')->with('plan')->whereIn('zone_id',$zone_ids)->get();


//        return Response::json($clients);

        foreach ($clients as $k=>$v)
        {
            $outstanding=$this->findOutstanding($v->amount);
            $clients[$k]->outstanding=$outstanding;

            if($outstanding==0)
            {
                unset($clients[$k]);
            }
            //  $timestamp =PayBill::where('client_id',$v->id)->orderBy('created_at', 'desc')->first();

//            dd($timestamp);
//                        if($timestamp)
//            {
//                $clients[$k]->paytime= $timestamp->created_at;
//            }

        }

        $total = PayBill::where('exicutive_id',Auth::id())->where('approved',0)->get()->sum('pay_amount');
//        return Response::json($clients);
        return view('admin.payment.dopayment',compact('clients','total'));


    }

    public function findOutstanding($amount_data)

    {
        $total_credit=$amount_data->sum('credit');
        $total_debit=$amount_data->sum('debit');

        return ($total_credit-$total_debit);

    }


    public function PaymentProcess()
    {


    }
}

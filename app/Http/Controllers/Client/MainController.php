<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MainController extends Controller
{
    //

    public function index()
    {
        return view('client.home');

    }
    public function aboutUs()
    {

            return view('client.about');
    }

    public function contactUs()
    {
        return view('client.contect');

    }

    public function quickRecharge()
    {
        return view('client.recharge');

    }

    public function complain()
        {
            return view('client.complain');
        }
}

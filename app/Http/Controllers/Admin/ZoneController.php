<?php

namespace App\Http\Controllers\Admin;

use App\Models\Zone;
use Carbon\Carbon;
//use http\Env\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Response;


class ZoneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        //
        $data= Zone::get();
      // 1 dd($data->toArray());

        return view('admin.zone_master',compact('data'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {



    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
                Zone::create($request->all());

                return back()->with('status','100')->with('message','Zone added successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }


    public function getModelDataZone(Request $request)
    {
//        dd();
//        print_r();

        $data=Zone::find($request->id);


        return Response::json($data);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateZone(Request $request)
    {
        //
      //  dd($request->toArray());

        $temp= $request->toArray();

        unset($temp['_token']);

        Zone::where('id',$request->id)->update($temp);

        return redirect('zone');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

//        dd($id);
        $zone=Zone::find($id);

            $zone->delete();


            return back();
    }
}

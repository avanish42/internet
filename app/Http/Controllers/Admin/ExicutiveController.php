<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Plan;
use App\Models\Area;
use App\Models\Zone;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Models\ZoneUser;

use App\Models\Role;




class ExicutiveController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

//    use RegistersUsers;

    public function allExicutive()
    {

       // $users= User::hasRole('exicutive')->all();
//        $employee=User::with('roles')->get()->toArray();
        $users = User::with('zones')->get();
//        dd($users->toArray());

        return view('admin.exicutive.allexicutive',compact('users'));

    }

    public function addExicutive(Request $request)
    {
//        dd(Auth::user()->role);

        $zones= Zone::with('area')->get();
        $role= DB::table('roles')->where('name','exicutive')->first();



        return view('admin.exicutive.addexicutive',compact('zones','role'));

    }

    public function storeExicutive( Request $request)
    {

//        dd($request->toArray());

        $user_data=$this->create($request->all());
       // dd($user_data);

//        DB::table('role_user')->insert([
//            ['user_id' => $user_data->id, 'role_id' => $request->role_id]
//
//        ]);

        foreach ($request->zone_ids as $k=>$v)
        {
            DB::table('zone_users')->insert([
                ['user_id' => $user_data->id, 'zone_id' => $v]

            ]);
        }


        return back()->with('status','100')->with('message','add Successfully');




    }

    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'role'=>0,
        ]);
    }

    protected function createroleuser( $data)
    {
            if ($data=='admin');
        {
            DB::table('users')->insert([
            ['user_id' => "a", 'role_id' => 0]

            ]);
        }
    }

    public  function editExicutive($id)
    {

    }

    public function updateExicutive(Request $request)
    {
        dd($request->toArray());

    }

    public function deleteExicutive($id)
    {
//        dd($id);
         User::where('id',$id)->delete();

        return back()->with('status',400)->with('message','Delete Successfully');


    }

    public function changePAssword($id)
    {

            $exicutive_data= User::find($id);
            return view('admin.exicutive.changepasswod',compact('exicutive_data'));


    }

    public function updatePassword( Request $request)
    {
       //     dd($request->toArray());

            User::where('id', $request->id)->update(['password' => Hash::make($request->password)]);
            return redirect('exicutive/all-exicutive')->with('status',100)->with('message','Password Change Successfully');
    }




}

<?php

namespace App\Http\Controllers\Admin;

use App\Models\Client;
use App\Models\Plan;
use App\Models\Zone;
use App\Models\Area;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//use Maatwebsite\Excel\Excel;
use Maatwebsite\Excel\Facades\Excel;

class SheetUpload extends Controller
{
    //

    public function showsheet()
    {
        return view('admin.sheetupload');
    }


    public function sheetUpload(Request $request)
    {
        $temp_data = $request->all();
        if ($request->hasFile('client_sheet')) {

            $path = $request->file('client_sheet')->getRealPath();
            $data = Excel::load($path, function ($reader) {
            })->get();
            //echo "<pre>";
           // print_r($data[0]);
            //die();


            foreach ($data->toArray() as $k => $v) {


                $client = new Client();

//                print_r($v);
//                die();
                if($v['zone']=='' || $v['area']=='')
                {
                    continue;
                }
                $zone= Zone::firstOrCreate(['name' => $v['zone']], ['price' => $v['price']]);

                $area= Area::firstOrCreate(['name' => $v['area']], ['zone_id' => $zone->id]);



                $plan= Plan::firstOrCreate(['name' => $v['plan']], ['price' => $v['price']], []);

                $client->zone_id= $zone->id;
                $client->area_id= $area->id;
                $client->plan_id = $plan->id;

                // dd($client->plan_id);
                $client->name = $v['name'];
                $client->customer_id = $v['cust_id'];
                $client->card_id = $v['card_no'];
                $client->user_id = $v['user_id'];
                $client->street = $v['billing_address'];
                $client->mobile = $v['ph_no1'] ? $v['ph_no1'] : "0000";
                $client->is_gst_in_plan = $v['gst_include'];
                $client->auto_bill = $v['bill'];
                $client->discount = $v['discount'];
                $client->email = $v['mail_id'] ;
//                $client->is_gst_in_plan = $v['gst_include'];
//                $client->auto_bill=$v['bill'];


                $client->save();
               // break;


            }

            return redirect('all-clients');


        }
    }


    public function clientDataUpdateBySheet(Request $request)
    {

        $temp_data = $request->all();
        if ($request->hasFile('client_sheet_update')) {

            $path = $request->file('client_sheet_update')->getRealPath();
            $data = Excel::load($path, function ($reader) {
            })->get();
             echo "<pre>";

            foreach ($data[0]->toArray() as $k => $v) {

             //  print_r($v);
                $client = Client::where('customer_id', $v['cust_id'])
                    ->update(['card_id' => $v['card_no'], 'is_gst_in_plan' => $v['gst_include'], 'is_pdf' =>
                        $v['bill']]);
               // print_r($client);

            }

        }
    }

    public function areaUpdate(Request $request)
    {

        if ($request->hasFile('client_sheet_area')) {

            $path = $request->file('client_sheet_area')->getRealPath();
            $data = Excel::load($path, function ($reader) {
            })->get();

            echo "<pre>";
            print_r($data->toArray());
            die();


        }
    }
}

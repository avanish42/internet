<?php

namespace App\Http\Controllers\Admin;

use App\Models\Area;
use App\Models\Zone;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Response;

class AreaController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $users= User::get();
        $zones= Zone::get();
        $data= Area::with('zone')->get();
        return view('admin.area_master', compact('users','zones','data'));
    }

    public  function  saveArea(Request $request)
    {
        Area::create($request->all());
        return back()->with('status','100')->with('message','Area Added successfuly ');
    }
    public function editArea(Request $request)
    {
        $data=Area::find($request->id);
        return Response::json($data);

    }
    public function updateArea(Request $request)
    {

        $temp= $request->toArray();

        unset($temp['_token']);

        Area::where('id',$request->id)->update($temp);

        return redirect('area');
    }
    public function deleteAera($id)
    {
        $area= Area::find($id);
        $area->delete();
        return back();
    }
}

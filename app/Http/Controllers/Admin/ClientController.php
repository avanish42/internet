<?php

namespace App\Http\Controllers\Admin;

use App\Models\Amount;
use App\Models\Area;
//use http\Env\Response;
use App\Models\Client;
use App\Models\GenerateBill;
use App\Models\Plan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Zone;
use Response;


class ClientController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function addClient()
    {
        $plans= Plan::get();
        $zones= Zone::get();
        return view('admin/client/addclient', compact('plans','zones'));

    }
    public function storeClient(Request $request)
    {
//            dd($request->toArray());

            Client::create($request->all());
            return back();

    }

    public function editClient($id)
    {



        $data=Client::with(['plan','zone','area'])->find($id);
        $plans= Plan::get();
        $zones= Zone::get();


        return view('admin.client.editclient',compact('data','plans','zones'));

    }
    public function updateClient(Request $request)
    {
        $temp =$request->toArray();

//        dd($temp);
        unset($temp['_token']);
        $temp['is_gst']= isset($temp['is_gst'])?1:0;
        $temp['is_pdf']= isset($temp['is_pdf'])?1:0;
        $temp['auto_bill']= isset($temp['auto_bill'])?1:0;
        $temp['is_igst']= isset($temp['is_igst'])?1:0;
        $temp['is_gst_in_plan']= isset($temp['is_gst_in_plan'])?1:0;



//        dd($request->toArray());
        Client::where('id', $request->id)
//            ->where('destination', 'San Diego')
            ->update($temp);
//        $client_data=Client::find($request->id);
//
//            $client_data->area_id=$temp['area_id'];
//            $client_data->zone_id=$temp['zone_id'];
//            $client_data->save();
//        return back();

        echo '<script>  window.close();</script>';


//        if (confirm('Are You Sure?')){
//            window.location = "http://www.google.com/";
//        }else{
//            alert("You are not redirected.")
//}



    }

    public function ajaxArea(Request $request)

    {

            $data= Area::where('zone_id',$request->id)->get();
            return Response::json($data);

    }

    public function allClient()
    {
       // $clients= Client::with('plan')->with('area.zone')->get();
//        dd($clients->toJson());
//         return Response::json($clients);
        return view('admin/client/allclient',compact('clients'));


    }

    public function clientBills()
    {
        $data= Client::with('generatedBill')->with('payBill')->get();
//      dd($data->toArray());
        return view('admin.client.bills');
    }

    public function  clientProfile($id)
    {
        $data=  Client::where('id',$id)->with('generatedBill')->with('payBill')->with('plan')->first();
        //dd($data->toArray());

        $payment= Amount::with('exicutive')->with('paybill')->where('client_id',$id)->get();
//        dd($payment->toArray());

        $bills= GenerateBill::with('exicutive')->where('amount','!=',0)->where('client_id',$id)->get();

        $pay = Amount::where('client_id',$id)->get()->sum('credit');
        $debit = Amount::where('client_id',$id)->get()->sum('debit');



      //  dd($amount);


        return view('admin.client.profile',compact('data','payment','bills','pay','debit'));



    }

    public function changeStatus($id)
    {

        $status=Client::where('id',$id)->first()->customer_status;

        Client::where('id',$id)
            ->update(['customer_status'=>!($status)]);
        return back()->with('status',100)->with('message','Status changed Successfully');

    }

    public function filterClient(Request $request)
    {

        $clients=array();

          $area= Area::get();
          $zones= Zone::get();

          if (isset($request->zone_id))
          {
//                    $clients= Client::with(['plan','area'])
//                        ->with(['zone'=>function ($query){
//                           $query->where('id',$request->zone_id);
//          }])->get();

              $clients= Client::with(['plan','area','zone'])->where('zone_id',$request->zone_id)->get();
                    dd($clients);

          }

        if (isset($request->area_id)) {

          }




            return view('admin.client.filterclient',compact('clients','area','zones'));


    }

    public function CheckCustomerId(Request $request)
    {
              //  dd($request->toArray());

        if(isset($request->client_id))
        {

            $client =Client::where('customer_id',$request->id)
                    ->where('id','!=',$request->client_id)
                    ->get();
        }
        else
        {
            $client =Client::where('customer_id',$request->id)->get();

        }

                return Response::json($client);


    }

    public  function CheckCardtId(Request $request)
    {

        if(isset($request->client_id)) {
            $client = Client::where('card_id', $request->id)
                ->where('id','!=',$request->client_id)
                ->get();
        }
        else
        {

            $client = Client::where('card_id', $request->id)->get();
        }

        return Response::json($client);

    }

    public function getAllClientForDatatable()
    {
        return datatables()->of(Client::query())
            ->addColumn('Action', 'admin.client.datatable.action')
            ->rawColumns(['Action'])
            ->make(true);
    }





}

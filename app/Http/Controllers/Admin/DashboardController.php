<?php

namespace App\Http\Controllers\Admin;
use App\Models\Amount;
use App\Models\Client;
use App\Models\GenerateBill;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Models\Zone;
use App\User;
use Response;

class DashboardController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $lastmonthdate=(Carbon::now()->subMonth()->endOfMonth()->toDateString());
        $lastmonth=date('m',strtotime(Carbon::now()->subMonth()->endOfMonth()->toDateString()));


        $dashboard_result=array();

        $total_client_till_current_month= Client::all();
        $total_active_client_till_current_month= Client::where('customer_status',1)->with('plan')->get();
        $total_client_till_last_month= Client:: whereDate('created_at', '<=',  $lastmonthdate)->get();
        $total_active_client_till_last_month= Client:: whereDate('created_at', '<=',  $lastmonthdate)->where('customer_status',1)->get();

        $total_suspended_client_till_current_month= Client::where('customer_status',0)->get();
        $total_suspended_client_till_last_month= Client:: whereDate('created_at', '<=', $lastmonthdate)->where('customer_status',0)->get();


        $this_month_new_user= Client::whereMonth('created_at',date('m'));
        $last_month_new_user= Client::whereMonth('created_at',$lastmonth);

            $total_payment_as_per_db=0;
        foreach ($total_active_client_till_current_month as $k =>$v)
        {
            if (is_numeric($v->plan->price)) {


                $total_payment_as_per_db = $v->plan->price + $total_payment_as_per_db;
            }
        }

          $total_payment_as_per_bill=GenerateBill::whereMonth('created_at',date('m'))->get();
          $total_payment_receive=Amount::whereMonth('created_at',date('m'))->get();













        $dashboard_result['total_user_till_current_month']=$total_client_till_current_month->count();
        $dashboard_result['total_user_till_last_month']=$total_client_till_last_month->count();

        $dashboard_result['total_active_client_till_current_month']=$total_active_client_till_current_month->count();
        $dashboard_result['total_active_client_till_last_month']=$total_active_client_till_last_month->count();

        $dashboard_result['total_suspended_client_till_current_month']=$total_suspended_client_till_current_month->count();
        $dashboard_result['total_suspended_client_till_last_month']=$total_suspended_client_till_last_month->count();

        $dashboard_result['this_month_new_user']=$this_month_new_user->count();
        $dashboard_result['last_month_new_user']=$last_month_new_user->count();

        $dashboard_result['total_payment_as_per_db']=$total_payment_as_per_db;
        $dashboard_result['total_payment_as_per_bill']=$total_payment_as_per_bill->sum('amount');
        $dashboard_result['total_payment_receive']=$total_payment_receive->sum('credit');


        $dashboard_result['remaining_payment']=$dashboard_result['total_payment_as_per_db']-$dashboard_result['total_payment_receive'];


        $dashboard_result['all_zone']=Zone::with('client.plan')->with('client.amount')->with('client.generatedBill')->get();

//            return Response::json($dashboard_result['all_zone']);

        foreach ( $dashboard_result['all_zone'] as $k =>$v)
        {
//                        $dashboard_result['all_zone'][$k]['total']= $v->client->plan->price->sum();
                            $sum= 0;
                        $tortal_generate = 0;
                        $total_paid=0;
                        foreach ($v->client as $key=>$value)
                        {
                            $sum = $sum +$value->plan->price;

                            if(isset($value->amount) && !empty($value->amount)){


                            foreach ($value->amount as $k_amount =>$v_amount)
                                {
                                    if(Carbon::now()->month ==Carbon::parse($v_amount->created_at)->format('m')) {

                                        $total_paid = $total_paid + $v_amount->credit;
                                        $tortal_generate = $tortal_generate + $v_amount->debit;
                                    }
                                }
                            }

//                            if(isset($value->generated_bill) && !empty($value->generated_bill)) {
//
//                                foreach ($value->generated_bill as $k_generate => $v_generate) {
//                                    $tortal_generate= $tortal_generate+$v_generate->amount;
//
//                                }
//                            }


                        }


                  $dashboard_result['all_zone'][$k]['total']=$sum;
                  $dashboard_result['all_zone'][$k]['total_generate']=$tortal_generate;
                  $dashboard_result['all_zone'][$k]['total_paid']=$total_paid;
                  $dashboard_result['all_zone'][$k]['total_remaining']=$tortal_generate-$total_paid;
        }

        $allexictive_payment= User::with('payment')->where('role',0)->get();

//        dd($allexictive_payment->toArray());

        return view('admin.dashboard',compact('dashboard_result'));

    }

    public function dashboard()
    {
        return view('admin.dashboard-new');
    }




}

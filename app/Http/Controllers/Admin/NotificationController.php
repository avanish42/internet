<?php

namespace App\Http\Controllers\Admin;

use App\Models\Area;
use App\Models\Client;
use App\Models\Zone;
use App\Services\SmsService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mail;
use App\Mail\SendNotification;

class NotificationController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }


    public function selectUser()
    {

        $areas = Area::all();
        $zones = Zone::all();
        $clients = array();


        return view('admin.notification.selectuser', compact('areas', 'zones', 'clients'));
    }

    public function filteredClientsByArea(Request $request)
    {


        if (isset($request->area_id)) {
            $clients = Client::where('area_id', $request->area_id)->get();

        } elseif ($request->zone_id) {
            $clients = Client::where('zone_id', $request->zone_id)->get();

        }
        $areas = Area::all();
        $zones = Zone::all();

        return view('admin.notification.selectuser', compact('areas', 'zones', 'clients'))->with('status', 100)->with('message', 'Find Clients');


    }


    public function sendAreaNotification(Request $request)
    {


//        dd($request->toArray());

        foreach ($request->client_ids as $k => $v) {
            $client_data = Client::find($v);

            if (isset($request->sms_checkbox) && ($request->sms_checkbox == 1)) {
                SmsService::sendSms($request->message, $client_data->mobile);

            }

            if (isset($request->email_checkbox) && ($request->email_checkbox == 1)) {


                if ($client_data->email) {
                    $client_data->title = $request->title;
                    $client_data->message = $request->message;
                    Mail::send(new SendNotification($client_data));
                }
            }



        }
        return redirect('notification/select-user')->with('status', 100)->with('message', 'Sent Successfully');

    }

}

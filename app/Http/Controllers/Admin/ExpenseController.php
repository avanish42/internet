<?php

namespace App\Http\Controllers\Admin;

use App\Models\Expense;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;


class ExpenseController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){

        $result= Expense::with('exicutive')->get();

       // dd($result->toArray());

        return view('admin.expense.add',compact('result','request'));
    }
    public function storeExpense(Request $request)
    {
//        dd($request->toArray());
        $temp= $request->toArray();
        unset($temp['_token']);
        $temp['exicutive_id']=Auth::id();
        Expense::create($temp);
        return back()->with('status',400)->with('message','add Successfully');

    }

    public function deleteExpense($id)
    {
        Expense::destroy($id);
            return back();

    }
}

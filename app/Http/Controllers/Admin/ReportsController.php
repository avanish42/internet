<?php

namespace App\Http\Controllers\Admin;

use App\Models\Amount;
use App\Models\Client;
use App\Models\Expense;
use App\Models\GenerateBill;
use App\Models\Zone;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use  Carbon\Carbon;
use Response;

class ReportsController extends Controller
{
    //

    public function userReport()
    {
       // dd($request->toArray());
        $zones =Zone::all();

        $users =Client::with('plan')->with('zone')->with('area')->get();


        return view('admin.Reports.userreports',compact('zones','users'));

    }

    public function getUserReport(Request $request)
    {
            //dd($request->toArray());
            if (isset($request->zone_id))
            {

            }
            if (isset($request->area_id))
            {

            }


    }
//
//    public function generateUserReport(Request $request)
//    {
//        dd($request->toArray());

//
//
//    }

    public function billingReport()
    {

        return view('admin.Reports.bilingreport');

    }
    public  function generateBillingReport(Request $request)
    {
       // dd($request->toArray());

        $from= Carbon::parse($request->from);
        $to= Carbon::parse($request->to);


       $data= GenerateBill::whereBetween('bill_date', [$from, $to])->with('client.plan')->get();


//       return Response::json($data);



        return view('admin.Reports.bilingreport',compact('data'));


    }

    public function expenceReport()
    {
        return view('admin.Reports.expencereport');

    }

    public function generateExpenceReport(Request $request)
    {
        $from= Carbon::parse($request->from);
        $to= Carbon::parse($request->to);
        $data= Expense::whereBetween('created_at', [$from, $to])->with('exicutive')->get();
        $expence= Expense::whereBetween('created_at', [$from, $to])->with('exicutive')->get()->sum('amount');


//        return Response::json($data);

        return view('admin.Reports.expencereport',compact('data','expence'));




    }

    public function paymentReport()
    {
        return view('admin.Reports.paymentreport');

    }

    public function generatePaymentReport(Request $request)
    {
        $from= Carbon::parse($request->from);
        $to= Carbon::parse($request->to);
        $data= Amount::whereBetween('created_at', [$from, $to])->with('exicutive')->whereNotNull('credit')->get();
        $total= Amount::whereBetween('created_at', [$from, $to])->with('exicutive')->whereNotNull('credit')->get()
            ->sum('credit');


//        return Response::json($total);

        return view('admin.Reports.paymentreport',compact('data','total'));




    }

}

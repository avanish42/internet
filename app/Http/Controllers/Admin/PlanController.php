<?php

namespace App\Http\Controllers\Admin;

use App\Models\Plan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Response;

class PlanController extends Controller
{

    public  function index()
    {
        $data= Plan::get();
        return view('admin.plan_master',compact('data'));
    }

    public function savePlan(Request $request)
    {
      //  dd($request->toArray());
        Plan::create($request->all());
        return back()->with('status','100')->with('message','Plan Added successfuly ');

    }
    public function getModelDataPlan(Request $request)
    {
        $data=Plan::find($request->id);


        return Response::json($data);

    }

    public function updatePlan(Request $request)
    {
        //dd($request);
        $temp= $request->toArray();

        unset($temp['_token']);

        Plan::where('id',$request->id)->update($temp);

        return redirect('plan')->with('status','100')->with('message','Update Successfully');

    }

    public function deletePlan($id)
    {
            $plan=Plan::find($id);


            $plan->delete();
            return back();
    }


}


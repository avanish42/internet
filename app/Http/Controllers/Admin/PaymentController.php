<?php

namespace App\Http\Controllers\Admin;

use App\Models\Amount;
use App\Models\Client;
use App\Models\PayBill;
use App\Services\SmsService;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Appnings\Payment\Facades\Payment;
use App\Notifications\GenerateBill;
use Response;
use Carbon\Carbon;


class PaymentController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function paymentPage()
    {

    return view('admin.payment.payment');
    }

    public function paymentProcess(Request $request)
    {


        $parameters = [

            'tid' => '1233221223322',

            'order_id' => '1232212',

            'amount' => '1200.00',

        ];

        $order = Payment::prepare($parameters);
        return Payment::process($order);

    }
    public function responsePage(Request $request)
    {
        $response = Indipay::response($request);
        dd($response);
    }

    public function canclePage(Request $request)
    {
        $cancle= Indipay::response($request);

        dd($cancle);

    }
    public function payByWeb()
    {
        $clients= Client::with('amount.generatebill')->with('plan')->get();

        foreach ($clients as $k=>$v)
        {
            $outstanding=$this->findOutstanding($v->amount);
            $clients[$k]->outstanding=$outstanding;

            if($outstanding==0)
            {
                unset($clients[$k]);
            }
        }
        return view('admin.payment.dopayment',compact('clients'));
    }

    public function paymentReminder(Request $request)
    {

        $clients= Client::with('amount.generatebill')->with('plan')->get();

        foreach ($clients as $k=>$v)
        {
            $outstanding=$this->findOutstanding($v->amount);
            $clients[$k]->outstanding=$outstanding;

            if($outstanding==0)
            {
                unset($clients[$k]);
            }
        }

        return view('admin.payment.payment-reminder',compact('clients'));

    }

    public function sendPaymentReminder(Request $request)
    {
        $clients =Client::whereIn('id',$request->client_ids )->get();
        foreach ($clients as $k => $v )
        {
            echo $v->mobile;
            SmsService::sendSms($request->message,$v->mobile);
        }

        return back()->with('status',100)->with('message','Message Sent Successfully');

    }



    public function processPaymentByWeb( Request $request)
    {
//        dd($request->toArray());
             //echo Auth::id();
             //$temp=array();
           // $temp= $request->toArray();
//            $temp['client_id']=$request->client_id;
//            $temp['exicutive_id']=Auth::id();
//            $temp['credit']=$request->credit;
//            $temp['payment_date']= Carbon::parse($request->payment_date);
//
//            $temp['remark']="Payment credited by Admin/Exicutive";
//
////            dd($temp);
//            Amount::create($temp);

                    $temp_paybill['client_id']= $request->client_id;
                    $temp_paybill['exicutive_id']= Auth::id();;
                    $temp_paybill['pay_amount']= $request->credit;
                    $temp_paybill['pay_chanel']= $request->pay_chanel;
                    $temp_paybill['reference_no']= $request->reference_no;
                    $temp_paybill['payment_date']= Carbon::parse($request->payment_date);
                    $temp_paybill['remark']= "Payment Received By    ". User::find(Auth::id())->name;
                   // $temp_paybill['payment_date']= $request->payment_date;


                PayBill::create($temp_paybill);

                return back()->with('status',100)->with('message','add Successfully');

    }

    public function pandingPayment( Request $request)
    {
        if(isset($_GET['eid']))
        {
//            dd($_GET['eid']);
            $pending_payment=PayBill::with('exicutive.zones')
                ->with('client')
                ->where('exicutive_id',$_GET['eid'])
                ->where('approved',0)
                ->get();

            $total= PayBill::where('approved',0)
                ->where('exicutive_id',$_GET['eid'])
                ->get()->sum('pay_amount');



        }
        else
        {
            $pending_payment=PayBill::with('exicutive.zones')->with('client')->where('approved',0)->get();

            $total= PayBill::where('approved',0)->get()->sum('pay_amount');

        }


        $allexictive= User::where('role',0)->get();
                    //dd($pending_payment->toArray());

                    return view('admin.payment.pendingPayment',compact('pending_payment','allexictive','total','request'));
    }


    public function processApprovePayment()
    {

    }

    public function approvedPayment(Request $request)
    {
//        dd($id);

        //  dd($paybill_data->toArray());
//        dd($request->toArray());

        PayBill::where('id',$request->id)->update(['approved'=>1]);

        $paybill_data=PayBill::where('id',$request->id)->first();
        $temp=[];
        // $temp= $request->toArray();
            $temp['client_id']=$paybill_data->client_id;
            $temp['paybill_id']=$request->id;
            $temp['exicutive_id']=Auth::id();
            $temp['credit']=$paybill_data->pay_amount;
            $temp['payment_date']= Carbon::parse($paybill_data->payment_date);

            $temp['remark']="Payment Approved by Admin";
//            dd($temp);
            Amount::create($temp);

                $client=Client::find($paybill_data->client_id);
        $message= "Dear ". ucfirst($client->name) .", \n";
        $message= $message."Your internet Bill of Rs. ".$paybill_data->pay_amount . " has been Paid.\n";
//        $message= $message."Please Pay your due amount ASAP.\n";
        $message= $message."The Star Internet";
        if ( isset($request->sms_checkbox) && !empty($request->sms_checkbox ) )
        {
                if($request->sms_checkbox==1)
                {

                    SmsService::sendSms($message,$client->mobile);
                }
        }

        return back()->with('status',100)->with('message','Approved Successfully');






    }
    public function canclePayment($id)
    {
        $paybill_data=PayBill::where('id',$id)->delete();

        return back()->with('status',400)->with('message','Cancle Successfully');





//        dd($id);

    }

    public function findOutstanding($amount_data)

    {
        $total_credit=$amount_data->sum('credit');
        $total_debit=$amount_data->sum('debit');

        return ($total_credit-$total_debit);

    }
    public function paymentHistory()
    {


    }
    public function recentPaymentHistory()
    {
        $results= Amount::with('client')->with('exicutive')->whereNotNull('credit')->orderBy('created_at', 'desc')
                ->get();
//        $results= Amount::get();

//            dd($results->toArray());
//        return Response::json($results);
            return view('admin.payment.recentpayment',compact('results'));
    }

    public function showLedger(Request $request)
    {
        //dd($request->toArray());
        $result= Amount::where('client_id',$request->id)->with('exicutive')->get();

        return Response::json($result);
    }

    public  function sendMail()
    {
        $user=User::find(1);
        User::find(2)->notify(new GenerateBill($user));
       // Client::find(6)->notify(new GenerateBill);
    }

    public function paymentReport()
    {

    }
}

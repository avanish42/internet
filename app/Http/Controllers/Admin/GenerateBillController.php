<?php

namespace App\Http\Controllers\Admin;

use App\Mail\SendPdfBill;
use App\Mail\SendTextBill;
use App\Models\Amount;
use App\Models\GenerateBill;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Area;
use App\Models\Zone;
use App\Models\Client;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use phpDocumentor\Reflection\Types\Null_;
use App\Services\SmsService;
use App\Mail\SendNotification;
//use App\Mail\SendPdfBill;
use Response;


use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use PDF;
use Symfony\Component\VarDumper\Caster\DateCaster;

class GenerateBillController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function generateBill(){

        return view('admin.');


    }

    public function generateBills(Request $request)
    {
        $clients=array();

        $area= Area::get();
        $zones= Zone::get();

        if (isset($request->zone_id))
        {
//                    $clients= Client::with(['plan','area'])
//                        ->with(['zone'=>function ($query){
//                           $query->where('id',$request->zone_id);
//          }])->get();

            $clients= Client::with(['plan','area','zone'])->where('zone_id',$request->zone_id)->get();
            dd($clients);

        }

        if (isset($request->area_id)) {

        }




        return view('admin.client.filterclient',compact('clients','area','zones'));


    }

    public function autoGenerateBill()
    {


    }

    public function manualGenerateBill( Request $request)
    {
     //   dd($request->toArray());

                        $month=0;
                if(isset($request->month) && !empty($request->month))
                {
                    $current= Carbon::now();
                    $date_formet = Carbon::createFromFormat('Y-m-d', $current->year.'-'.$request->month.'-'.$current->day);
                    $month= $request->month;
                }

                else{

                    $date_formet= Carbon::now();
                    $month=  $date_formet->month;

                }




        $client_data=Client::whereIn('id',$request->client_ids)->with('amount')->with('generatedBill')->with('plan')
            ->get();

        foreach ($client_data as $k=>$v)
        {
            $amount=$this->calculateBillAmmount($v);
            $client= Client::find($v->id);
            if($v->is_pdf==1)
            {
               // $v->invoice_no= 'Star-'.date('my')."-".$v->customer_id;
                $privious_bill_no= GenerateBill::where('bill_no', 'LIKE', "%S".date('Y')."%")->orderby('created_at', 'desc')
                    ->first();
//

                if(isset($privious_bill_no) && !empty($privious_bill_no))
                {
                    $tem_bill_no= str_split($privious_bill_no->bill_no,5);

                    $tem_bill_no[1] =((int)$tem_bill_no[1])+1;
                    $tem_bill_no[1]=  str_pad($tem_bill_no[1],(5-strlen($tem_bill_no[1])),"0",STR_PAD_LEFT);
                    $v->invoice_no= $tem_bill_no[0].$tem_bill_no[1];
                }
                else
                {
                    $v->invoice_no= "S".date('Y')."0001";

                }

                $v->invoice_date= date('d-M-y',strtotime(Carbon::now()));
                $v->due_date= date('d-M-y',strtotime(Carbon::now()->addDay(12)));
                $v->start_date= date('d-M-y',strtotime($date_formet->startOfMonth())) ;
                $v->end_date= date('d-M-y',strtotime($date_formet->lastOfMonth())) ;
                $v->currnt_amount=$amount;
                $v->currnt_amount_word= $this->getIndianCurrency(abs($amount)) ;
                $v->amount_with_penalty=$v->currnt_amount+50;
                if ($v->is_gst_in_plan==1)
                {

                    $v->total_taxable_amount=$v->plan->price-($v->discount?$v->discount:"0");
                    $v->taxable_amount=round(($v->total_taxable_amount*100)/118,2);

                    $v->sgst=  ($v->total_taxable_amount-$v->taxable_amount)/2;

                    $v->total_tax=$v->sgst+$v->sgst;

                    $v->total_amount_paid=$v->taxable_amount+$v->total_tax;


                }
                elseif ($v->is_gst_in_plan==0)
                {
                    $v->taxable_amount=$v->plan->price-($v->discount?$v->discount:"0");
                    $v->sgst=($v->taxable_amount *9)/100;
                    $v->total_tax=$v->sgst+$v->sgst;
                    $v->total_amount_paid=$v->taxable_amount+$v->total_tax;
                }
//
              //  return view('bill.lalit',compact('v'));
                $pdf = PDF::loadView('bill.lalit',compact('v'));
                $path= "bills/".date('M')."/star-".time()."-".$v->invoice_no.".pdf";
                // $pdf->save(Storage::putFileAs())
                $pdf->save(storage_path('app/public/'.$path));
                $generate_bill_data=array('client_id'=>$v->id,'amount'=>$amount,'Remark'=>"Bill Generated",
                    'exicutive_id'=>Auth::id(),'bill_no'=>$v->invoice_no,'bill_path'=>$path,'bill_date'=>$date_formet,'updated_at'=>$date_formet);


                $message= "Dear ". ucfirst($v->name) .", \n";
                $message= $message."Your internet Bill of Rs. ".$amount . " has been generated.\n";
                $message= $message."Please Pay your due amount ASAP.\n";
                $message= $message."The Star Internet";
                if ( isset($request->sms_checkbox) && !empty($request->sms_checkbox))
                {

                    SmsService::sendSms($message,$v->mobile);
                }
            }
            else{

                    $message= "Dear ". ucfirst($v->name) .", \n";
                    $message= $message."Your internet package of Rs. ".$amount . " is renew.\n";
                    $message= $message."Please Pay your subscription ASAP.\n";
                    $message= $message."The Star Internet";

                if ( isset($request->sms_checkbox) && !empty($request->sms_checkbox))
                {

                    SmsService::sendSms($message,$v->mobile);
                }
                $privious_bill_no= GenerateBill::where('bill_no', 'LIKE', "%R".date('Y')."%")->orderby('created_at', 'desc')
                    ->first();
                if(isset($privious_bill_no) && !empty($privious_bill_no))
                {
                    $tem_bill_no= str_split($privious_bill_no->bill_no,5);
                    $tem_bill_no[1] =((int)$tem_bill_no[1])+1;
                    $tem_bill_no[1]=  str_pad($tem_bill_no[1],(5-strlen($tem_bill_no[1])),"0",STR_PAD_LEFT);
                    $v->invoice_no= $tem_bill_no[0].$tem_bill_no[1];
                }
                else
                {
                    $v->invoice_no= "R".date('Y')."0001";

                }

                if ( isset($v->email) && !empty($v->email) && isset($request->email_checkbox))
                {
                    $v->title= "Package Renew";
                    $v->message= $message;
                  //  Mail::send(new SendNotification($v));
                }
                $generate_bill_data=array('client_id'=>$v->id, 'amount'=>$amount,'Remark'=>"Bill Generated",
                    'exicutive_id'=>Auth::id(),'bill_no'=>$v->invoice_no,'bill_path'=>"",'bill_date'=>$date_formet,'updated_at'=>$date_formet);
            }



            $check_Generate_billdata=  GenerateBill::where('client_id',$v->id)
                ->whereMonth('updated_at',$month)
                ->first();

            if(isset($check_Generate_billdata))
            {
                $generate_bill=  GenerateBill::where('client_id',$v->id)
                    ->whereMonth('updated_at',$month)
                    ->update(['bill_path'=>$generate_bill_data['bill_path'],'bill_date'=>$date_formet,
                        'amount'=>$amount,'updated_at'=>$date_formet]);
                 $generate_bill=$check_Generate_billdata;
            }
            else
            {
                $generate_bill=$v->generatedBill()->save(new GenerateBill($generate_bill_data));

            }

        if(($v->is_pdf==1))
            {

                //$this->resendBill($generate_bill->id);
            }
        elseif($v->is_pdf==0)
        {
           // Mail::to($client)->send( new SendTextBill($client));
        }




            $amount_data= array('debit'=>$amount,'remark'=>"System  Generated",'exicutive_id'=>Auth::id(),'generate_bill_id'=> $generate_bill->id);

            $v->amount()->save(new Amount($amount_data));

        }
        if( isset($request->month) && !empty($request->month))
        {

            return redirect('bill/current-generate-bills?month='.$month)->with('status',100)->with('message','Bill Generate Successfuly..');
        }

        return redirect('bill/current-generate-bills')->with('status',100)->with('message','Bill Generate Successfuly..');


    }

    protected  function SendGenerateBillMail()
    {

    }


    protected function calculateBillAmmount($client_data)
    {
        //dd($client_data->toArray());
        $amount= $client_data->plan->price-$client_data->discount;
        $tax=0;

        if ($client_data->is_pdf==1) {


            if ($client_data->is_gst_in_plan == 0) {
                $tax = ($amount * 18) / 100;
                $amount = $amount + $tax;

            }
        }
        return $amount;
        //  dd($amount);


    }



    public function allBillList()
    {


        if(isset($_GET['zone_id'])) {

            $clients=Client::whereDoesntHave('generatedBill',function ($q){
                $q->whereMonth('bill_date',date('m'));
                 })->with('plan')->with('generatedBill')
                ->where('customer_status',1)
                ->where('zone_id',$_GET['zone_id'])
                ->get();

             }

            else
            {
                $clients=Client::whereDoesntHave('generatedBill',function ($q){
                    $q->whereMonth('bill_date',date('m'));
                    })->with('plan')->with('generatedBill')
                    ->where('customer_status',1)
                    ->get();
            }

            $all_zones= Zone::all();

        return view('admin.generatebill.alluserlist',compact('clients','all_zones'));

    }

    public function currentGeneratedBills(){

        $desired_month= date('m');
        if(isset($_GET['month'] ))
        {
            $desired_month=$_GET['month'];
        }
//        echo $desired_month;

        $clients=Client::whereHas('generatedBill' ,function ($q) use($desired_month){
            $q->whereMonth('bill_date',$desired_month);
            $q->where('amount','!=','0');
        })->with('plan')
            ->with(['generatedBill'=>function($q) use ($desired_month){
                    $q->whereMonth('bill_date',$desired_month);
            }])
            ->get();

            $month= $monthName = date('F', mktime(0, 0, 0, $desired_month, 10));

        return view('admin.generatebill.currentgeneratedbills',compact('clients','month'));


    }

    public function cancleGeneratedBills(Request $request)
    {
        foreach ($request->bill_ids as $k=>$v)
        {
                    $update_date= GenerateBill::where('id', $v)->first()->updated_at;
            GenerateBill::where('id', $v)
                ->update(['amount' =>'0','bill_path'=>'','bill_date'=>Null,'updated_at'=>$update_date ]);

            $amount =Amount::where('generate_bill_id',$v);
            $amount->delete();
        }


        return back()->with('status',400)->with('message','Deleted');

    }

    public function getIndianCurrency(float $number)
    {
        $decimal = round($number - ($no = floor($number)), 2) * 100;
        $hundred = null;
        $digits_length = strlen($no);
        $i = 0;
        $str = array();
        $words = array(0 => '', 1 => 'one', 2 => 'two',
            3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
            7 => 'seven', 8 => 'eight', 9 => 'nine',
            10 => 'ten', 11 => 'eleven', 12 => 'twelve',
            13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
            16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
            19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
            40 => 'forty', 50 => 'fifty', 60 => 'sixty',
            70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
        $digits = array('', 'hundred','thousand','lakh', 'crore');
        while( $i < $digits_length ) {
            $divider = ($i == 2) ? 10 : 100;
            $number = floor($no % $divider);
            $no = floor($no / $divider);
            $i += $divider == 10 ? 1 : 2;
            if ($number) {
                $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
                $str [] = ($number < 21) ? $words[$number].' '. $digits[$counter]. $plural.' '.$hundred:$words[floor($number / 10) * 10].' '.$words[$number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
            } else $str[] = null;
        }
        $Rupees = implode('', array_reverse($str));
        $paise = ($decimal) ? "." . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
        return ($Rupees ? $Rupees . 'Rupees ' : '') . $paise ;
    }

    public function resendBill($id)
    {
//        dd($id);
//        $id=$id;
        $generate_bill_data= GenerateBill::find($id)->toArray();

        // dd($generate_bill_data);

        $client= Client::where('id',$generate_bill_data['client_id'])->first();

        $client->bill_path= $generate_bill_data['public_path'];
        $client->amount= $generate_bill_data['amount'];
//        dd($client->toArray());

        Mail::to($client)->send( new SendPdfBill($client));


        return back()->with('status',400)->with('message','Bill Send Successfully');

    }

    public function nonGeneratedBillByMonth()
    {
        $desired_month= date('m')-1;
        if(isset($_GET['month'] ))
        {
            $desired_month=$_GET['month'];
        }

        $clients=Client::whereDoesntHave('generatedBill',function ($q) use ($desired_month){
            $q->whereMonth('bill_date',$desired_month);
        })->with('plan')->with('generatedBill')->get();

        $month= $desired_month;

        return view('admin.generatebill.nongeneratedbillbymonth',compact('clients','month'));

    }



}

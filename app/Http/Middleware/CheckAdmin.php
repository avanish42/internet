<?php

namespace App\Http\Middleware;

use Closure;

class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::user()->role == 1) {
            return $next($request);
        }
//        return $next($request);
        return redirect('exicutive/search-for-payment')->with('status',400)->with('message','You dont have permission for this page');
    }
}

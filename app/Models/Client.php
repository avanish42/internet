<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\GenerateBill;
use App\Models\PayBill;
use App\Models\Amount;
use Illuminate\Notifications\Notifiable;


class Client extends Model
{
    //
    use Notifiable;

    protected $fillable=['zone_id','area_id','plan_id','name','email','coustmer_type','customer_id','card_id','user_id','mobile','joining_date','billing_date','is_pdf','auto_bill',
    'is_gst','discount','installment_amount','gst_no','device_type','device_token','street','postal_code','city','state','is_igst','is_gst_in_plan','created_at','updated_at'];


    public function setAutoBillAttribute($value)
    {
//      return   isset($value)?"1":"0";
      if(isset($value) && $value=='on' )
      {
          $this->attributes['auto_bill']=1;

      }
      else
      {
          $this->attributes['auto_bill']=0;

      }

    }

    public function setIsGstAttribute($value)
    {
        if(isset($value) && $value=='on' )
        {
            $this->attributes['is_gst']=1;

        }
        else
        {
            $this->attributes['is_gst']=0;

        }
    }
    public function setIsPdfAttribute($value)
    {
        if(isset($value) && $value=='on' )
        {
            $this->attributes['is_pdf']=1;
        }
        else
        {
            $this->attributes['is_pdf']=0;
        }
    }

    public function setIsIgstAttribute($value)
    {
        if(isset($value) && $value=='on')
        {
            $this->attributes['is_igst']=1;
        }
        else
        {
            $this->attributes['is_igst']=0;
        }
    }

    public function setIsGstInPlanAttribute($value)
    {
        if(isset($value) && $value=='on')
        {
            $this->attributes['is_gst_in_plan']=1;
        }
        else
        {
            $this->attributes['is_gst_in_plan']=0;
        }
    }



    public function generatedBill()
    {
        return $this->hasMany(GenerateBill::class,'client_id','id');
    }



    public function payBill()
    {
        return $this->hasMany(PayBill::class,'client_id','id');
    }

    public  function plan()
    {
        return $this->belongsTo(Plan::class,'plan_id','id');
    }

    public  function area()
    {
        return $this->belongsTo(Area::class,'area_id','id');
    }
    public  function zone()
    {
        return $this->belongsTo(Zone::class,'zone_id','id');
    }


    public function amount()
    {
        return $this->hasMany(Amount::class,'client_id','id');
    }

















}

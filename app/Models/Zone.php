<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Area;

class Zone extends Model
{
    //
    protected $fillable=['name','code','description'];

    public function area()
    {
        return $this->hasMany(Area::class,'zone_id','id');
    }

    public  function client()
    {
        return $this->hasMany(Client::class,'zone_id','id');
    }

}

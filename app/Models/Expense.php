<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Expense extends Model
{
    //
    protected $table='expenses';
    protected $fillable=['amount','remark','exicutive_id','description'];


    public function exicutive(){

        return $this->belongsTo(User::class,'exicutive_id','id');
    }


}


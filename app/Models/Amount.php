<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Client;
use App\User;

class Amount extends Model
{
    //
    protected $fillable=['paybill_id','client_id','exicutive_id','remark','credit','debit','generate_bill_id','payment_date'];

    public function client()
    {
        return $this->belongsTo(Client::class,'client_id','id');
    }
    public function exicutive()
    {
        return $this->belongsTo(User::class,'exicutive_id','id');
    }

    public function payBill()
    {
        return $this->belongsTo(PayBill::class,'paybill_id','id');
    }

    public function generatebill()
    {

        return $this->belongsTo(GenerateBill::class,'generate_bill_id','id');
    }

}

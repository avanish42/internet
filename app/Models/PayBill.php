<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use App\Models\Client;

class PayBill extends Model
{
    //

    protected $fillable=['client_id','exicutive_id','pay_amount','pay_chanel','remark','payment_date','reference_no'];


    public function exicutive()
    {
        return $this->belongsTo(User::class,'exicutive_id','id');
    }
    public function client()
    {
        return $this->belongsTo(Client::class,'client_id','id');
    }

}

<?php

namespace App\Models;
use App\Models\Zone;
use App\User;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $fillable=['zone_id','name','code','address','remark','exicutive_id'];

    public function zone()
    {
        return $this->belongsTo(Zone::class,'zone_id','id');
    }

}

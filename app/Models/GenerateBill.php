<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use App\Models\Client;

class GenerateBill extends Model
{

    public $timestamps = true;
    protected $fillable=['client_id','exicutive_id','bill_path','amount','Remark','bill_no','bill_date','updated_at'];

protected $appends = ['public_path'];

    public function exicutive()
    {
        return $this->belongsTo(User::class,'exicutive_id','id');
    }

    public function client()
    {
        return $this->belongsTo(Client::class,'client_id','id');
    }
    public function   getPublicPathAttribute($value)
    {
        return  public_path('storage/'.$this->getOriginal('bill_path'));
    }

    public function getBillPathAttribute($value)
    {
        return  asset('storage/'.$value);
    }




}

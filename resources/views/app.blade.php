<!DOCTYPE html>
<html lang="en">

@include('layout.head');

<body>

@if(session('status') && session('status') == 100)
    <script type="text/javascript">
        var status = {!! json_encode(session('status')) !!}
        var message = {!! json_encode(session('message')) !!}
    </script>

@elseif(session('status') && session('status') == 200)

    <script type="text/javascript">
        var status = {!! json_encode(session('status')) !!}
        var message = {!! json_encode(session('message')) !!}
    </script>

@elseif(session('status') && session('status') == 400)

    <script type="text/javascript">
        var status = {!! json_encode(session('status')) !!}
        var message = {!! json_encode(session('message')) !!}
    </script>

@endif

<script type="text/javascript">
    var APP_URL ={!! json_encode(url('/')) !!}
    var token = '{!! csrf_token() !!}'
</script>



{{--<!--Preloader-->--}}
{{--<div class="preloader-it">--}}
    {{--<div class="la-anim-1"></div>--}}
{{--</div>--}}
{{--<!--/Preloader-->--}}
<div class="wrapper theme-4-active pimary-color-red ">
@include('layout.top');

    @if(Auth::user()->role==1)
    @include('layout.left');
    @endif

    @if(Auth::user()->role==0)
        @include('layout.subadminleft');
    @endif



@include('layout.right');

@yield('content');
</div>

@include('layout.script');

</body>

</html>

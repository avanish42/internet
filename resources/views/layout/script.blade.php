<!-- jQuery -->
<script src={{URL::asset("vendors/bower_components/jquery/dist/jquery.min.js")}} ></script>
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
{{--<script src={{URL::asset("vendors/bower_components/jquery/dist/jquery.min.js")}} ></script>--}}

<!-- Bootstrap Core JavaScript -->
<script src={{URL::asset("vendors/bower_components/bootstrap/dist/js/bootstrap.min.js")}} ></script>
<script src={{URL::asset("bower_components/remarkable-bootstrap-notify/dist/bootstrap-notify.js")}} ></script>
{{--<script src=></script>--}}

<!-- Data table JavaScript -->
<script src={{URL::asset("vendors/bower_components/datatables/media/js/jquery.dataTables.min.js")}} ></script>
<script src={{URL::asset("dist/js/dataTables-data.js")}} ></script>




<!-- Data table JavaScript -->
<script src={{URL::asset("vendors/bower_components/datatables/media/js/jquery.dataTables.min.js")}} ></script>
{{--<script src={{URL::asset("vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js")}} ></script>--}}
{{--<script src={{URL::asset("vendors/bower_components/datatables.net-buttons/js/buttons.flash.min.js")}} ></script>--}}
{{--<script src={{URL::asset("vendors/bower_components/jszip/dist/jszip.min.js")}} ></script>--}}
{{--<script src={{URL::asset("vendors/bower_components/pdfmake/build/pdfmake.min.js")}} ></script>--}}
{{--<script src={{URL::asset("vendors/bower_components/pdfmake/build/vfs_fonts.js")}} ></script>--}}
{{--<script src={{URL::asset("vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js")}} ></script>--}}
{{--<script src={{URL::asset("vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js")}} ></script>--}}
{{--<script src={{URL::asset("dist/js/export-table-data.js")}} ></script>--}}




<!-- Sweet-Alert  -->
<script src={{URL::asset("vendors/bower_components/sweetalert/dist/sweetalert.min.js")}} ></script>
<script src={{URL::asset("dist/js/sweetalert-data.js")}} ></script>

<!-- Moment JavaScript -->
<script src={{URL::asset("vendors/bower_components/moment/min/moment-with-locales.min.js")}} ></script>


<!-- Bootstrap Switch JavaScript -->
<script src={{URL::asset("vendors/bower_components/bootstrap-switch/dist/js/bootstrap-switch.min.js")}} ></script>

<!-- Bootstrap Datetimepicker JavaScript -->
{{--<script src={{URL::asset("vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js")}} ></script>--}}
{{--<script src={{URL::asset("vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datepicker.js")}} ></script>--}}
<!-- Bootstrap Daterangepicker JavaScript -->
{{--<script src={{URL::asset("vendors/bower_components/bootstrap-daterangepicker/daterangepicker.js")}} ></script>--}}


<!-- Form Picker Init JavaScript -->
{{--<script src={{URL::asset("dist/js/form-picker-data.js")}} ></script>--}}


<!-- Slimscroll JavaScript -->
<script src={{URL::asset("dist/js/jquery.slimscroll.js")}} ></script>

<!-- Sparkline JavaScript -->
<script src={{URL::asset("vendors/jquery.sparkline/dist/jquery.sparkline.min.js")}} ></script>

<!-- Owl JavaScript -->
<script src={{URL::asset("vendors/bower_components/owl.carousel/dist/owl.carousel.min.js")}} ></script>


<!-- simpleWeather JavaScript -->
<script src={{URL::asset("vendors/bower_components/moment/min/moment.min.js")}} ></script>
<script src={{URL::asset("vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js")}} ></script>
<script src={{URL::asset("dist/js/simpleweather-data.js")}} ></script>


<!-- Progressbar Animation JavaScript -->
<script src={{URL::asset("vendors/bower_components/waypoints/lib/jquery.waypoints.min.js")}} ></script>
<script src={{URL::asset("vendors/bower_components/jquery.counterup/jquery.counterup.min.js")}} ></script>


<!-- Fancy Dropdown JS -->
<script src={{URL::asset("dist/js/dropdown-bootstrap-extended.js")}} ></script>

<!-- ChartJS JavaScript -->
<script src={{URL::asset("vendors/chart.js/Chart.min.js")}} ></script>
<!-- Form Advance Init JavaScript -->
<script src={{URL::asset("dist/js/form-advance-data.js")}} ></script>

<!-- Morris Charts JavaScript -->
<script src={{URL::asset("vendors/bower_components/raphael/raphael.min.js")}} ></script>
<script src={{URL::asset("vendors/bower_components/morris.js/morris.min.js")}} ></script>
<script src={{URL::asset("vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js")}} ></script>


<!-- Switchery JavaScript -->
<script src={{URL::asset("vendors/bower_components/switchery/dist/switchery.min.js")}} ></script>

<!-- Fancy Dropdown JS -->
<script src={{URL::asset("dist/js/dropdown-bootstrap-extended.js")}} ></script>
<!-- Bootstrap Daterangepicker JavaScript -->
{{--<script src= {{URL::asset("vendors/bower_components/bootstrap-daterangepicker/daterangepicker.js")}}></script>--}}


<!-- Init JavaScript -->
<script src={{URL::asset("dist/js/init.js")}} ></script>
<script src={{URL::asset("dist/js/dashboard-data.js")}} ></script>

<!-- user JavaScript -->
<script src={{URL::asset("js/user.js")}} ></script>

@stack('pagescript')





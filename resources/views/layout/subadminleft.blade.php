<!-- Left Sidebar Menu -->
<div class="fixed-sidebar-left">
    <ul class="nav navbar-nav side-nav nicescroll-bar">

        {{--<li>--}}
        {{--<a href={{url('filter-client')}}><div class="pull-left"><i class="zmdi zmdi-flag mr-20"></i><span--}}
        {{--class="right-nav-text">Client Management</span></div><div class="pull-right"><span--}}
        {{--class="label label-warning">8</span></div><div class="clearfix"></div></a>--}}
        {{--</li>--}}
        <li><hr class="light-grey-hr mb-10"/></li>
        <li class="navigation-header">
            <span>Billing & ACCOUNTING</span>
            <i class="zmdi zmdi-more"></i>
        </li>

        <li>
            <a  href="javascript:void(0);" data-toggle="collapse" data-target="#form_dr"><div class="pull-left"><i
                            class="zmdi zmdi-edit mr-20"></i><span class="right-nav-text">Payment
                    </span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
            <ul id="form_dr" class="collapse collapse-level-1 two-col-list">
                <li>
                    <a href="{{URL::asset('exicutive/search-for-payment')}}">Search  Payment</a>
                </li>

                <li>
                    <a href="{{URL::asset('exicutive/recent-payment-history')}}">Recent Payment</a>
                </li>

            </ul>
        </li>



        <li><hr class="light-grey-hr mb-10"/></li>



        <li class="navigation-header">
            <span>Reports</span>
            <i class="zmdi zmdi-more"></i>
        </li>



        <li><hr class="light-grey-hr mb-10"/></li>

    </ul>
</div>
<!-- /Left Sidebar Menu -->
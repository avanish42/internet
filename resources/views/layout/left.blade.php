<!-- Left Sidebar Menu -->
<div class="fixed-sidebar-left">
    <ul class="nav navbar-nav side-nav nicescroll-bar">
        <li class="navigation-header">
            <span>Main</span>
            <i class="zmdi zmdi-more"></i>
        </li>
        <li>
            <a href="javascript:void(0);" data-toggle="collapse" data-target="#dashboard_dr"><div
                        class="pull-left"><i class="zmdi zmdi-landscape mr-20"></i><span
                            class="right-nav-text">Package Management</span></div><div class="pull-right"><i class="zmdi
                            zmdi-caret-down"></i></div><div class="clearfix"></div></a>
            <ul id="dashboard_dr" class="collapse collapse-level-1">
                <li>
                    <a href="{{URL::asset('zone')}}">Zone</a>
                </li>

                <li>
                    <a href="{{URL::asset('area')}}">Area</a>
                </li>
                <li>
                    <a href="{{URL::asset('plan')}}">Plan</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="javascript:void(0);" data-toggle="collapse" data-target="#ecom_dr"><div class="pull-left"><i
                            class="zmdi zmdi-shopping-basket mr-20"></i><span
                            class="right-nav-text">Clients </span></div><div class="pull-right"><span class="label
                            label-success">Management</span></div><div class="clearfix"></div></a>
            <ul id="ecom_dr" class="collapse collapse-level-1">

                <li>
                    <a href="{{URL::asset('add-client')}}">Add New Client</a>
                </li>
                <li>
                    <a href="{{URL::asset('all-clients')}}">All Clients</a>
                </li>
                <li>
                    {{--<a href={{URL::asset('client-bills')}}>Bill Generate </a>--}}
                </li>

            </ul>
        </li>
        <li>
            <a href="javascript:void(0);" data-toggle="collapse" data-target="#app_dr"><div class="pull-left"><i
                            class="zmdi zmdi-apps mr-20"></i><span class="right-nav-text">User Management
                    </span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
            <ul id="app_dr" class="collapse collapse-level-1">
                <li>
                    <a href={{url('exicutive/add-exicutive')}}>Add Exicutive </a>
                </li>
                <li>
                    <a href={{url('exicutive/all-exicutive')}}>All Exicutive </a>
                </li>
                <li>
                    {{--<a href={{URL::asset('payment')}}>payment</a>--}}
                </li>

            </ul>
        </li>
        {{--<li>--}}
            {{--<a href={{url('filter-client')}}><div class="pull-left"><i class="zmdi zmdi-flag mr-20"></i><span--}}
                            {{--class="right-nav-text">Client Management</span></div><div class="pull-right"><span--}}
                            {{--class="label label-warning">8</span></div><div class="clearfix"></div></a>--}}
        {{--</li>--}}
        <li><hr class="light-grey-hr mb-10"/></li>
        <li class="navigation-header">
            <span>Billing & ACCOUNTING</span>
            <i class="zmdi zmdi-more"></i>
        </li>
        <li>
            <a href="javascript:void(0);" data-toggle="collapse" data-target="#ui_dr"><div class="pull-left"><i
                            class="zmdi zmdi-smartphone-setup mr-20"></i><span
                            class="right-nav-text">Billing </span></div><div class="pull-right"><i
                            class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
            <ul id="ui_dr" class="collapse collapse-level-1 two-col-list">
                <li>
                    <a href={{URL::asset('bill/all-bill-list')}}>Generate Bills</a>
                </li>
                <li>
                    <a href={{URL::asset('bill/current-generate-bills')}}>current Generated Bills</a>
                </li>

                <li>
                    <a href={{URL::asset('bill/non-generated-bill/?month='.(date('m')-1))}}>Non Generated Bills</a>
                </li>

            </ul>
        </li>
        <li>
            <a  href="javascript:void(0);" data-toggle="collapse" data-target="#form_dr"><div class="pull-left"><i
                            class="zmdi zmdi-edit mr-20"></i><span class="right-nav-text">Payment
                    </span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
            <ul id="form_dr" class="collapse collapse-level-1 two-col-list">
                <li>
                    <a href="{{URL::asset('payment/search-for-payment')}}">Search  Payment</a>
                </li>

                <li>
                    <a href="{{URL::asset('payment/recent-payment-history')}}">Recent Payment</a>
                </li>

                {{--<li>--}}
                    {{--<a href="{{URL::asset('payment/pending-payment')}}">Panding Payment</a>--}}
                {{--</li>--}}

            </ul>
        </li>
        <li>
            <a class="<?php if(isset($request) && $request->is('payment/pending-payment')) echo 'active' ?>"
               href="{{URL::asset
            ('payment/pending-payment')
            }}"><div
                        class="pull-left"><i class="zmdi
            zmdi-flag
            mr-20"></i><span class="right-nav-text">Confirm Payment</span></div><div class="pull-right"><span
                            class="label label-warning">{{ \App\Models\PayBill::where('approved',0)->get()->count()
                            }}</span></div><div
                        class="clearfix"></div></a>
        </li>

        <li>
            <a class="<?php if(isset($request) && $request->is('payment/pending-payment')) echo 'active' ?>"
               href="{{URL::asset
            ('payment/payment-reminder')
            }}"><div
                        class="pull-left"><i class="zmdi
            zmdi-flag
            mr-20"></i><span class="right-nav-text">Payment Reminder</span></div><div class="pull-right"><span
                            class="label label-warning">New</span></div><div
                        class="clearfix"></div></a>
        </li>





        <li><hr class="light-grey-hr mb-10"/></li>
        <li class="navigation-header">
            <span>Extra</span>
            <i class="zmdi zmdi-more"></i>
        </li>



        <li>
            <a  class="<?php if(isset($request) && $request->is('expense/all-expense')) echo 'active' ?>"
                    href={{URL::asset('expense/all-expense')}}><div class="pull-left"><i
                            class="zmdi zmdi-smartphone-setup mr-20"></i><span class="right-nav-text">Expense</span></div><div class="pull-right"><span class="label label-warning">&#8377;</span></div><div class="clearfix"></div></a>
        </li>
        <li>
            <a  href="javascript:void(0);" data-toggle="collapse" data-target="#form_drn"><div class="pull-left"><i
                            class="zmdi zmdi-edit mr-20"></i><span class="right-nav-text">Notifications
                    </span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
            <ul id="form_drn" class="collapse collapse-level-1 two-col-list">
                <li>
                    <a href="{{URL::asset('notification/select-user')}}">Filter User</a>
                </li>

                {{--<li>--}}
                {{--<a href="{{URL::asset('payment/recent-payment-history')}}">Recent Payment</a>--}}
                {{--</li>--}}

            </ul>
        </li>
        <li><hr class="light-grey-hr mb-10"/></li>

        <li class="navigation-header">
            <span>Reports</span>
            <i class="zmdi zmdi-more"></i>
        </li>

        <li>
            <a href="javascript:void(0);" data-toggle="collapse" data-target="#ui_dr_report"><div class="pull-left"><i
                            class="zmdi zmdi-smartphone-setup mr-20"></i><span
                            class="right-nav-text">Reports </span></div><div class="pull-right"><i
                            class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
            <ul id="ui_dr_report" class="collapse collapse-level-1 two-col-list">
                <li>
                    <a href={{URL::asset('reports/user-reports')}}>User Report</a>
                </li>
                <li>
                    <a href={{URL::asset('reports/billing-report')}}>Billing Report</a>
                </li>

                <li>
                    <a href={{URL::asset('reports/payment-report')}}>Payment Report</a>
                </li>

                <li>
                    <a href={{URL::asset('reports/expence-report')}}>Expence</a>
                </li>


            </ul>
        </li>

        <li><hr class="light-grey-hr mb-10"/></li>

    </ul>
</div>
<!-- /Left Sidebar Menu -->
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title> @yield('title')</title>


    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href= {{asset("favicon.ico")}} type="image/x-icon">



    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <!-- Morris Charts CSS -->
    <link href={{URL::asset("vendors/bower_components/morris.js/morris.css" )}} rel="stylesheet" type="text/css"/>
    <!--alerts CSS -->
    <link href= {{URL::asset("vendors/bower_components/sweetalert/dist/sweetalert.css")}} rel="stylesheet"
          type="text/css">

    <!-- Bootstrap Datetimepicker CSS -->
    <link href= {{URL::asset("vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css")}}  rel="stylesheet" type="text/css"/>
    <link href= {{URL::asset("vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/datepicker.css")}}
            rel="stylesheet" type="text/css"/>


    <!-- Bootstrap Switches CSS -->
    <link href={{URL::asset("vendors/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css" )}}
          rel="stylesheet" type="text/css"/>

    <!-- Bootstrap Daterangepicker CSS -->
    <link href={{URL::asset("vendors/bower_components/bootstrap-daterangepicker/daterangepicker.css")}} rel="stylesheet"
          type="text/css"/>


    <!-- Data table CSS -->
    <link href={{URL::asset("vendors/bower_components/datatables/media/css/jquery.dataTables.min.css")}} rel="stylesheet"
          type="text/css"/>



    <!-- Custom CSS -->
    <link href= {{URL::asset("dist/css/style.css")}} rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
@component('mail::message')
# Hello {{$client->name}}



{{--@component('mail::button', ['url' => ''])--}}
{{--Button Text--}}
{{--@endcomponent--}}

@component('mail::table')
    | S No|    Name       | Example                 |
    | ----|:------------- :| ----------------------:|
    | 1   |  Name          | {{$client->name}}      |
    | 2   | Amount         | 457.32                 |      |
@endcomponent
@component('mail::panel')
    This is System generated bill
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent

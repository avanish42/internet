@component('mail::message')
# Bill

This is system generated bill.

@component('mail::table')
    | S no   | name          | Attribute  |
    | -------|:-------------:| --------:|
    | 1      |   Clint name  | {{$client->name}}      |
    | 2      | Amount        | {{$client->amount}}     |
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent

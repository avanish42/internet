<footer id="footer">
    <!-- Footer Top Start -->
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <section class="col-lg-3 col-md-3 col-xs-12 col-sm-3 footer-one">
                    <h3>About</h3>
                    <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                    </p>
                </section>
                <section class="col-lg-3 col-md-3 col-xs-12 col-sm-3 footer-two">
                    <h3>Quick Links</h3>
                    <ul class="contact-us">
                        <li> <i class="fa fa-angle-right"></i>&nbsp;&nbsp; <a href="index.html">Home</a></li>
                        <li> <i class="fa fa-angle-right"></i>&nbsp;&nbsp; <a href="about_us.html">About Us</a></li>
                        <li> <i class="fa fa-angle-right"></i>&nbsp;&nbsp; <a href="#">Our Services</a></li>
                        <li> <i class="fa fa-angle-right"></i>&nbsp;&nbsp; <a href="login/index.html">Login</a></li>
                        <li> <i class="fa fa-angle-right"></i>&nbsp;&nbsp; <a href="contact_us.html">Contact Us</a></li>


                    </ul>
                </section>

                <section class="col-lg-3 col-md-3 col-xs-12 col-sm-3 footer-three">
                    <h3>Quick Links</h3>
                    <ul class="contact-us">
                        <li> <i class="fa fa-angle-right"></i>&nbsp;&nbsp; <a href="privacy_policy.html">Privacy Policy </a></li>
                        <li> <i class="fa fa-angle-right"></i>&nbsp;&nbsp; <a href="terms_&_conditions.html">Terms & Conditions</a></li>
                        <li> <i class="fa fa-angle-right"></i>&nbsp;&nbsp; <a href="refund_policy.html">Refund Policy</a></li>
                        <li> <i class="fa fa-angle-right"></i>&nbsp;&nbsp; <a href="disclaimer.html">Disclaimer</a></li>
                        <li> <i class="fa fa-angle-right"></i>&nbsp;&nbsp; <a href="#">Site Map</a></li>


                    </ul>
                </section>

                <section class="col-lg-3 col-md-3 col-xs-12 col-sm-3 footer-four">
                    <h3>Contact Us</h3>
                    <ul class="contact-us">
                        <li>
                            <i class="fa fa-map-marker"></i>
                            <p>
                                <strong class="contact-pad">Address:</strong> B-117, Vasant Kunj Enclave, <br>Vasant Kunj, New Delhi -110070
                            </p>
                        </li>
                        <li>
                            <i class="fa fa-phone"></i>
                            <p><strong>Phone:</strong> +91 98731 35358, 99101 35358</p>
                        </li>
                        <li>
                            <i class="fa fa-envelope"></i>
                            <p><strong>Email:</strong><a href="mailto:starinternet12@gmail.com">starinternet12@gmail.com</a></p>
                        </li>
                    </ul>
                </section>

            </div>
        </div>
    </div>
    <!-- Footer Top End -->
    <!-- Footer Bottom Start -->
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-xs-12 col-sm-6 "> &copy; Copyright 2018 by <a href="#">THE STAR INTERNET</a>. All Rights Reserved. </div>
                <div class="col-lg-6 col-md-6 col-xs-12 col-sm-6 ">
                    <ul class="social social-icons-footer-bottom">
                        <li class="facebook"><a href="#" data-toggle="tooltip" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                        <li class="twitter"><a href="#" data-toggle="tooltip" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                        <li class="dribbble"><a href="#" data-toggle="tooltip" title="Dribble"><i class="fa fa-dribbble"></i></a></li>
                        <li class="linkedin"><a href="#" data-toggle="tooltip" title="LinkedIn"><i class="fa fa-linkedin"></i></a></li>
                        <li class="rss"><a href="#" data-toggle="tooltip" title="Rss"><i class="fa fa-rss"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer Bottom End -->
</footer>
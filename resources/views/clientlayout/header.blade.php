<!-- Header Start -->
<header id="header">
    <!-- Header Top Bar Start -->
    <div class="top-bar">
        <div class="slidedown collapse">
            <div class="container">
                <div class="phone-email pull-left">
                    <a><i class="fa fa-phone"></i> Call Us : +91 987313 35358, 99101 35358</a>
                    <a href="mailto:support@starinternet.com"><i class="fa fa-envelope"></i> Email : starinternet12@gmail.com</a>
                </div>
                <div class="follow-us pull-right">
                    <ul class="social pull-left">
                        <li class="facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li class="dribbble"><a href="#"><i class="fa fa-dribbble"></i></a></li>
                        <li class="linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        <li class="rss"><a href="#"><i class="fa fa-rss"></i></a></li>
                    </ul>
                    <div id="search-form" class="pull-right">
                        <form action="#" method="get">
                            <input type="text" class="search-text-box" placeholder="Search...">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Header Top Bar End -->
    <!-- Main Header Start -->
    <div class="main-header">
        <div class="container">
            <!-- TopNav Start -->
            <div class="topnav navbar-header">
                <a class="navbar-toggle down-button" data-toggle="collapse" data-target=".slidedown">
                    <i class="fa fa-angle-down icon-current"></i>
                </a>
            </div>
            <!-- TopNav End -->
            <!-- Logo Start -->
            <div class="logo pull-left">
                <h1>
                    <a href="index.html" style="color:#fff; text-transform:uppercase; font-weight:bold;"> The <span style="color:#2eaef0">Star Internet</span>
                        <!--<img src="img/logo.png" alt="Star Internet" width="125" height="60">-->
                    </a>
                </h1>
            </div>
            <!-- Logo End -->
            <!-- Mobile Menu Start -->
            <div class="mobile navbar-header">
                <a class="navbar-toggle" data-toggle="collapse" href=".html">
                    <i class="fa fa-bars fa-2x"></i>
                </a>
            </div>
            <!-- Mobile Menu End -->
            <!-- Menu Start -->
            <nav class="collapse navbar-collapse menu">
                <ul class="nav navbar-nav sf-menu">
                    <li>
                        <a id="current" href={{asset('/')}}>Home </a></li>
                    <li><a  href={{asset('about')}}>About Us </a></li>

                    {{--<li>--}}
                        {{--<a href="#" class="sf-with-ul">--}}
                            {{--Our Servicves--}}
                            {{--<span class="sf-sub-indicator">--}}
                           {{--<i class="fa fa-angle-down "></i>--}}
                           {{--</span>--}}
                        {{--</a>--}}
                        {{--<ul>--}}
                            {{--<li><a href="#" class="sf-with-ul">Services 1</a></li>--}}
                            {{--<li><a href="#" class="sf-with-ul">Services 2</a></li>--}}
                            {{--<li><a href="#" class="sf-with-ul">Services 3</a></li>--}}

                        {{--</ul>--}}
                    {{--</li>--}}
                    <li><a  href={{asset('complain')}}>Quick Complaint </a></li>
                    <li><a  href={{asset('recharge')}}>Quick Recharge</a></li>
                    <li><a target="_blank" href={{asset('login')}}>Admin Login </a></li>
                    <li><a  href={{asset('contect')}}>Contact Us </a></li>


                </ul>
            </nav>
            <!-- Menu End -->
        </div>
    </div>
    <!-- Main Header End -->
</header>
<!-- Header End -->
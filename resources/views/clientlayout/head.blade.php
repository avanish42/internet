
<head>
    <meta charset="utf-8">
    <title>@yield('title')</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:400,300,400italic,700,300italic' rel='stylesheet' type='text/css'>
    <!-- Library CSS -->
    <link rel="stylesheet" href="{{asset('client/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('client/css/bootstrap-theme.css')}}">
    <link rel="stylesheet" href="{{asset('client/css/fonts/font-awesome/css/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset('client/css/animations.css')}}" media="screen">
    <link rel="stylesheet" href="{{asset('client/css/superfish.css')}}" media="screen">
    {{--<link rel="stylesheet" href="{{asset('client/css/bootstrap-tss')}}" media="screen">--}}
    <link rel="stylesheet" href="{{asset('client/css/revolution-slider/css/settings.css')}}" media="screen">
    <link rel="stylesheet" href="{{asset('client/css/revolution-slider/css/extralayers.css')}}"  media="screen">
    <link rel="stylesheet" href="{{asset('client/css/prettyPhoto.css')}}" media="screen">
    <link rel="stylesheet" href="{{asset('client/css/pricing-table.css')}}">

    <link rel="stylesheet" href="{{asset('client/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('client/css/global.css')}}">
    <link rel="stylesheet" href="{{asset('client/css/colors/blue.css')}}"  class="colors">
    <link rel="stylesheet" href="{{asset('client/css/theme-responsive.css')}}">
    <link rel="stylesheet" href="{{asset('client/css/switcher.css')}}" rel="stylesheet"  >
    <link rel="stylesheet" href="{{asset('client/css/spectrum.css')}}" rel="stylesheet"  >


    <!-- Favicons -->
    <!--<link rel="shortcut icon" href="img/ico/favicon.ico">-->

    <style>
        .logo h1 {
            margin: 23px 0;
        }

    </style>


</head>
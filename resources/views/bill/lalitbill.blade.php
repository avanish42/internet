<html>
<head>
<title>Bill</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>

<p style="margin-left: 133.0pt;"><strong><span style="font-size: 26.0pt; font-family: 'Microsoft Sans Serif','sans-serif';">THE STAR INTERNET</span></strong></p>
<p style="line-height: 8.35pt;"><span style="font-size: 12.0pt;">&nbsp;</span></p>
<table style="margin-left: 2.0pt; border-collapse: collapse;">
    <tbody>
    <tr style="height: 12.5pt;">
        <td style="width: 224.0pt; padding: 0in 0in 0in 0in; height: 12.5pt;" width="299">
            <p style="margin-left: 187.0pt;"><strong><span style="font-size: 10.0pt; font-family: 'Microsoft Sans Serif','sans-serif';">Internet</span></strong></p>
        </td>
        <td style="width: 1.75in; padding: 0in 0in 0in 0in; height: 12.5pt;" width="168">
            <p style="margin-right: 45.2pt; text-align: right;"><strong><span style="font-size: 10.0pt; font-family: 'Microsoft Sans Serif','sans-serif';">Service Provider</span></strong></p>
        </td>
        <td style="width: 140.0pt; padding: 0in 0in 0in 0in; height: 12.5pt;" width="187">
            <p><span style="font-size: 10.5pt;">&nbsp;</span></p>
        </td>
    </tr>
    <tr style="height: 17.0pt;">
        <td style="width: 224.0pt; padding: 0in 0in 0in 0in; height: 17.0pt;" width="299">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 1.75in; padding: 0in 0in 0in 0in; height: 17.0pt;" width="168">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 140.0pt; padding: 0in 0in 0in 0in; height: 17.0pt;" width="187">
            <p style="margin-left: 1.0pt;"><strong><span style="font-size: 10.0pt; font-family: 'Microsoft Sans Serif','sans-serif'; color: #85322f;">YOUR INTERNET BILL</span></strong></p>
        </td>
    </tr>
    <tr style="height: 11.65pt;">
        <td style="width: 224.0pt; padding: 0in 0in 0in 0in; height: 11.65pt;" width="299">
            <p style="margin-left: 12.0pt;"><span style="font-size: 7.0pt; font-family: 'Helvetica','sans-serif';">GST No : 07DSTPS2856E1Z4</span></p>
        </td>
        <td style="width: 1.75in; padding: 0in 0in 0in 0in; height: 11.65pt;" width="168">
            <p style="text-align: right;"><span style="font-size: 7.0pt; font-family: 'Helvetica','sans-serif';">GST NO :</span></p>
        </td>
        <td style="width: 140.0pt; padding: 0in 0in 0in 0in; height: 11.65pt;" width="187">
            <p><span style="font-size: 10.0pt;">&nbsp;</span></p>
        </td>
    </tr>
    <tr style="height: 21.8pt;">
        <td style="width: 224.0pt; padding: 0in 0in 0in 0in; height: 21.8pt;" width="299">
            <p style="margin-left: 12.0pt;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">CDR SHAMIT BISWAS</span></strong></p>
        </td>
        <td style="width: 1.75in; padding: 0in 0in 0in 0in; height: 21.8pt;" width="168">
            <p style="text-align: right;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">Invoice No :</span></strong></p>
        </td>
        <td style="width: 140.0pt; padding: 0in 0in 0in 0in; height: 21.8pt;" width="187">
            <p style="margin-left: 20.0pt;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">S201811768</span></strong></p>
        </td>
    </tr>
    <tr style="height: 15.65pt;">
        <td style="width: 224.0pt; padding: 0in 0in 0in 0in; height: 15.65pt;" width="299">
            <p style="margin-left: 12.0pt;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">P-6/8,TARAPORE OFFICER ENCLAVE,</span></strong></p>
        </td>
        <td style="width: 1.75in; padding: 0in 0in 0in 0in; height: 15.65pt;" width="168">
            <p style="text-align: right;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">Invoice Date :</span></strong></p>
        </td>
        <td style="width: 140.0pt; padding: 0in 0in 0in 0in; height: 15.65pt;" width="187">
            <p style="margin-left: 20.0pt;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">04 Sep 2018</span></strong></p>
        </td>
    </tr>
    <tr style="height: 11.8pt;">
        <td style="width: 224.0pt; padding: 0in 0in 0in 0in; height: 11.8pt;" width="299">
            <p><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">RANGPURI NEW DELHI 110070</span></strong></p>
        </td>
        <td style="width: 1.75in; padding: 0in 0in 0in 0in; height: 11.8pt;" width="168">
            <p><span style="font-size: 10.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 140.0pt; padding: 0in 0in 0in 0in; height: 11.8pt;" width="187">
            <p><span style="font-size: 10.0pt;">&nbsp;</span></p>
        </td>
    </tr>
    <tr style="height: 15.1pt;">
        <td style="width: 224.0pt; padding: 0in 0in 0in 0in; height: 15.1pt;" width="299">
            <p style="margin-left: 12.0pt;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">Mail Id :</span></strong></p>
        </td>
        <td style="width: 1.75in; padding: 0in 0in 0in 0in; height: 15.1pt;" width="168">
            <p style="text-align: right;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">Invoice period :</span></strong></p>
        </td>
        <td style="width: 140.0pt; padding: 0in 0in 0in 0in; height: 15.1pt;" width="187">
            <p style="margin-left: 20.0pt;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">01 Aug 2018-31 Aug 2018</span></strong></p>
        </td>
    </tr>
    <tr style="height: 15.55pt;">
        <td style="width: 224.0pt; padding: 0in 0in 0in 0in; height: 15.55pt;" width="299">
            <p style="margin-left: 12.0pt;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">Contact No : 9531844982/9933243393</span></strong></p>
        </td>
        <td style="width: 1.75in; padding: 0in 0in 0in 0in; height: 15.55pt;" width="168">
            <p style="text-align: right;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">Due Date :</span></strong></p>
        </td>
        <td style="width: 140.0pt; padding: 0in 0in 0in 0in; height: 15.55pt;" width="187">
            <p style="margin-left: 20.0pt;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">14 Sep 2018</span></strong></p>
        </td>
    </tr>
    <tr style="height: 15.1pt;">
        <td style="width: 224.0pt; padding: 0in 0in 0in 0in; height: 15.1pt;" width="299">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 1.75in; padding: 0in 0in 0in 0in; height: 15.1pt;" width="168">
            <p style="text-align: right;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">User Id :</span></strong></p>
        </td>
        <td style="width: 140.0pt; padding: 0in 0in 0in 0in; height: 15.1pt;" width="187">
            <p style="margin-left: 20.0pt;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">biswas68</span></strong></p>
        </td>
    </tr>
    <tr style="height: 17.0pt;">
        <td style="width: 224.0pt; padding: 0in 0in 0in 0in; height: 17.0pt;" width="299">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 1.75in; padding: 0in 0in 0in 0in; height: 17.0pt;" width="168">
            <p style="text-align: right;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">Customer Id :</span></strong></p>
        </td>
        <td style="width: 140.0pt; padding: 0in 0in 0in 0in; height: 17.0pt;" width="187">
            <p style="margin-left: 20.0pt;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">1174002507</span></strong></p>
        </td>
    </tr>
    </tbody>
</table>
<p style="line-height: 11.35pt;"><span style="font-size: 12.0pt;">&nbsp;</span></p>
<p style="margin-left: 71.0pt;"><strong><span style="font-size: 12.0pt; font-family: 'Helvetica','sans-serif'; color: white;">Bill Summary</span></strong></p>
<p style="line-height: 8.0pt;"><span style="font-size: 12.0pt;">&nbsp;</span></p>
<table style="margin-left: .5pt; border-collapse: collapse;">
    <tbody>
    <tr style="height: 15.2pt;">
        <td style="width: 73.0pt; border: solid #3D6B86 1.0pt; border-bottom: none; padding: 0in 0in 0in 0in; height: 15.2pt;" width="97">
            <p style="text-align: center;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">Previous</span></strong></p>
        </td>
        <td style="width: 24.0pt; border: none; border-right: solid #3D6B86 1.0pt; padding: 0in 0in 0in 0in; height: 15.2pt;" width="32">
            <p style="margin-right: 4.0pt; text-align: right; line-height: 15.2pt;"><strong><span style="font-size: 14.0pt; font-family: 'Helvetica','sans-serif'; color: red;">-</span></strong></p>
        </td>
        <td style="width: 1.0pt; border: none; border-top: solid #3D6B86 1.0pt; padding: 0in 0in 0in 0in; height: 15.2pt;" width="1">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 96.0pt; border-top: solid #3D6B86 1.0pt; border-left: none; border-bottom: none; border-right: solid #3D6B86 1.0pt; padding: 0in 0in 0in 0in; height: 15.2pt;" width="128">
            <p style="text-align: center;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">Payment</span></strong></p>
        </td>
        <td style="width: 26.0pt; border: none; border-right: solid #3D6B86 1.0pt; padding: 0in 0in 0in 0in; height: 15.2pt;" width="35">
            <p style="margin-right: 3.0pt; text-align: right; line-height: 15.2pt;"><strong><span style="font-size: 14.0pt; font-family: 'Helvetica','sans-serif'; color: lime;">+</span></strong></p>
        </td>
        <td style="width: 107.0pt; border: none; border-top: solid #3D6B86 1.0pt; padding: 0in 0in 0in 0in; height: 15.2pt;" width="143">
            <p style="text-align: center;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">Current Bill Amount</span></strong></p>
        </td>
        <td style="width: 27.0pt; border-top: none; border-left: solid #3D6B86 1.0pt; border-bottom: none; border-right: solid #3D6B86 1.0pt; padding: 0in 0in 0in 0in; height: 15.2pt;" width="36">
            <p style="margin-left: 5.0pt; line-height: 15.2pt;"><strong><span style="font-size: 14.0pt; font-family: 'Helvetica','sans-serif'; color: white;">A</span></strong><strong><span style="font-size: 14.0pt; font-family: 'Helvetica','sans-serif'; color: blue;">=</span></strong></p>
        </td>
        <td style="width: 106.0pt; border-top: solid #3D6B86 1.0pt; border-left: none; border-bottom: none; border-right: solid #3D6B86 1.0pt; padding: 0in 0in 0in 0in; height: 15.2pt;" width="141">
            <p style="text-align: center;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">Amount Payable By</span></strong></p>
        </td>
        <td style="width: 9.0pt; border: none; border-right: solid #3D6B86 1.0pt; padding: 0in 0in 0in 0in; height: 15.2pt;" width="12">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 82.0pt; border-top: solid #3D6B86 1.0pt; border-left: none; border-bottom: none; border-right: solid #3D6B86 1.0pt; background: red; padding: 0in 0in 0in 0in; height: 15.2pt;" width="109">
            <p style="text-align: center;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">Amount Payable</span></strong></p>
        </td>
        <td style="width: 0in; padding: 0in 0in 0in 0in; height: 15.2pt;" width="0">
            <p><span style="font-size: .5pt;">&nbsp;</span></p>
        </td>
    </tr>
    <tr style="height: 11.8pt;">
        <td style="width: 73.0pt; border-top: none; border-left: solid #3D6B86 1.0pt; border-bottom: none; border-right: solid #3D6B86 1.0pt; padding: 0in 0in 0in 0in; height: 11.8pt;" width="97">
            <p style="text-align: center;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">Dues</span></strong></p>
        </td>
        <td style="width: 24.0pt; border: none; border-right: solid #3D6B86 1.0pt; padding: 0in 0in 0in 0in; height: 11.8pt;" width="32">
            <p><span style="font-size: 10.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 1.0pt; padding: 0in 0in 0in 0in; height: 11.8pt;" width="1">
            <p><span style="font-size: 10.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 96.0pt; border: none; border-right: solid #3D6B86 1.0pt; padding: 0in 0in 0in 0in; height: 11.8pt;" width="128">
            <p><span style="font-size: 10.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 26.0pt; border: none; border-right: solid #3D6B86 1.0pt; padding: 0in 0in 0in 0in; height: 11.8pt;" width="35">
            <p><span style="font-size: 10.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 107.0pt; padding: 0in 0in 0in 0in; height: 11.8pt;" width="143">
            <p><span style="font-size: 10.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 27.0pt; border-top: none; border-left: solid #3D6B86 1.0pt; border-bottom: none; border-right: solid #3D6B86 1.0pt; padding: 0in 0in 0in 0in; height: 11.8pt;" width="36">
            <p><span style="font-size: 10.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 106.0pt; border: none; border-right: solid #3D6B86 1.0pt; padding: 0in 0in 0in 0in; height: 11.8pt;" width="141">
            <p style="text-align: center;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">Due Date</span></strong></p>
        </td>
        <td style="width: 9.0pt; border: none; border-right: solid #3D6B86 1.0pt; padding: 0in 0in 0in 0in; height: 11.8pt;" width="12">
            <p><span style="font-size: 10.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 82.0pt; border: none; border-right: solid #3D6B86 1.0pt; background: red; padding: 0in 0in 0in 0in; height: 11.8pt;" width="109">
            <p style="text-align: center;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">After Due Date</span></strong></p>
        </td>
        <td style="width: 0in; padding: 0in 0in 0in 0in; height: 11.8pt;" width="0">
            <p><span style="font-size: .5pt;">&nbsp;</span></p>
        </td>
    </tr>
    <tr style="height: 8.3pt;">
        <td style="width: 73.0pt; border: solid #3D6B86 1.0pt; border-top: none; padding: 0in 0in 0in 0in; height: 8.3pt;" width="97">
            <p><span style="font-size: 7.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 24.0pt; border: none; border-right: solid #3D6B86 1.0pt; padding: 0in 0in 0in 0in; height: 8.3pt;" width="32">
            <p><span style="font-size: 7.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 1.0pt; border: none; border-bottom: solid #3D6B86 1.0pt; padding: 0in 0in 0in 0in; height: 8.3pt;" width="1">
            <p><span style="font-size: 7.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 96.0pt; border-top: none; border-left: none; border-bottom: solid #3D6B86 1.0pt; border-right: solid #3D6B86 1.0pt; padding: 0in 0in 0in 0in; height: 8.3pt;" width="128">
            <p><span style="font-size: 7.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 26.0pt; border: none; border-right: solid #3D6B86 1.0pt; padding: 0in 0in 0in 0in; height: 8.3pt;" width="35">
            <p><span style="font-size: 7.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 107.0pt; border: none; border-bottom: solid #3D6B86 1.0pt; padding: 0in 0in 0in 0in; height: 8.3pt;" width="143">
            <p><span style="font-size: 7.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 27.0pt; border-top: none; border-left: solid #3D6B86 1.0pt; border-bottom: none; border-right: solid #3D6B86 1.0pt; padding: 0in 0in 0in 0in; height: 8.3pt;" width="36">
            <p><span style="font-size: 7.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 106.0pt; border-top: none; border-left: none; border-bottom: solid #3D6B86 1.0pt; border-right: solid #3D6B86 1.0pt; padding: 0in 0in 0in 0in; height: 8.3pt;" width="141">
            <p><span style="font-size: 7.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 9.0pt; border: none; border-right: solid #3D6B86 1.0pt; padding: 0in 0in 0in 0in; height: 8.3pt;" width="12">
            <p><span style="font-size: 7.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 82.0pt; border-top: none; border-left: none; border-bottom: solid #3D6B86 1.0pt; border-right: solid #3D6B86 1.0pt; background: red; padding: 0in 0in 0in 0in; height: 8.3pt;" width="109">
            <p><span style="font-size: 7.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 0in; padding: 0in 0in 0in 0in; height: 8.3pt;" width="0">
            <p><span style="font-size: .5pt;">&nbsp;</span></p>
        </td>
    </tr>
    <tr style="height: 14.15pt;">
        <td style="width: 73.0pt; border-top: none; border-left: solid #3D6B86 1.0pt; border-bottom: none; border-right: solid #3D6B86 1.0pt; padding: 0in 0in 0in 0in; height: 14.15pt;" width="97">
            <p style="text-align: center;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">0.00</span></strong></p>
        </td>
        <td style="width: 24.0pt; border: none; border-right: solid #3D6B86 1.0pt; padding: 0in 0in 0in 0in; height: 14.15pt;" width="32">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 1.0pt; padding: 0in 0in 0in 0in; height: 14.15pt;" width="1">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 96.0pt; border: none; border-right: solid #3D6B86 1.0pt; padding: 0in 0in 0in 0in; height: 14.15pt;" width="128">
            <p style="margin-right: 34.0pt; text-align: right;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">0.00</span></strong></p>
        </td>
        <td style="width: 26.0pt; border: none; border-right: solid teal 1.0pt; padding: 0in 0in 0in 0in; height: 14.15pt;" width="35">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 107.0pt; padding: 0in 0in 0in 0in; height: 14.15pt;" width="143">
            <p style="text-align: center;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">1,000.00</span></strong></p>
        </td>
        <td style="width: 27.0pt; border-top: none; border-left: solid teal 1.0pt; border-bottom: none; border-right: solid #3D6B86 1.0pt; padding: 0in 0in 0in 0in; height: 14.15pt;" width="36">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 106.0pt; border: none; border-right: solid #3D6B86 1.0pt; padding: 0in 0in 0in 0in; height: 14.15pt;" width="141">
            <p style="text-align: center;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">1,000.00</span></strong></p>
        </td>
        <td style="width: 9.0pt; border: none; border-right: solid #3D6B86 1.0pt; padding: 0in 0in 0in 0in; height: 14.15pt;" width="12">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 82.0pt; border-top: none; border-left: none; border-bottom: solid red 1.0pt; border-right: solid #3D6B86 1.0pt; background: red; padding: 0in 0in 0in 0in; height: 14.15pt;" width="109">
            <p style="text-align: center;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">1,050.00</span></strong></p>
        </td>
        <td style="width: 0in; padding: 0in 0in 0in 0in; height: 14.15pt;" width="0">
            <p><span style="font-size: .5pt;">&nbsp;</span></p>
        </td>
    </tr>
    <tr style="height: 10.0pt;">
        <td style="width: 73.0pt; border-top: solid #3D6B86 1.0pt; border-left: none; border-bottom: solid #3D6B86 1.0pt; border-right: none; padding: 0in 0in 0in 0in; height: 10.0pt;" width="97">
            <p><span style="font-size: 8.5pt;">&nbsp;</span></p>
        </td>
        <td style="width: 24.0pt; border: none; border-bottom: solid #3D6B86 1.0pt; padding: 0in 0in 0in 0in; height: 10.0pt;" width="32">
            <p><span style="font-size: 8.5pt;">&nbsp;</span></p>
        </td>
        <td style="width: 1.0pt; border-top: solid #3D6B86 1.0pt; border-left: none; border-bottom: solid #3D6B86 1.0pt; border-right: none; padding: 0in 0in 0in 0in; height: 10.0pt;" width="1">
            <p><span style="font-size: 8.5pt;">&nbsp;</span></p>
        </td>
        <td style="width: 96.0pt; border-top: solid #3D6B86 1.0pt; border-left: none; border-bottom: solid #3D6B86 1.0pt; border-right: none; padding: 0in 0in 0in 0in; height: 10.0pt;" width="128">
            <p><span style="font-size: 8.5pt;">&nbsp;</span></p>
        </td>
        <td style="width: 26.0pt; padding: 0in 0in 0in 0in; height: 10.0pt;" width="35">
            <p><span style="font-size: 8.5pt;">&nbsp;</span></p>
        </td>
        <td style="width: 107.0pt; border: none; border-top: solid teal 1.0pt; padding: 0in 0in 0in 0in; height: 10.0pt;" width="143">
            <p><span style="font-size: 8.5pt;">&nbsp;</span></p>
        </td>
        <td style="width: 27.0pt; border: none; border-bottom: solid windowtext 1.0pt; padding: 0in 0in 0in 0in; height: 10.0pt;" width="36">
            <p><span style="font-size: 8.5pt;">&nbsp;</span></p>
        </td>
        <td style="width: 106.0pt; border-top: solid #3D6B86 1.0pt; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: none; padding: 0in 0in 0in 0in; height: 10.0pt;" width="141">
            <p><span style="font-size: 8.5pt;">&nbsp;</span></p>
        </td>
        <td style="width: 9.0pt; border: none; border-bottom: solid windowtext 1.0pt; padding: 0in 0in 0in 0in; height: 10.0pt;" width="12">
            <p><span style="font-size: 8.5pt;">&nbsp;</span></p>
        </td>
        <td style="width: 82.0pt; border: none; border-bottom: solid windowtext 1.0pt; padding: 0in 0in 0in 0in; height: 10.0pt;" width="109">
            <p><span style="font-size: 8.5pt;">&nbsp;</span></p>
        </td>
        <td style="width: 0in; padding: 0in 0in 0in 0in; height: 10.0pt;" width="0">
            <p><span style="font-size: .5pt;">&nbsp;</span></p>
        </td>
    </tr>
    <tr style="height: 15.9pt;">
        <td style="width: 194.0pt; border: solid #3D6B86 1.0pt; border-top: none; background: #3D6B86; padding: 0in 0in 0in 0in; height: 15.9pt;" colspan="4" width="259">
            <p style="margin-left: 2.0pt;"><strong><span style="font-size: 12.0pt; font-family: 'Helvetica','sans-serif'; color: white;">CURRENT BILL DETAILS</span></strong></p>
        </td>
        <td style="width: 26.0pt; padding: 0in 0in 0in 0in; height: 15.9pt;" width="35">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 107.0pt; padding: 0in 0in 0in 0in; height: 15.9pt;" width="143">
            <p style="text-align: center;"><strong><span style="font-size: 12.0pt; font-family: 'Helvetica','sans-serif'; color: #5c5c5c;">Amount(Rs.)</span></strong></p>
        </td>
        <td style="width: 27.0pt; border: none; border-left: solid windowtext 1.0pt; padding: 0in 0in 0in 0in; height: 15.9pt;" width="36">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 106.0pt; padding: 0in 0in 0in 0in; height: 15.9pt;" width="141">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 9.0pt; padding: 0in 0in 0in 0in; height: 15.9pt;" width="12">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 82.0pt; border: none; border-right: solid windowtext 1.0pt; padding: 0in 0in 0in 0in; height: 15.9pt;" width="109">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 0in; padding: 0in 0in 0in 0in; height: 15.9pt;" width="0">
            <p><span style="font-size: .5pt;">&nbsp;</span></p>
        </td>
    </tr>
    <tr style="height: 18.3pt;">
        <td style="width: 73.0pt; padding: 0in 0in 0in 0in; height: 18.3pt;" width="97">
            <p style="margin-left: 2.0pt;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">Plane Name</span></strong></p>
        </td>
        <td style="width: 24.0pt; padding: 0in 0in 0in 0in; height: 18.3pt;" width="32">
            <p style="margin-right: 13.0pt; text-align: right;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">:</span></strong></p>
        </td>
        <td style="width: 1.0pt; padding: 0in 0in 0in 0in; height: 18.3pt;" width="1">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 122.0pt; padding: 0in 0in 0in 0in; height: 18.3pt;" colspan="2" width="163">
            <p style="margin-right: 7.0pt; text-align: right;"><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">50 mbps Unlimited Data</span></p>
        </td>
        <td style="width: 107.0pt; padding: 0in 0in 0in 0in; height: 18.3pt;" width="143">
            <p style="text-align: center;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">1000</span></strong></p>
        </td>
        <td style="width: 27.0pt; border: none; border-left: solid windowtext 1.0pt; padding: 0in 0in 0in 0in; height: 18.3pt;" width="36">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 106.0pt; padding: 0in 0in 0in 0in; height: 18.3pt;" width="141">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 9.0pt; padding: 0in 0in 0in 0in; height: 18.3pt;" width="12">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 82.0pt; border: none; border-right: solid windowtext 1.0pt; padding: 0in 0in 0in 0in; height: 18.3pt;" width="109">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 0in; padding: 0in 0in 0in 0in; height: 18.3pt;" width="0">
            <p><span style="font-size: .5pt;">&nbsp;</span></p>
        </td>
    </tr>
    <tr style="height: 17.0pt;">
        <td style="width: 73.0pt; padding: 0in 0in 0in 0in; height: 17.0pt;" width="97">
            <p style="margin-left: 2.0pt;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">Discount</span></strong></p>
        </td>
        <td style="width: 24.0pt; padding: 0in 0in 0in 0in; height: 17.0pt;" width="32">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 1.0pt; padding: 0in 0in 0in 0in; height: 17.0pt;" width="1">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 96.0pt; padding: 0in 0in 0in 0in; height: 17.0pt;" width="128">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 26.0pt; padding: 0in 0in 0in 0in; height: 17.0pt;" width="35">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 107.0pt; padding: 0in 0in 0in 0in; height: 17.0pt;" width="143">
            <p style="text-align: center;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">0.00</span></strong></p>
        </td>
        <td style="width: 27.0pt; border: none; border-left: solid windowtext 1.0pt; padding: 0in 0in 0in 0in; height: 17.0pt;" width="36">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 197.0pt; border: none; border-right: solid windowtext 1.0pt; padding: 0in 0in 0in 0in; height: 17.0pt;" colspan="3" width="263">
            <p style="margin-right: 21.0pt; text-align: center;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">Advertisement</span></strong></p>
        </td>
        <td style="width: 0in; padding: 0in 0in 0in 0in; height: 17.0pt;" width="0">
            <p><span style="font-size: .5pt;">&nbsp;</span></p>
        </td>
    </tr>
    <tr style="height: 19.0pt;">
        <td style="width: 194.0pt; padding: 0in 0in 0in 0in; height: 19.0pt;" colspan="4" width="259">
            <p style="margin-left: 2.0pt;"><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">01 Aug 2018-31 Aug 2018</span></p>
        </td>
        <td style="width: 26.0pt; padding: 0in 0in 0in 0in; height: 19.0pt;" width="35">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 107.0pt; padding: 0in 0in 0in 0in; height: 19.0pt;" width="143">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 27.0pt; border: none; border-left: solid windowtext 1.0pt; padding: 0in 0in 0in 0in; height: 19.0pt;" width="36">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 106.0pt; padding: 0in 0in 0in 0in; height: 19.0pt;" width="141">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 9.0pt; padding: 0in 0in 0in 0in; height: 19.0pt;" width="12">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 82.0pt; border: none; border-right: solid windowtext 1.0pt; padding: 0in 0in 0in 0in; height: 19.0pt;" width="109">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 0in; padding: 0in 0in 0in 0in; height: 19.0pt;" width="0">
            <p><span style="font-size: .5pt;">&nbsp;</span></p>
        </td>
    </tr>
    <tr style="height: 17.0pt;">
        <td style="width: 194.0pt; padding: 0in 0in 0in 0in; height: 17.0pt;" colspan="4" width="259">
            <p style="margin-left: 2.0pt;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">Other Charges &amp; Credits</span></strong></p>
        </td>
        <td style="width: 26.0pt; padding: 0in 0in 0in 0in; height: 17.0pt;" width="35">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 107.0pt; padding: 0in 0in 0in 0in; height: 17.0pt;" width="143">
            <p style="text-align: center;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">0.00</span></strong></p>
        </td>
        <td style="width: 27.0pt; border: none; border-left: solid windowtext 1.0pt; padding: 0in 0in 0in 0in; height: 17.0pt;" width="36">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 106.0pt; padding: 0in 0in 0in 0in; height: 17.0pt;" width="141">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 9.0pt; padding: 0in 0in 0in 0in; height: 17.0pt;" width="12">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 82.0pt; border: none; border-right: solid windowtext 1.0pt; padding: 0in 0in 0in 0in; height: 17.0pt;" width="109">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 0in; padding: 0in 0in 0in 0in; height: 17.0pt;" width="0">
            <p><span style="font-size: .5pt;">&nbsp;</span></p>
        </td>
    </tr>
    <tr style="height: 15.55pt;">
        <td style="width: 73.0pt; padding: 0in 0in 0in 0in; height: 15.55pt;" width="97">
            <p style="margin-left: 2.0pt;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">Installation</span></strong></p>
        </td>
        <td style="width: 24.0pt; padding: 0in 0in 0in 0in; height: 15.55pt;" width="32">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 1.0pt; padding: 0in 0in 0in 0in; height: 15.55pt;" width="1">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 96.0pt; padding: 0in 0in 0in 0in; height: 15.55pt;" width="128">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 26.0pt; padding: 0in 0in 0in 0in; height: 15.55pt;" width="35">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 107.0pt; padding: 0in 0in 0in 0in; height: 15.55pt;" width="143">
            <p style="text-align: center;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">0.00</span></strong></p>
        </td>
        <td style="width: 27.0pt; border: none; border-left: solid windowtext 1.0pt; padding: 0in 0in 0in 0in; height: 15.55pt;" width="36">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 106.0pt; padding: 0in 0in 0in 0in; height: 15.55pt;" width="141">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 9.0pt; padding: 0in 0in 0in 0in; height: 15.55pt;" width="12">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 82.0pt; border: none; border-right: solid windowtext 1.0pt; padding: 0in 0in 0in 0in; height: 15.55pt;" width="109">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 0in; padding: 0in 0in 0in 0in; height: 15.55pt;" width="0">
            <p><span style="font-size: .5pt;">&nbsp;</span></p>
        </td>
    </tr>
    <tr style="height: 15.5pt;">
        <td style="width: 73.0pt; padding: 0in 0in 0in 0in; height: 15.5pt;" width="97">
            <p style="margin-left: 2.0pt;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">Taxes</span></strong></p>
        </td>
        <td style="width: 24.0pt; padding: 0in 0in 0in 0in; height: 15.5pt;" width="32">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 1.0pt; padding: 0in 0in 0in 0in; height: 15.5pt;" width="1">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 96.0pt; padding: 0in 0in 0in 0in; height: 15.5pt;" width="128">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 26.0pt; padding: 0in 0in 0in 0in; height: 15.5pt;" width="35">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 107.0pt; padding: 0in 0in 0in 0in; height: 15.5pt;" width="143">
            <p style="text-align: center;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">0.00</span></strong></p>
        </td>
        <td style="width: 27.0pt; border: none; border-left: solid windowtext 1.0pt; padding: 0in 0in 0in 0in; height: 15.5pt;" width="36">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 197.0pt; border: none; border-right: solid windowtext 1.0pt; padding: 0in 0in 0in 0in; height: 15.5pt;" colspan="3" width="263">
            <p style="margin-right: 21.0pt; text-align: center;"><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">Manually customized</span></p>
        </td>
        <td style="width: 0in; padding: 0in 0in 0in 0in; height: 15.5pt;" width="0">
            <p><span style="font-size: .5pt;">&nbsp;</span></p>
        </td>
    </tr>
    <tr style="height: 15.55pt;">
        <td style="width: 98.0pt; padding: 0in 0in 0in 0in; height: 15.55pt;" colspan="3" width="131">
            <p style="margin-left: 2.0pt;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">Taxbale Amount</span></strong></p>
        </td>
        <td style="width: 96.0pt; padding: 0in 0in 0in 0in; height: 15.55pt;" width="128">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 26.0pt; padding: 0in 0in 0in 0in; height: 15.55pt;" width="35">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 107.0pt; padding: 0in 0in 0in 0in; height: 15.55pt;" width="143">
            <p style="text-align: center;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">847.46</span></strong></p>
        </td>
        <td style="width: 27.0pt; border: none; border-left: solid windowtext 1.0pt; padding: 0in 0in 0in 0in; height: 15.55pt;" width="36">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 106.0pt; padding: 0in 0in 0in 0in; height: 15.55pt;" width="141">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 9.0pt; padding: 0in 0in 0in 0in; height: 15.55pt;" width="12">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 82.0pt; border: none; border-right: solid windowtext 1.0pt; padding: 0in 0in 0in 0in; height: 15.55pt;" width="109">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 0in; padding: 0in 0in 0in 0in; height: 15.55pt;" width="0">
            <p><span style="font-size: .5pt;">&nbsp;</span></p>
        </td>
    </tr>
    <tr style="height: 17.0pt;">
        <td style="width: 73.0pt; padding: 0in 0in 0in 0in; height: 17.0pt;" width="97">
            <p style="margin-left: 2.0pt;"><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">( SGST 9% )</span></p>
        </td>
        <td style="width: 24.0pt; padding: 0in 0in 0in 0in; height: 17.0pt;" width="32">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 1.0pt; padding: 0in 0in 0in 0in; height: 17.0pt;" width="1">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 96.0pt; padding: 0in 0in 0in 0in; height: 17.0pt;" width="128">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 26.0pt; padding: 0in 0in 0in 0in; height: 17.0pt;" width="35">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 107.0pt; padding: 0in 0in 0in 0in; height: 17.0pt;" width="143">
            <p style="text-align: center;"><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">76.27</span></p>
        </td>
        <td style="width: 27.0pt; border: none; border-left: solid windowtext 1.0pt; padding: 0in 0in 0in 0in; height: 17.0pt;" width="36">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 106.0pt; padding: 0in 0in 0in 0in; height: 17.0pt;" width="141">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 9.0pt; padding: 0in 0in 0in 0in; height: 17.0pt;" width="12">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 82.0pt; border: none; border-right: solid windowtext 1.0pt; padding: 0in 0in 0in 0in; height: 17.0pt;" width="109">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 0in; padding: 0in 0in 0in 0in; height: 17.0pt;" width="0">
            <p><span style="font-size: .5pt;">&nbsp;</span></p>
        </td>
    </tr>
    <tr style="height: 15.1pt;">
        <td style="width: 73.0pt; padding: 0in 0in 0in 0in; height: 15.1pt;" width="97">
            <p style="margin-left: 2.0pt;"><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">( CGST 9% )</span></p>
        </td>
        <td style="width: 24.0pt; padding: 0in 0in 0in 0in; height: 15.1pt;" width="32">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 1.0pt; padding: 0in 0in 0in 0in; height: 15.1pt;" width="1">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 96.0pt; padding: 0in 0in 0in 0in; height: 15.1pt;" width="128">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 26.0pt; padding: 0in 0in 0in 0in; height: 15.1pt;" width="35">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 107.0pt; padding: 0in 0in 0in 0in; height: 15.1pt;" width="143">
            <p style="text-align: center;"><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">76.27</span></p>
        </td>
        <td style="width: 27.0pt; border: none; border-left: solid windowtext 1.0pt; padding: 0in 0in 0in 0in; height: 15.1pt;" width="36">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 106.0pt; padding: 0in 0in 0in 0in; height: 15.1pt;" width="141">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 9.0pt; padding: 0in 0in 0in 0in; height: 15.1pt;" width="12">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 82.0pt; border: none; border-right: solid windowtext 1.0pt; padding: 0in 0in 0in 0in; height: 15.1pt;" width="109">
            <p><span style="font-size: 12.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 0in; padding: 0in 0in 0in 0in; height: 15.1pt;" width="0">
            <p><span style="font-size: .5pt;">&nbsp;</span></p>
        </td>
    </tr>
    <tr style="height: 3.25pt;">
        <td style="width: 98.0pt; padding: 0in 0in 0in 0in; height: 3.25pt;" colspan="3" rowspan="2" width="131">
            <p style="margin-left: 2.0pt;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">Current Bill Amount</span></strong></p>
        </td>
        <td style="width: 96.0pt; padding: 0in 0in 0in 0in; height: 3.25pt;" width="128">
            <p><span style="font-size: 2.5pt;">&nbsp;</span></p>
        </td>
        <td style="width: 26.0pt; padding: 0in 0in 0in 0in; height: 3.25pt;" width="35">
            <p><span style="font-size: 2.5pt;">&nbsp;</span></p>
        </td>
        <td style="width: 107.0pt; padding: 0in 0in 0in 0in; height: 3.25pt;" rowspan="2" width="143">
            <p style="text-align: center;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">1,000.00</span></strong></p>
        </td>
        <td style="width: 27.0pt; border-top: none; border-left: solid windowtext 1.0pt; border-bottom: solid windowtext 1.0pt; border-right: none; padding: 0in 0in 0in 0in; height: 3.25pt;" width="36">
            <p><span style="font-size: 2.5pt;">&nbsp;</span></p>
        </td>
        <td style="width: 106.0pt; border: none; border-bottom: solid windowtext 1.0pt; padding: 0in 0in 0in 0in; height: 3.25pt;" width="141">
            <p><span style="font-size: 2.5pt;">&nbsp;</span></p>
        </td>
        <td style="width: 9.0pt; border: none; border-bottom: solid windowtext 1.0pt; padding: 0in 0in 0in 0in; height: 3.25pt;" width="12">
            <p><span style="font-size: 2.5pt;">&nbsp;</span></p>
        </td>
        <td style="width: 82.0pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; padding: 0in 0in 0in 0in; height: 3.25pt;" width="109">
            <p><span style="font-size: 2.5pt;">&nbsp;</span></p>
        </td>
        <td style="width: 0in; padding: 0in 0in 0in 0in; height: 3.25pt;" width="0">
            <p><span style="font-size: .5pt;">&nbsp;</span></p>
        </td>
    </tr>
    <tr style="height: 12.8pt;">
        <td style="width: 96.0pt; padding: 0in 0in 0in 0in; height: 12.8pt;" width="128">
            <p>&nbsp;</p>
        </td>
        <td style="width: 26.0pt; padding: 0in 0in 0in 0in; height: 12.8pt;" width="35">
            <p>&nbsp;</p>
        </td>
        <td style="width: 27.0pt; padding: 0in 0in 0in 0in; height: 12.8pt;" width="36">
            <p>&nbsp;</p>
        </td>
        <td style="width: 106.0pt; padding: 0in 0in 0in 0in; height: 12.8pt;" width="141">
            <p>&nbsp;</p>
        </td>
        <td style="width: 9.0pt; padding: 0in 0in 0in 0in; height: 12.8pt;" width="12">
            <p>&nbsp;</p>
        </td>
        <td style="width: 82.0pt; padding: 0in 0in 0in 0in; height: 12.8pt;" width="109">
            <p>&nbsp;</p>
        </td>
        <td style="width: 0in; padding: 0in 0in 0in 0in; height: 12.8pt;" width="0">
            <p><span style="font-size: .5pt;">&nbsp;</span></p>
        </td>
    </tr>
    <tr style="height: 7.45pt;">
        <td style="width: 73.0pt; border: none; border-bottom: solid windowtext 1.0pt; padding: 0in 0in 0in 0in; height: 7.45pt;" width="97">
            <p><span style="font-size: 6.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 24.0pt; border: none; border-bottom: solid windowtext 1.0pt; padding: 0in 0in 0in 0in; height: 7.45pt;" width="32">
            <p><span style="font-size: 6.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 1.0pt; border: none; border-bottom: solid windowtext 1.0pt; padding: 0in 0in 0in 0in; height: 7.45pt;" width="1">
            <p><span style="font-size: 6.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 96.0pt; border: none; border-bottom: solid windowtext 1.0pt; padding: 0in 0in 0in 0in; height: 7.45pt;" width="128">
            <p><span style="font-size: 6.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 26.0pt; border: none; border-bottom: solid windowtext 1.0pt; padding: 0in 0in 0in 0in; height: 7.45pt;" width="35">
            <p><span style="font-size: 6.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 107.0pt; border: none; border-bottom: solid windowtext 1.0pt; padding: 0in 0in 0in 0in; height: 7.45pt;" width="143">
            <p><span style="font-size: 6.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 27.0pt; border: none; border-bottom: solid windowtext 1.0pt; padding: 0in 0in 0in 0in; height: 7.45pt;" width="36">
            <p><span style="font-size: 6.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 106.0pt; border: none; border-bottom: solid windowtext 1.0pt; padding: 0in 0in 0in 0in; height: 7.45pt;" width="141">
            <p><span style="font-size: 6.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 9.0pt; border: none; border-bottom: solid windowtext 1.0pt; padding: 0in 0in 0in 0in; height: 7.45pt;" width="12">
            <p><span style="font-size: 6.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 82.0pt; border: none; border-bottom: solid windowtext 1.0pt; padding: 0in 0in 0in 0in; height: 7.45pt;" width="109">
            <p><span style="font-size: 6.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 0in; padding: 0in 0in 0in 0in; height: 7.45pt;" width="0">
            <p><span style="font-size: .5pt;">&nbsp;</span></p>
        </td>
    </tr>
    <tr style="height: 12.75pt;">
        <td style="width: 73.0pt; padding: 0in 0in 0in 0in; height: 12.75pt;" width="97">
            <p style="margin-left: 2.0pt;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">Guidelines:</span></strong></p>
        </td>
        <td style="width: 24.0pt; padding: 0in 0in 0in 0in; height: 12.75pt;" width="32">
            <p>&nbsp;</p>
        </td>
        <td style="width: 1.0pt; padding: 0in 0in 0in 0in; height: 12.75pt;" width="1">
            <p>&nbsp;</p>
        </td>
        <td style="width: 96.0pt; padding: 0in 0in 0in 0in; height: 12.75pt;" width="128">
            <p>&nbsp;</p>
        </td>
        <td style="width: 26.0pt; padding: 0in 0in 0in 0in; height: 12.75pt;" width="35">
            <p>&nbsp;</p>
        </td>
        <td style="width: 107.0pt; padding: 0in 0in 0in 0in; height: 12.75pt;" width="143">
            <p>&nbsp;</p>
        </td>
        <td style="width: 27.0pt; border: none; border-right: solid yellow 1.0pt; padding: 0in 0in 0in 0in; height: 12.75pt;" width="36">
            <p>&nbsp;</p>
        </td>
        <td style="width: 197.0pt; border: none; border-right: solid yellow 1.0pt; background: yellow; padding: 0in 0in 0in 0in; height: 12.75pt;" colspan="3" width="263">
            <p style="margin-left: 59.0pt;"><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">For Your Record</span></strong></p>
        </td>
        <td style="width: 0in; padding: 0in 0in 0in 0in; height: 12.75pt;" width="0">
            <p><span style="font-size: .5pt;">&nbsp;</span></p>
        </td>
    </tr>
    <tr style="height: 3.75pt;">
        <td style="width: 73.0pt; padding: 0in 0in 0in 0in; height: 3.75pt;" width="97">
            <p><span style="font-size: 3.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 24.0pt; padding: 0in 0in 0in 0in; height: 3.75pt;" width="32">
            <p><span style="font-size: 3.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 1.0pt; padding: 0in 0in 0in 0in; height: 3.75pt;" width="1">
            <p><span style="font-size: 3.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 96.0pt; padding: 0in 0in 0in 0in; height: 3.75pt;" width="128">
            <p><span style="font-size: 3.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 26.0pt; padding: 0in 0in 0in 0in; height: 3.75pt;" width="35">
            <p><span style="font-size: 3.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 107.0pt; padding: 0in 0in 0in 0in; height: 3.75pt;" width="143">
            <p><span style="font-size: 3.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 27.0pt; border: none; border-right: solid yellow 1.0pt; padding: 0in 0in 0in 0in; height: 3.75pt;" width="36">
            <p><span style="font-size: 3.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 106.0pt; border: none; border-right: solid yellow 1.0pt; background: yellow; padding: 0in 0in 0in 0in; height: 3.75pt;" width="141">
            <p><span style="font-size: 3.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 9.0pt; border: none; border-right: solid yellow 1.0pt; background: yellow; padding: 0in 0in 0in 0in; height: 3.75pt;" width="12">
            <p><span style="font-size: 3.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 82.0pt; border: none; border-right: solid yellow 1.0pt; background: yellow; padding: 0in 0in 0in 0in; height: 3.75pt;" width="109">
            <p><span style="font-size: 3.0pt;">&nbsp;</span></p>
        </td>
        <td style="width: 0in; padding: 0in 0in 0in 0in; height: 3.75pt;" width="0">
            <p><span style="font-size: .5pt;">&nbsp;</span></p>
        </td>
    </tr>
    </tbody>
</table>
<p><span style="font-size: 11.0pt; font-family: 'Times New Roman','serif';">&nbsp;</span></p>
<p style="line-height: 1.75pt;"><span style="font-size: 12.0pt;">&nbsp;</span></p>
<ol>
    <li style="margin-left: 10.0pt; text-indent: -7.55pt; tab-stops: 10.0pt;"><span style="font-size: 7.0pt; font-family: 'Helvetica','sans-serif';">For any quries and complaints call between 09:00 to 20:00 at tel. 9873135358.</span></li>
</ol>
<p style="line-height: 7.1pt;"><span style="font-size: 7.0pt; font-family: 'Helvetica','sans-serif';">&nbsp;</span></p>
<ol start="2">
    <li style="margin-left: 10.0pt; text-indent: -7.55pt; tab-stops: 10.0pt;"><span style="font-size: 7.0pt; font-family: 'Helvetica','sans-serif';">Installation charges are non refundable.</span></li>
</ol>
<p style="line-height: 8.9pt;"><span style="font-size: 7.0pt; font-family: 'Helvetica','sans-serif';">&nbsp;</span></p>
<ol start="3">
    <li style="margin-left: 10.0pt; text-indent: -7.55pt; tab-stops: 10.0pt;"><span style="font-size: 7.0pt; font-family: 'Helvetica','sans-serif';">Under any circumstances product(s) once will not be accepted back for any replacement or refunds.</span></li>
</ol>
<p style="line-height: 8.9pt;"><span style="font-size: 12.0pt;">&nbsp;</span></p>
<ol start="4">
    <li style="margin-left: 4.0pt;"><span style="font-size: 7.0pt; font-family: 'Times','serif';">4</span><span style="font-size: 7.0pt; font-family: 'Helvetica','sans-serif';">. Price subjected to change withount notice.</span></li>
</ol>
<p><span style="font-size: 12.0pt; font-family: 'Times New Roman','serif';">&nbsp;</span></p>
<p style="line-height: 1.0pt;"><span style="font-size: 12.0pt;">&nbsp;</span></p>
<p style="line-height: 15.95pt;"><span style="font-size: 12.0pt;">&nbsp;</span></p>
<p><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">Cheque No. _______________________</span></p>
<p style="line-height: 4.5pt;"><span style="font-size: 12.0pt;">&nbsp;</span></p>
<p><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">Bank_____________________________</span></p>
<p style="line-height: 4.5pt;"><span style="font-size: 12.0pt;">&nbsp;</span></p>
<p><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">Date _____________________________</span></p>
<p style="line-height: 6.3pt;"><span style="font-size: 12.0pt;">&nbsp;</span></p>
<p><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">Amount___________________________</span></p>
<p style="line-height: 5.5pt;"><span style="font-size: 12.0pt;">&nbsp;</span></p>
<p><span style="font-size: 11.0pt; font-family: 'Times New Roman','serif';">&nbsp;</span></p>
<p style="margin-left: 2.0pt;"><span style="font-size: 7.0pt; font-family: 'Helvetica','sans-serif';">The Star Internet, B-117, Vasant Kunj Enclave , Near Vasant Hospital, Vasant Kunj New Delhi - 110070.</span></p>
<p style="margin-left: 2.0pt; line-height: 97%;"><span style="font-size: 7.0pt; line-height: 97%; font-family: 'Helvetica','sans-serif';">Mob. - 9873135358 , 9910135358</span></p>
<p style="line-height: 6.05pt;"><span style="font-size: 12.0pt;">&nbsp;</span></p>
<table style="margin-left: 2.0pt; border-collapse: collapse;">
    <tbody>
    <tr style="height: 11.8pt;">
        <td style="width: 309.0pt; padding: 0in 0in 0in 0in; height: 11.8pt;" colspan="2" width="412">
            <p><strong><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif'; color: #3d6b86;">Payment Slip (Please attach this payment slip with your</span></strong></p>
        </td>
        <td style="width: 118.0pt; padding: 0in 0in 0in 0in; height: 11.8pt;" width="157">
            <p><span style="font-size: 10.0pt;">&nbsp;</span></p>
        </td>
    </tr>
    <tr style="height: 17.0pt;">
        <td style="width: 85.0pt; padding: 0in 0in 0in 0in; height: 17.0pt;" width="113">
            <p><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">Customer Name</span></p>
        </td>
        <td style="width: 224.0pt; padding: 0in 0in 0in 0in; height: 17.0pt;" width="299">
            <p style="margin-left: 12.0pt;"><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">CDR SHAMIT BISWAS</span></p>
        </td>
        <td style="width: 118.0pt; padding: 0in 0in 0in 0in; height: 17.0pt;" width="157">
            <p style="margin-left: 44.0pt;"><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">Cheque Number</span></p>
        </td>
    </tr>
    <tr style="height: 17.0pt;">
        <td style="width: 85.0pt; padding: 0in 0in 0in 0in; height: 17.0pt;" width="113">
            <p><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">Invoice No.</span></p>
        </td>
        <td style="width: 224.0pt; padding: 0in 0in 0in 0in; height: 17.0pt;" width="299">
            <p style="margin-left: 12.0pt;"><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">S201811768</span></p>
        </td>
        <td style="width: 118.0pt; padding: 0in 0in 0in 0in; height: 17.0pt;" width="157">
            <p style="margin-left: 44.0pt;"><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">Date</span></p>
        </td>
    </tr>
    <tr style="height: 17.05pt;">
        <td style="width: 85.0pt; padding: 0in 0in 0in 0in; height: 17.05pt;" width="113">
            <p><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">Amount Due</span></p>
        </td>
        <td style="width: 224.0pt; padding: 0in 0in 0in 0in; height: 17.05pt;" width="299">
            <p style="margin-left: 12.0pt;"><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">1,000.00</span></p>
        </td>
        <td style="width: 118.0pt; padding: 0in 0in 0in 0in; height: 17.05pt;" width="157">
            <p style="margin-left: 44.0pt;"><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">Bank &amp; Branch</span></p>
        </td>
    </tr>
    <tr style="height: 17.0pt;">
        <td style="width: 85.0pt; padding: 0in 0in 0in 0in; height: 17.0pt;" width="113">
            <p><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">Due Date</span></p>
        </td>
        <td style="width: 224.0pt; padding: 0in 0in 0in 0in; height: 17.0pt;" width="299">
            <p style="margin-left: 12.0pt;"><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">14 Sep 2018</span></p>
        </td>
        <td style="width: 118.0pt; padding: 0in 0in 0in 0in; height: 17.0pt;" width="157">
            <p style="margin-left: 44.0pt;"><span style="font-size: 10.0pt; font-family: 'Helvetica','sans-serif';">Amount</span></p>
        </td>
    </tr>
    </tbody>
</table>
<p style="line-height: 5.35pt;"><span style="font-size: 12.0pt;">&nbsp;</span></p>
<p style="margin-left: 2.0pt;"><strong><span style="font-size: 7.0pt; font-family: 'Helvetica','sans-serif';">*Crossed Cheque/DD should be drawn locally in favour of THE STAR INTERNET. Please mention your name, invoice no. on back of the cheque.</span></strong></p>

</body>

</html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even){background-color: #f2f2f2}

        th {
            background-color: #4CAF50;
            color: white;
        }

        #rcorners2 {
            /*display: inline;*/
            border-radius: 25px;
            border: 2px solid #73AD21;
            padding: 20px;
            width: 100px;
            height: 150px;
            float: left;
            margin: 39px;
        }
    </style>
    <title>Test bill</title>
</head>

<body>
<h1 style="text-align: center">Star Internet Bill </h1>

<div>
    <table>
        <tr>
            <th><?php echo $data['name']; ?></th>
            <th>Lastname</th>
            <th>Savings</th>
        </tr>
        <tr>
            <td>Peter</td>
            <td>Griffin</td>
            <td>$100</td>
        </tr>
        <tr>
            <td>Lois</td>
            <td>Griffin</td>
            <td>$150</td>
        </tr>
        <tr>
            <td>Joe</td>
            <td>Swanson</td>
            <td>$300</td>
        </tr>
        <tr>
            <td>Cleveland</td>
            <td>Brown</td>
            <td>$250</td>
        </tr>
    </table>

    <p id="rcorners2">Rounded corners!</p>
    <p id="rcorners2">Rounded corners!</p>
    <p id="rcorners2">Rounded corners!</p>

</div>

<table>

</table>

</body>
</html>
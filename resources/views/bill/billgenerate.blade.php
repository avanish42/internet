@extends('app')

@section('title','Clients')

@section('content')
    <!-- Main Content -->
    <div class="page-wrapper">
        <div class="container-fluid">

            <!-- Title -->
            <div class="row heading-bg">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h5 class="txt-dark">All Clients</h5>
                </div>
                <!-- Breadcrumb -->
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="index.html">Dashboard</a></li>
                        <li class="active"><span>clients</span></li>
                        {{--<li><a href="#"><span>Zone</span></a></li>--}}
                    </ol>
                </div>
                <!-- /Breadcrumb -->
            </div>
            <!-- /Title -->

            <!-- Row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default card-view" style="background-color: lightgrey">
                        <div class="panel-heading">
                            <div class="pull-left" >
                                <h6 class="panel-title txt-dark">Filter</h6>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="form-wrap">
                                    <form class="form-horizontal" method="post" action="{{URL::asset('filter-client')}}">
                                        <div class="form-group mb-0">
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    @csrf

                                                    <div class="col-sm-4">
                                                        <label class="control-label mb-10">Select Aera</label>
                                                        <select name="area_id" class="form-control rounded-input">

                                                            <option disabled selected value>-- Select Exicutive --</option>
                                                            @foreach($area as $ar)
                                                                <option value="{{$ar->id}}">{{$ar->name}}</option>
                                                            @endforeach
                                                        </select>

                                                    </div>

                                                    <div class="col-sm-4">
                                                        <label class="control-label mb-10">Select Zone*
                                                        </label>
                                                        <select  name="zone_id" class="form-control rounded-input"
                                                                 required>
                                                            <option disabled selected value>-- Select zone --</option>
                                                            @foreach($zones as $zone)
                                                                <option value="{{$zone->id}}">{{$zone->name}}</option>
                                                            @endforeach

                                                        </select>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-sm-4">

                                                    </div>

                                                    <div class="col-sm-2">
                                                        <div class="">
                                                            <label class="control-label mb-10"></label>
                                                            <input type="submit" value="Search" class="form-control

                                                        rounded-input btn btn-success" placeholder="Any
                                                        Remark/Description/Details.
                                                        ..">
                                                            {{--<button type="submit" class="btn btn-success btn-anim"><i--}}
                                                            {{--class="icon-arrow-right"></i><span class="btn-text">Add</span></button>--}}

                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <div class="">
                                                            <label class="control-label mb-10"></label>
                                                            <input type="reset" value="Reset" class="form-control
                                                        rounded-input btn btn-danger" placeholder="Any
                                                        Remark/Description/Details.
                                                        ..">
                                                            {{--<button type="submit" class="btn btn-success btn-anim"><i--}}
                                                            {{--class="icon-arrow-right"></i><span class="btn-text">Add</span></button>--}}

                                                        </div>
                                                    </div>

                                                    <div class="col-sm-4">

                                                    </div>



                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Row -->
            <!-- /Row -->





            <!-- Row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h6 class="panel-title txt-dark">All Plans</h6>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="table-wrap">
                                    <div class="table-responsive">
                                        <table id="datable_1" class="table jsgrid-table table-hover display  pb-30" >
                                            <thead>
                                            <tr>
                                                <th>S No</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Cutomer Type</th>
                                                <th>Plan</th>
                                                <th>Customer Id</th>
                                                <th>Card Id</th>
                                                <th>User Id</th>
                                                <th>Mobile</th>
                                                <th>discount</th>
                                                <th>GST No</th>
                                                <th>Joining Date</th>
                                                <th>Billing Date</th>



                                                <th>Pdf/Gst/Auto</th>
                                                <th>Created /Updated </th>
                                                <th>Action</th>
                                                <th>Status</th>
                                                <th>Profile</th>

                                            </tr>
                                            </thead>

                                            <tbody>
                                            @foreach($clients as $v)
                                                <tr>
                                                    <td>{{$v->id}}</td>
                                                    <td>{{$v->name}}</td>
                                                    <td>{{$v->email?$v->email:"N/A"}} </td>
                                                    <td>{{$v->coustmer_type}} </td>
                                                    <td>{{$v->plan->name}}</td>
                                                    <td>{{$v->customer_id?$v->customer_id:"N/A"}}</td>
                                                    <td>{{$v->card_id?$v->card_id:"N/A"}}</td>
                                                    <td>{{$v->user_id?$v->user_id:"N/A"}}</td>
                                                    <td>{{$v->mobile?$v->mobile:"N/A"}}</td>
                                                    <td>{{$v->discount?$v->discount:"N/A"}}</td>
                                                    <td>{{$v->gst_no?$v->gst_no:"N/A"}}</td>
                                                    <td>{{date('d-M-y',strtotime($v->joining_date)) }}</td>
                                                    <td>{{date('d-M-y',strtotime($v->billing_date)) }}</td>
                                                    {{--<td>{{$v->billing_date}}</td>--}}

                                                    <td style="width: 137px;">


                                                        {{--{{$v->is_pdf}}  /{{$v->is_gst}}/{{$v->auto_bill}}--}}
                                                        <?php
                                                        $gst_class=($v->is_gst)?"label-success":"label-danger";
                                                        $pdf_class=($v->is_pdf)?"label-success":"label-danger"  ;
                                                        $auto_class=($v->auto_bill) ?"label-success":"label-danger" ;
                                                        $igst_class=($v->is_igst) ?"label-success":"label-danger" ;
                                                        $gstinclude_class=($v->is_gst_in_plan) ? "label-success":"label-danger" ;
                                                        ?>

                                                        <span class="label <?php echo $pdf_class;  ?>">PDF</span>
                                                        / <span class="label <?php echo $gst_class;  ?>">GST</span>
                                                        / <span class="label <?php echo $auto_class;  ?>">Auto</span>
                                                        / <span class="label <?php echo $igst_class;  ?>">iGST</span>
                                                        / <span class="label <?php echo $gstinclude_class;
                                                        ?>">GST Include</span>


                                                    </td>

                                                    <td>{{ \Carbon\Carbon::parse($v->created_at)->diffForHumans()
                                                    }}/ {{ \Carbon\Carbon::parse($v->updated_at)->diffForHumans()
                                                    }}</td>

                                                    <td style="width: 90px;">

                                                        <button  class="btn btn-success btn-icon-anim btn-square
                                                    btn-xs"

                                                                 data-whatever="@mdo"><a href={{url('edit-client')
                                                                .'/'.$v->id}}><i class="fa
                                                                fa-pencil-square-o"></i></a> </button>



                                                        {{--<a  href={{URL::asset('zone')}} alt="alert"  id="sa-warning"--}}
                                                        {{--class="btn--}}
                                                        {{--btn-circle btn-danger img-responsive model_img"><i class="fa--}}
                                                        {{--fa-archive"></i></a>--}}
                                                    </td>
                                                    <?php $url1= 'change-status/'.$v->id; ?>
                                                    <td><button class="btn btn-success btn-icon-anim btn-square
                                                    btn-xs" onclick= "click_js_same('<?php echo $url1; ?>')" ><i
                                                                    class="icon-check"></i></button></td>
                                                    <td>
                                                        <?php $url= 'client-profile/'.$v->id; ?>
                                                        <button class="btn btn-default btn-icon-anim btn-square
                                                    btn-xs" onclick= "click_js('<?php echo $url; ?>')" ><i
                                                                    class="icon-user"></i></button></td>


                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Row -->




            @include('layout.footer')



        </div>
        <!-- /Main Content -->
    </div>

@endsection
<script>
    function click_js(url) {
        window.open(APP_URL+'/'+url);
        // console.log(APP_URL);
    }

    function  click_js_same(url) {

        window.location.href=(APP_URL+'/'+url);


    }

    var pagewrapper = document.getElementsByClassName("page-wrapper");
    // element.classList.remove("mystyle");
    // pagewrapper.style.height=null;


</script>

@push()

    <script>
        console.log('add page script');
    </script>
@endpush
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Style-Type" content="text/css"/>

    <title>Bill</title></head>

<style>
    img {
        display: -dompdf-image !important;
    }
</style>
<body>
<div>
    <img src="{{url('logo.jpeg' )}}" width="55" height="50" alt=""
          style="-aw-left-pos:18.04pt; -aw-rel-hpos:column; -aw-rel-vpos:paragraph; -aw-top-pos:2.86pt; -aw-wrap-type:none; margin-left:18.04pt; margin-top:2.86pt; position:absolute; z-index:0"/>
    <p style="margin:0pt 0pt 0pt 72pt; text-indent:36pt"><span
                style="font-family:Cambria; font-size:28pt; font-weight:bold">THE STAR INTERNET</span></p>
    <p style="margin:0pt;"><span style="font-family:Cambria; font-size:8pt;
    font-weight:bold">                                                                                                            </span><span
                style="font-family:Cambria; font-size:8pt; font-weight:bold">INTERNET SERVICE PROVIDER</span></p>
    <p style="margin:0pt"><span style="font-family:Cambria; font-size:11pt">&#xa0;</span></p>
    <p style="margin:0pt"><span style="font-family:Cambria; font-size:11pt">&#xa0;</span></p>
    <p style="margin:0pt 18pt 0pt 0pt"><span
                style="font-family:Cambria; font-size:11pt; font-weight:normal">Buyer </span><span
                style="width:5.32pt; text-indent:0pt; display:inline-block"></span><span
                style="width:36pt; text-indent:0pt; display:inline-block"></span><span
                style="width:36pt; text-indent:0pt; display:inline-block"></span><span
                style="width:36pt; text-indent:0pt; display:inline-block"></span><span
                style="width:36pt; text-indent:0pt; display:inline-block"></span><span
                style="width:36pt; text-indent:0pt; display:inline-block"></span><span
                style="width:36pt; text-indent:0pt; display:inline-block"></span><span
                style="width:36pt; text-indent:0pt; display:inline-block"></span><span
                style="width:36pt; text-indent:0pt; display:inline-block"></span><span
                style="width:36pt; text-indent:0pt; display:inline-block"></span><span
                style="width:36pt; text-indent:0pt; display:inline-block"></span><span
                style="font-family:Cambria; font-size:11pt; font-weight:normal">         </span><span
                style="color:#632423; font-family:Cambria; font-size:11pt; font-weight:normal">Y</span><span
                style="color:#632423; font-family:Cambria; font-size:11pt; font-weight:bold">OUR INTERNET BIL</span><span
                style="color:#632423; font-family:Cambria; font-size:11pt; font-weight:bold">L</span><span
                style="font-family:Cambria; font-size:11pt; font-weight:bold">  </span></p>
    <table cellspacing="0" cellpadding="0" style="border-collapse:collapse; margin-left:0pt; width:554.65pt">
        <tr style="height:13.9pt">
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:327.45pt"><p
                        style="margin:0pt 18pt 0pt 0pt"><span
                            style="font-family:Calibri; font-size:10pt; font-weight:normal">GST NO</span></p></td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:65.85pt"><p
                        style="margin:0pt 18pt 0pt 0pt"><span style="font-family:Calibri; font-size:10pt">GST NO</span>
                </p></td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:2.95pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:11pt">:</span></p></td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:115.2pt"><p
                        style="margin:0pt"><span style="font-family:Calibri;
                        font-size:10pt">{{$v->gst_no?$v->gst_no:"N/A"}}</span></p>
            </td>
        </tr>
        <tr style="height:13.9pt">
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:327.45pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:11pt;
                        font-weight:bold">{{$v->name}}</span>
                </p></td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:65.85pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:11pt">Invoice No :</span></p>
            </td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:2.95pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:11pt">:</span></p></td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:115.2pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:11pt">{{$v->invoice_no}}</span></p></td>
        </tr>
        <tr style="height:13pt">
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:327.45pt"><p
                        style="margin:0pt"><span
                            style="font-family:Calibri; font-size:11pt; font-weight:bold">{{$v->street}}</span><span
                            style="font-family:Calibri; font-size:11pt; font-weight:bold"></span><span
                            style="font-family:Calibri; font-size:11pt; font-weight:bold"> </span><span
                            style="font-family:Calibri; font-size:11pt; font-weight:bold"></span>
                </p></td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:65.85pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:11pt">Invoice Date</span></p>
            </td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:2.95pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:11pt">:</span></p></td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:115.2pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:11pt">{{$v->invoice_date}}</span></p></td>
        </tr>
        <tr style="height:12.1pt">
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:327.45pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:11pt">Mail Id :
                        {{$v->email?$v->email:"N/A"}}</span></p></td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:65.85pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:11pt">User Id</span></p></td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:2.95pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:11pt">:</span></p></td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:115.2pt"><p
                        style="margin:0pt"><span style="font-family:Calibri;
                        font-size:11pt">{{$v->user_id?$v->user_id:"N/A"}}</span></p></td>
        </tr>
        <tr style="height:12.1pt">
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:327.45pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:11pt">Contact No :
                        {{$v->mobile?$v->mobile:"N/A"}}</span><span
                            style="font-family:Calibri; font-size:11pt"></span></p></td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:65.85pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:11pt">Customer Id</span></p></td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:2.95pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:11pt">:</span></p></td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:115.2pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:11pt">{{$v->customer_id}}</span></p></td>
        </tr>
        <tr style="height:11.65pt">
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:327.45pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:11pt">&#xa0;</span></p></td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:65.85pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:11pt">Invoice period</span></p>
            </td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:2.95pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:11pt">:</span></p></td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:115.2pt"><p
                        style="margin:0pt"><span style="font-family:Calibri;
                        font-size:11pt">{{$v->start_date}}--{{$v->end_date}}</span>
                </p></td>
        </tr>
        <tr style="height:11.25pt">
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:327.45pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:11pt">&#xa0;</span></p></td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:65.85pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:11pt">Due Date</span></p></td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:2.95pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:11pt">:</span></p></td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:115.2pt"><p
                        style="margin:0pt"><span style=" background-color:#ff6699;font-weight: bold;
                        font-family:Calibri;
                        font-size:11pt">{{$v->due_date}}</span></p></td>
        </tr>
    </table>
    <p style="margin:0pt"><span
                style="font-family:'Times New Roman'; font-size:14pt; font-weight:bold; text-decoration:underline">BILL SUMMARY</span><span
                style="width:32.38pt; text-indent:0pt; font-family:'Lucida Console'; font-size:14pt; text-decoration:underline; display:inline-block">&#xa0;&#xa0;&#xa0;</span><span
                style="width:36pt; text-indent:0pt; font-family:'Lucida Console'; font-size:14pt; text-decoration:underline; display:inline-block">&#xa0;&#xa0;&#xa0;&#xa0;</span><span
                style="width:36pt; text-indent:0pt; font-family:'Lucida Console'; font-size:14pt; text-decoration:underline; display:inline-block">&#xa0;&#xa0;&#xa0;&#xa0;</span><span
                style="width:36pt; text-indent:0pt; font-family:'Lucida Console'; font-size:14pt; text-decoration:underline; display:inline-block">&#xa0;&#xa0;&#xa0;&#xa0;</span><span
                style="width:36pt; text-indent:0pt; font-family:'Lucida Console'; font-size:14pt; text-decoration:underline; display:inline-block">&#xa0;&#xa0;&#xa0;&#xa0;</span><span
                style="width:36pt; text-indent:0pt; font-family:'Lucida Console'; font-size:14pt; text-decoration:underline; display:inline-block">&#xa0;&#xa0;&#xa0;&#xa0;</span><span
                style="width:36pt; text-indent:0pt; font-family:'Lucida Console'; font-size:14pt; text-decoration:underline; display:inline-block">&#xa0;&#xa0;&#xa0;&#xa0;</span><span
                style="width:36pt; text-indent:0pt; font-family:'Lucida Console'; font-size:14pt; text-decoration:underline; display:inline-block">&#xa0;&#xa0;&#xa0;&#xa0;</span><span
                style="width:36pt; text-indent:0pt; font-family:'Lucida Console'; font-size:14pt; text-decoration:underline; display:inline-block">&#xa0;&#xa0;&#xa0;&#xa0;</span><span
                style="width:36pt; text-indent:0pt; font-family:'Lucida Console'; font-size:14pt; text-decoration:underline; display:inline-block">&#xa0;&#xa0;&#xa0;&#xa0;</span><span
                style="width:36pt; text-indent:0pt; font-family:'Lucida Console'; font-size:14pt; text-decoration:underline; display:inline-block">&#xa0;&#xa0;&#xa0;&#xa0;</span><span
                style="width:36pt; text-indent:0pt; font-family:'Lucida Console'; font-size:14pt; text-decoration:underline; display:inline-block">&#xa0;&#xa0;&#xa0;&#xa0;</span>
    </p>
    <p style="margin:0pt"><span style="font-family:'Times New Roman'; font-size:5pt; font-weight:bold">&#xa0;</span></p>
    <table cellspacing="0" cellpadding="0" style="border-collapse:collapse; margin-left:0pt; width:544pt">
        <tr style="height:30pt">
            <td style="background-color:#ddd9c3; border-bottom-color:#000000; border-bottom-style:solid; border-bottom-width:0.75pt; border-left-color:#000000; border-left-style:solid; border-left-width:1pt; border-right-color:#000000; border-right-style:solid; border-right-width:1pt; border-top-color:#000000; border-top-style:solid; border-top-width:1pt; padding-left:4.9pt; padding-right:4.9pt; vertical-align:middle; width:65.2pt">
                <p style="margin:0pt; text-align:center"><span
                            style="font-family:Calibri; font-size:11pt; font-weight:bold">Previous Dues</span></p></td>
            <td rowspan="2" style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:middle; width:7.2pt"><p
                        style="margin:0pt; text-align:center"><span
                            style="font-family:Calibri; font-size:11pt">&#xa0;</span></p></td>
            <td style="background-color:#ddd9c3; border-bottom-color:#000000; border-bottom-style:solid; border-bottom-width:0.75pt; border-left-color:#000000; border-left-style:solid; border-left-width:1pt; border-right-color:#000000; border-right-style:solid; border-right-width:1pt; border-top-color:#000000; border-top-style:solid; border-top-width:1pt; padding-left:4.9pt; padding-right:4.9pt; vertical-align:middle; width:65.2pt">
                <p style="margin:0pt; text-align:center"><span
                            style="font-family:Calibri; font-size:11pt; font-weight:bold">Payment</span></p></td>
            <td rowspan="2" style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:middle; width:7.2pt"><p
                        style="margin:0pt; text-align:center"><span
                            style="font-family:Calibri; font-size:11pt">&#xa0;</span></p></td>
            <td style="background-color:#dbeef3; border-bottom-color:#000000; border-bottom-style:solid; border-bottom-width:0.75pt; border-left-color:#000000; border-left-style:solid; border-left-width:1pt; border-right-color:#000000; border-right-style:solid; border-right-width:1pt; border-top-color:#000000; border-top-style:solid; border-top-width:1pt; padding-left:4.9pt; padding-right:4.9pt; vertical-align:middle; width:88.2pt">
                <p style="margin:0pt; text-align:center"><span
                            style="font-family:Calibri; font-size:11pt; font-weight:bold">Current Bill Amount</span></p>
            </td>
            <td rowspan="2" style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:middle; width:7.2pt"><p
                        style="margin:0pt; text-align:center"><span
                            style="font-family:Calibri; font-size:11pt">&#xa0;</span></p></td>
            <td style="background-color:#dbeef3; border-left-color:#000000; border-left-style:solid; border-left-width:1pt; border-right-color:#000000; border-right-style:solid; border-right-width:1pt; border-top-color:#000000; border-top-style:solid; border-top-width:1pt; padding-left:4.9pt; padding-right:4.9pt; vertical-align:middle; width:99.2pt">
                <p style="margin:0pt; text-align:center"><span
                            style="font-family:Calibri; font-size:11pt; font-weight:bold">Amount Payable ByDue Date</span>
                </p></td>
            <td rowspan="2" style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:middle; width:7.2pt"><p
                        style="margin:0pt; text-align:center"><span
                            style="font-family:Calibri; font-size:11pt">&#xa0;</span></p></td>
            <td style="background-color:#ff6699; border-left-color:#000000; border-left-style:solid; border-left-width:1pt; border-right-color:#000000; border-right-style:solid; border-right-width:1pt; border-top-color:#000000; border-top-style:solid; border-top-width:1pt; padding-left:4.9pt; padding-right:4.9pt; vertical-align:middle; width:99.2pt">
                <p style="margin:0pt; text-align:center"><span
                            style="font-family:Calibri; font-size:11pt; font-weight:bold">Amount Payable After Due Date</span>
                </p></td>
        </tr>
        <tr style="height:22pt">
            <td style="border-bottom-color:#000000; border-bottom-style:solid; border-bottom-width:1pt; border-left-color:#000000; border-left-style:solid; border-left-width:1pt; border-right-color:#000000; border-right-style:solid; border-right-width:1pt; padding-left:4.9pt; padding-right:4.9pt; vertical-align:middle; width:65.2pt">
                <p style="margin:0pt; text-align:center"><span
                            style="font-family:Calibri; font-size:12pt; font-weight:bold">0</span><span
                            style="font-family:Calibri; font-size:12pt; font-weight:bold">.00</span></p></td>
            <td style="border-bottom-color:#000000; border-bottom-style:solid; border-bottom-width:1pt; border-left-color:#000000; border-left-style:solid; border-left-width:1pt; border-right-color:#000000; border-right-style:solid; border-right-width:1pt; padding-left:4.9pt; padding-right:4.9pt; vertical-align:middle; width:65.2pt">
                <p style="margin:0pt; text-align:center"><span
                            style="font-family:Calibri; font-size:12pt; font-weight:bold">0</span><span
                            style="font-family:Calibri; font-size:12pt; font-weight:bold">.00</span></p></td>
            <td style="border-bottom-color:#000000; border-bottom-style:solid; border-bottom-width:1pt; border-left-color:#000000; border-left-style:solid; border-left-width:1pt; border-right-color:#000000; border-right-style:solid; border-right-width:1pt; padding-left:4.9pt; padding-right:4.9pt; vertical-align:middle; width:88.2pt">
                <p style="margin:0pt; text-align:center"><span
                            style="font-family:Calibri; font-size:12pt; font-weight:bold">{{$v->currnt_amount}}</span><span
                            style="font-family:Calibri; font-size:12pt; font-weight:bold"></span></p></td>
            <td style="border-bottom-color:#000000; border-bottom-style:solid; border-bottom-width:1pt; border-left-color:#000000; border-left-style:solid; border-left-width:1pt; border-right-color:#000000; border-right-style:solid; border-right-width:1pt; border-top-color:#000000; border-top-style:solid; border-top-width:0.75pt; padding-left:4.9pt; padding-right:4.9pt; vertical-align:middle; width:99.2pt">
                <p style="margin:0pt; text-align:center"><span
                            style="font-family:Calibri; font-size:12pt;
                            font-weight:bold">{{$v->currnt_amount}}</span><span
                            style="font-family:Calibri; font-size:12pt; font-weight:bold"></span></p></td>
            <td style="border-bottom-color:#000000; border-bottom-style:solid; border-bottom-width:1pt; border-left-color:#000000; border-left-style:solid; border-left-width:1pt; border-right-color:#000000; border-right-style:solid; border-right-width:1pt; border-top-color:#000000; border-top-style:solid; border-top-width:0.75pt; padding-left:4.9pt; padding-right:4.9pt; vertical-align:middle; width:99.2pt">
                <p style="margin:0pt; text-align:center"><span
                            style="font-family:Calibri; font-size:12pt;
                            font-weight:bold">{{$v->amount_with_penalty}}</span><span
                            style="font-family:Calibri; font-size:12pt; font-weight:bold"></span></p></td>
        </tr>
    </table>
    <p style="margin:0pt"><span
                style="width:36pt; text-indent:0pt; font-family:'Lucida Console'; font-size:6pt; text-decoration:underline; display:inline-block">&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;</span><span
                style="width:36pt; text-indent:0pt; font-family:'Lucida Console'; font-size:6pt; text-decoration:underline; display:inline-block">&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;</span><span
                style="width:36pt; text-indent:0pt; font-family:'Lucida Console'; font-size:6pt; text-decoration:underline; display:inline-block">&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;</span><span
                style="width:36pt; text-indent:0pt; font-family:'Lucida Console'; font-size:6pt; text-decoration:underline; display:inline-block">&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;</span><span
                style="width:36pt; text-indent:0pt; font-family:'Lucida Console'; font-size:6pt; text-decoration:underline; display:inline-block">&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;</span><span
                style="width:36pt; text-indent:0pt; font-family:'Lucida Console'; font-size:6pt; text-decoration:underline; display:inline-block">&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;</span><span
                style="width:36pt; text-indent:0pt; font-family:'Lucida Console'; font-size:6pt; text-decoration:underline; display:inline-block">&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;</span><span
                style="width:36pt; text-indent:0pt; font-family:'Lucida Console'; font-size:6pt; text-decoration:underline; display:inline-block">&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;</span><span
                style="width:36pt; text-indent:0pt; font-family:'Lucida Console'; font-size:6pt; text-decoration:underline; display:inline-block">&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;</span><span
                style="width:36pt; text-indent:0pt; font-family:'Lucida Console'; font-size:6pt; text-decoration:underline; display:inline-block">&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;</span><span
                style="width:36pt; text-indent:0pt; font-family:'Lucida Console'; font-size:6pt; text-decoration:underline; display:inline-block">&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;</span><span
                style="width:36pt; text-indent:0pt; font-family:'Lucida Console'; font-size:6pt; text-decoration:underline; display:inline-block">&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;</span><span
                style="width:36pt; text-indent:0pt; font-family:'Lucida Console'; font-size:6pt; text-decoration:underline; display:inline-block">&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;</span><span
                style="width:36pt; text-indent:0pt; font-family:'Lucida Console'; font-size:6pt; text-decoration:underline; display:inline-block">&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;</span><span
                style="width:36pt; text-indent:0pt; font-family:'Lucida Console'; font-size:6pt; text-decoration:underline; display:inline-block">&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;</span>
    </p>
    {{--<img src={{url('docs/fibernet1.png')}} width="279" height="246" alt=""--}}
             {{--style="-aw-left-pos:331.78pt; -aw-rel-hpos:column; -aw-rel-vpos:paragraph; -aw-top-pos:18.71pt; -aw-wrap-type:none; margin-left:331.78pt; margin-top:18.71pt; position:absolute; z-index:1"/>--}}
    <p style="margin:0pt"><span style="font-family:'Times New Roman'; font-size:14pt; font-weight:bold">CURRENT BILL DETAILS</span><span
                style="width:8.84pt; text-indent:0pt; display:inline-block"></span><span
                style="width:36pt; text-indent:0pt; display:inline-block"></span></p>
    <table cellspacing="0" cellpadding="0" style="border-collapse:collapse; margin-left:0pt; width:535pt">
        <tr style="height:15.75pt">
            <td colspan="3"
                style="background-color:#ddd9c3; border-bottom-color:#000000; border-bottom-style:solid; border-bottom-width:0.75pt; border-left-color:#000000; border-left-style:solid; border-left-width:0.75pt; border-right-color:#000000; border-right-style:solid; border-right-width:0.75pt; border-top-color:#000000; border-top-style:solid; border-top-width:0.75pt; padding-left:5.03pt; padding-right:5.03pt; vertical-align:middle; width:229.3pt">
                <p style="margin:0pt; text-align:center"><span
                            style="font-family:Calibri; font-size:12pt">Discription</span></p></td>
            <td style="background-color:#ddd9c3; border-bottom-color:#000000; border-bottom-style:solid; border-bottom-width:0.75pt; border-right-color:#000000; border-right-style:solid; border-right-width:0.75pt; border-top-color:#000000; border-top-style:solid; border-top-width:0.75pt; padding-left:5.4pt; padding-right:5.03pt; vertical-align:middle; width:69.35pt">
                <p style="margin:0pt; text-align:center"><span
                            style="font-family:Calibri; font-size:12pt">Amount (Rs)</span></p></td>
        </tr>
        <tr style="height:12pt">
            <td style="border-bottom-color:#000000; border-bottom-style:solid; border-bottom-width:0.75pt; border-left-color:#000000; border-left-style:solid; border-left-width:0.75pt; padding-left:5.03pt; padding-right:5.4pt; vertical-align:bottom; width:64.85pt">
                <p style="margin:0pt"><span style="font-family:Calibri; font-size:12pt">Plane Name</span></p></td>
            <td style="border-bottom-color:#000000; border-bottom-style:solid; border-bottom-width:0.75pt; padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:3.25pt">
                <p style="margin:0pt"><span style="font-family:Calibri; font-size:12pt">:</span></p></td>
            <td style="border-bottom-color:#000000; border-bottom-style:solid; border-bottom-width:0.75pt; padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:139.6pt">
                <p style="margin:0pt"><span style="font-family:Calibri;
                font-size:12pt">{{$v->plan->name?$v->plan->name:"N/A"}}</span>
                </p></td>
            <td style="border-bottom-color:#000000; border-bottom-style:solid; border-bottom-width:0.75pt; border-left-color:#000000; border-left-style:solid; border-left-width:0.75pt; border-right-color:#000000; border-right-style:solid; border-right-width:0.75pt; padding-left:5.03pt; padding-right:5.03pt; vertical-align:top; width:69.35pt">
                <p style="margin:0pt; text-align:center"><span
                            style="font-family:Calibri; font-size:12pt; font-weight:bold">{{$v->plan->price?$v->plan->price:"N/A"}}</span><span
                            style="font-family:Calibri; font-size:12pt; font-weight:bold">.00</span></p></td>
        </tr>
        <tr style="height:12pt">
            <td colspan="3"
                style="border-bottom-color:#000000; border-bottom-style:solid; border-bottom-width:0.75pt; border-left-color:#000000; border-left-style:solid; border-left-width:0.75pt; border-right-color:#000000; border-right-style:solid; border-right-width:0.75pt; border-top-color:#000000; border-top-style:solid; border-top-width:0.75pt; padding-left:5.03pt; padding-right:5.03pt; vertical-align:middle; width:229.3pt">
                <p style="margin:0pt"><span style="font-family:Calibri; font-size:12pt">Discount</span></p></td>
            <td style="border-bottom-color:#000000; border-bottom-style:solid; border-bottom-width:0.75pt; border-right-color:#000000; border-right-style:solid; border-right-width:0.75pt; padding-left:5.4pt; padding-right:5.03pt; vertical-align:top; width:69.35pt">
                <p style="margin:0pt; text-align:center"><span style="font-family:Calibri;
                font-size:12pt">{{$v->discount}}</span><span
                            style="font-family:Calibri; font-size:12pt"></span></p></td>
        </tr>
        <tr style="height:12pt">
            <td colspan="3"
                style="border-bottom-color:#000000; border-bottom-style:solid; border-bottom-width:0.75pt; border-left-color:#000000; border-left-style:solid; border-left-width:0.75pt; border-right-color:#000000; border-right-style:solid; border-right-width:0.75pt; border-top-color:#000000; border-top-style:solid; border-top-width:0.75pt; padding-left:5.03pt; padding-right:5.03pt; vertical-align:middle; width:229.3pt">
                <p style="margin:0pt"><span style="font-family:Calibri; font-size:12pt">Installation</span></p></td>
            <td style="border-bottom-color:#000000; border-bottom-style:solid; border-bottom-width:0.75pt; border-right-color:#000000; border-right-style:solid; border-right-width:0.75pt; padding-left:5.4pt; padding-right:5.03pt; vertical-align:top; width:69.35pt">
                <p style="margin:0pt; text-align:center"><span style="font-family:Calibri; font-size:12pt">0</span><span
                            style="font-family:Calibri; font-size:12pt">.00</span></p></td>
        </tr>
        <tr style="height:12pt">
            <td colspan="3"
                style="border-bottom-color:#000000; border-bottom-style:solid; border-bottom-width:0.75pt; border-left-color:#000000; border-left-style:solid; border-left-width:0.75pt; border-right-color:#000000; border-right-style:solid; border-right-width:0.75pt; border-top-color:#000000; border-top-style:solid; border-top-width:0.75pt; padding-left:5.03pt; padding-right:5.03pt; vertical-align:middle; width:229.3pt">
                <p style="margin:0pt"><span style="font-family:Calibri; font-size:12pt">Taxes( GST)</span></p></td>
            <td style="border-bottom-color:#000000; border-bottom-style:solid; border-bottom-width:0.75pt; border-right-color:#000000; border-right-style:solid; border-right-width:0.75pt; padding-left:5.4pt; padding-right:5.03pt; vertical-align:top; width:69.35pt">
                <p style="margin:0pt; text-align:center"><span style="font-family:Calibri; font-size:12pt">&#xa0;</span>
                </p></td>
        </tr>
        <tr style="height:12pt">
            <td colspan="3"
                style="border-bottom-color:#000000; border-bottom-style:solid; border-bottom-width:0.75pt; border-left-color:#000000; border-left-style:solid; border-left-width:0.75pt; border-right-color:#000000; border-right-style:solid; border-right-width:0.75pt; border-top-color:#000000; border-top-style:solid; border-top-width:0.75pt; padding-left:5.03pt; padding-right:5.03pt; vertical-align:middle; width:229.3pt">
                <p style="margin:0pt"><span style="font-family:Calibri; font-size:12pt">Taxable</span><span
                            style="font-family:Calibri; font-size:12pt"> Amount</span></p></td>
            <td style="border-bottom-color:#000000; border-bottom-style:solid; border-bottom-width:0.75pt; border-right-color:#000000; border-right-style:solid; border-right-width:0.75pt; padding-left:5.4pt; padding-right:5.03pt; vertical-align:top; width:69.35pt">
                <p style="margin:0pt; text-align:center"><span
                            style="font-family:Arial; font-size:12pt; font-weight:bold">{{$v->taxable_amount}}</span></p></td>
        </tr>
        <tr style="height:12pt">
            <td colspan="3"
                style="border-bottom-color:#000000; border-bottom-style:solid; border-bottom-width:0.75pt; border-left-color:#000000; border-left-style:solid; border-left-width:0.75pt; border-right-color:#000000; border-right-style:solid; border-right-width:0.75pt; border-top-color:#000000; border-top-style:solid; border-top-width:0.75pt; padding-left:5.03pt; padding-right:5.03pt; vertical-align:middle; width:229.3pt">
                <p style="margin:0pt"><span style="font-family:Calibri; font-size:12pt">( SGST 9% )</span></p></td>
            <td style="border-bottom-color:#000000; border-bottom-style:solid; border-bottom-width:0.75pt; border-right-color:#000000; border-right-style:solid; border-right-width:0.75pt; padding-left:5.4pt; padding-right:5.03pt; vertical-align:top; width:69.35pt">
                <p style="margin:0pt; text-align:center"><span
                            style="font-family:Arial; font-size:12pt;
                            font-weight:normal">{{$v->sgst?$v->sgst:"N/A"}}</span><span
                            style="font-family:Arial; font-size:12pt; font-weight:bold">&#xa0;</span></p></td>
        </tr>
        <tr style="height:12pt">
            <td colspan="3"
                style="border-bottom-color:#000000; border-bottom-style:solid; border-bottom-width:0.75pt; border-left-color:#000000; border-left-style:solid; border-left-width:0.75pt; border-right-color:#000000; border-right-style:solid; border-right-width:0.75pt; border-top-color:#000000; border-top-style:solid; border-top-width:0.75pt; padding-left:5.03pt; padding-right:5.03pt; vertical-align:middle; width:229.3pt">
                <p style="margin:0pt"><span style="font-family:Calibri; font-size:12pt">( CGST 9% )</span></p></td>
            <td style="border-bottom-color:#000000; border-bottom-style:solid; border-bottom-width:0.75pt; border-right-color:#000000; border-right-style:solid; border-right-width:0.75pt; padding-left:5.4pt; padding-right:5.03pt; vertical-align:middle; width:69.35pt">
                <p style="margin:0pt; text-align:center"><span style="font-family:Arial; font-size:12pt">{{$v->sgst?$v->sgst:"N/A"}}</span>
                </p></td>
        </tr>
        <tr style="height:12pt">
            <td colspan="3"
                style="border-bottom-color:#000000; border-bottom-style:solid; border-bottom-width:0.75pt; border-left-color:#000000; border-left-style:solid; border-left-width:0.75pt; border-right-color:#000000; border-right-style:solid; border-right-width:0.75pt; border-top-color:#000000; border-top-style:solid; border-top-width:0.75pt; padding-left:5.03pt; padding-right:5.03pt; vertical-align:middle; width:229.3pt">
                <p style="margin:0pt"><span style="font-family:Calibri; font-size:12pt">( IGST 18% )</span></p></td>
            <td style="border-bottom-color:#000000; border-bottom-style:solid; border-bottom-width:0.75pt; border-right-color:#000000; border-right-style:solid; border-right-width:0.75pt; padding-left:5.4pt; padding-right:5.03pt; vertical-align:middle; width:69.35pt">
                <p style="margin:0pt; text-align:center"><span style="font-family:Arial; font-size:12pt">-</span></p>
            </td>
        </tr>
        <tr style="height:12pt">
            <td colspan="3"
                style="border-bottom-color:#000000; border-bottom-style:solid; border-bottom-width:0.75pt; border-left-color:#000000; border-left-style:solid; border-left-width:0.75pt; border-right-color:#000000; border-right-style:solid; border-right-width:0.75pt; border-top-color:#000000; border-top-style:solid; border-top-width:0.75pt; padding-left:5.03pt; padding-right:5.03pt; vertical-align:middle; width:229.3pt">
                <p style="margin:0pt"><span style="font-family:Calibri; font-size:12pt">TOTAL TAX AMOUNT</span></p></td>
            <td style="border-bottom-color:#000000; border-bottom-style:solid; border-bottom-width:0.75pt; border-right-color:#000000; border-right-style:solid; border-right-width:0.75pt; padding-left:5.4pt; padding-right:5.03pt; vertical-align:middle; width:69.35pt">
                <p style="margin:0pt; text-align:center"><span
                            style="font-family:Arial; font-size:12pt; font-weight:bold">{{$v->total_tax}}</span></p></td>
        </tr>
        <tr style="height:12pt">
            <td colspan="3"
                style="border-bottom-color:#000000; border-bottom-style:solid; border-bottom-width:0.75pt; border-left-color:#000000; border-left-style:solid; border-left-width:0.75pt; border-right-color:#000000; border-right-style:solid; border-right-width:0.75pt; border-top-color:#000000; border-top-style:solid; border-top-width:0.75pt; padding-left:5.03pt; padding-right:5.03pt; vertical-align:middle; width:229.3pt">
                <p style="margin:0pt"><span style="font-family:Calibri; font-size:12pt">round off(- +)</span></p></td>
            <td style="border-bottom-color:#000000; border-bottom-style:solid; border-bottom-width:0.75pt; border-right-color:#000000; border-right-style:solid; border-right-width:0.75pt; padding-left:5.4pt; padding-right:5.03pt; vertical-align:top; width:69.35pt">
                <p style="margin:0pt; text-align:center"><span
                            style="font-family:Calibri; font-size:12pt">0.00</span><span
                            style="font-family:Calibri; font-size:12pt">&#xa0;</span></p></td>
        </tr>
        <tr style="height:12pt">
            <td colspan="3"
                style="background-color:#f2f2f2; border-bottom-color:#000000; border-bottom-style:solid; border-bottom-width:0.75pt; border-left-color:#000000; border-left-style:solid; border-left-width:0.75pt; border-right-color:#000000; border-right-style:solid; border-right-width:0.75pt; border-top-color:#000000; border-top-style:solid; border-top-width:0.75pt; padding-left:5.03pt; padding-right:5.03pt; vertical-align:middle; width:229.3pt">
                <p style="margin:0pt"><span style="font-family:Calibri; font-size:12pt">TOTAL AMOUNT TO BE PAID </span>
                </p></td>
            <td style="background-color:#f2f2f2; border-bottom-color:#000000; border-bottom-style:solid; border-bottom-width:0.75pt; border-right-color:#000000; border-right-style:solid; border-right-width:0.75pt; padding-left:5.4pt; padding-right:5.03pt; vertical-align:top; width:69.35pt">
                <p style="margin:0pt; text-align:center"><span
                            style="font-family:Arial; font-size:12pt; font-weight:bold">{{$v->total_amount_paid}}</span><span
                            style="font-family:Arial; font-size:12pt; font-weight:bold"></span></p></td>
        </tr>
        <tr style="height:0pt">
            <td style="width:75.65pt; border:none"></td>
            <td style="width:14.05pt; border:none"></td>
            <td style="width:150.4pt; border:none"></td>
            <td style="width:80.15pt; border:none"></td>
        </tr>
    </table>
    <p style="margin:0pt"><span style="font-family:Helvetica; font-size:6pt; font-weight:bold">&#xa0;</span></p>
    <p style="margin:0pt"><span style="font-family:Helvetica; font-size:10pt; font-weight:bold">In Words
            :{{ucwords($v->currnt_amount_word) }}</span>
    </p>
    <p style="margin:0pt"><span style="font-family:Helvetica; font-size:10pt; font-weight:bold">&#xa0;</span></p>
    <table cellspacing="0" cellpadding="0" style="border-collapse:collapse; float:left; margin:0pt 9pt; width:286.45pt">
        <tr style="height:4.35pt">
            <td colspan="3" style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:275.65pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:9pt; text-decoration:underline">Company’s Bank Details</span>
                </p></td>
        </tr>
        <tr style="height:4.35pt">
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:57.6pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:9pt">Bank Name </span></p></td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:8.3pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:9pt">:</span></p></td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:188.15pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:9pt">HDFC BANK</span></p></td>
        </tr>
        <tr style="height:4.35pt">
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:57.6pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:9pt">ACCOUNT NO</span></p></td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:8.3pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:9pt">:</span></p></td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:188.15pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:9pt">000000000000</span></p></td>
        </tr>
        <tr style="height:4.35pt">
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:57.6pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:9pt">IFS Code</span></p></td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:8.3pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:9pt">:</span></p></td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:188.15pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:9pt">HDFC0009117</span></p></td>
        </tr>
        <tr style="height:4.35pt">
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:57.6pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:9pt">Branch </span></p></td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:8.3pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:9pt">:</span></p></td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:188.15pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:9pt">VASANT KUNJ PRESIDENTIAL BUSINESS PARK</span>
                </p></td>
        </tr>
        <tr style="height:0pt">
            <td style="width:68.4pt; border:none"></td>
            <td style="width:19.1pt; border:none"></td>
            <td style="width:198.95pt; border:none"></td>
        </tr>
    </table>
    <p style="margin:0pt"><span style="font-family:Helvetica; font-size:10pt; font-weight:bold"> </span><span
                style="font-family:Helvetica; font-size:10pt; font-weight:bold">Guidelines:</span></p>
    <p style="margin:0pt 0pt 0pt 36pt; text-indent:-18pt"><span
                style="font-family:Wingdings; font-size:8pt"></span><span style="font:7.0pt 'Times New Roman'">&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0; </span><span
                style="font-family:Helvetica; font-size:8pt">For any quires and complaints  all between</span></p>
    <p style="margin:0pt 0pt 0pt 36pt; text-indent:-18pt"><span
                style="font-family:Wingdings; font-size:8pt"><span
                style="font-family:Helvetica; font-size:8pt"> &#xa0;&#xa0;&#xa0;&#xa0; 10:00 to 20:00 at Ph</span><span
                style="font-family:Helvetica; font-size:8pt">.. 9873135358,9910135358</span></p>

    <p style="margin:0pt 0pt 0pt 36pt; text-indent:-18pt"><span
                style="font-family:Wingdings; font-size:8pt"></span><span style="font:7.0pt 'Times New Roman'">&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0; </span><span
                style="font-family:Helvetica; font-size:8pt">Installation charges are non refundable.</span></p>
    <p style="margin:0pt 0pt 0pt 36pt; text-indent:-18pt"><span
                style="font-family:Wingdings; font-size:8pt"></span><span style="font:7.0pt 'Times New Roman'">&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0; </span><span
                style="font-family:Helvetica; font-size:8pt">No warranty on Physical Damage &amp; Burnt items.</span>
    </p>
    <p style="margin:0pt 0pt 0pt 36pt; text-indent:-18pt"><span
                style="font-family:Wingdings; font-size:8pt"></span><span style="font:7.0pt 'Times New Roman'">&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0; </span><span
                style="font-family:Helvetica; font-size:7pt">Price subjected to change withount notice</span></p>
    {{--<p style="margin:0pt 0pt 0pt 36pt; text-indent:-18pt"><span--}}
                {{--style="font-family:Wingdings; font-size:8pt"></span><span style="font:7.0pt 'Times New Roman'">&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0; </span><span--}}
                {{--style="font-family:Helvetica; font-size:8pt">All disputes subject to Delhi jurisdiction only.</span></p>--}}


    <p style="margin:0pt"><span style="font-family:'Times New Roman'; font-size:11pt">……………………………………………………………………………………………………………………………….......</span>
    </p>
    <table cellspacing="0" cellpadding="0" style="border-collapse:collapse; margin-left:0pt; width:577pt">
        <tr style="height:12pt">
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:64.2pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:9pt">Customer Name</span></p>
            </td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:2.7pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:9pt">:</span></p></td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:184.65pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:9pt">{{$v->name}}</span></p>
            </td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:127.75pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:9pt">Cheque Number</span></p>
            </td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:2.7pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:9pt">:</span></p></td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:130.2pt"><p
                        style="margin:0pt"><span
                            style="font-family:Calibri; font-size:9pt">________________________</span></p></td>
        </tr>
        <tr style="height:15pt">
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:64.2pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:9pt">Invoice No.</span></p></td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:2.7pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:9pt">:</span></p></td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:184.65pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:9pt">{{$v->invoice_no}}</span></p></td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:127.75pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:9pt">Date</span></p></td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:2.7pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:9pt">:</span></p></td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:130.2pt"><p
                        style="margin:0pt"><span
                            style="font-family:Calibri; font-size:9pt">________________________</span></p></td>
        </tr>
        <tr style="height:15pt">
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:64.2pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:9pt">Amount Due </span></p></td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:2.7pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:9pt">:</span></p></td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:184.65pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:9pt ">{{
                        $v->currnt_amount}}</span></p></td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:127.75pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:9pt">Bank &amp; Branch</span></p>
            </td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:2.7pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:9pt">:</span></p></td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:130.2pt"><p
                        style="margin:0pt"><span
                            style="font-family:Calibri; font-size:9pt">________________________</span></p></td>
        </tr>
        <tr style="height:15pt">
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:64.2pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:9pt">Due Date </span></p></td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:2.7pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:9pt">:</span></p></td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:184.65pt"><p
                        style="margin:0pt"><span style="font-family:Arial;font-weight: bold; font-size:7pt;
                       background-color:#ff6699;">{{
                        $v->due_date}}</span></p></td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:127.75pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:9pt">Amount</span></p></td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:2.7pt"><p
                        style="margin:0pt"><span style="font-family:Calibri; font-size:9pt">:</span></p></td>
            <td style="padding-left:5.4pt; padding-right:5.4pt; vertical-align:bottom; width:130.2pt"><p
                        style="margin:0pt"><span
                            style="font-family:Calibri; font-size:9pt">________________________</span></p></td>
        </tr>
    </table>
    <p style="margin:0pt"><span style="font-family:Helvetica; font-size:7pt; font-weight:bold">&#xa0;</span></p>
    <p style="margin:0pt"><span style="font-family:Helvetica; font-size:7pt; font-weight:bold">&#xa0;</span></p>
    <p style="margin:0pt"><span style="font-family:Helvetica; font-size:7pt; font-weight:bold">Crossed Cheque/DD should be drawn locally in favour of THE STAR INTERNET. Please mention your name, invoice no. on back of the cheque</span>
    </p></div>
<div class="docpe" style="position: absolute;color: white;margin-left:-450;">
    <a target="_blank" href="http://www.docsoso.com">DocSoSo.com -- Free Online Document Converter</a>
</div>
</body>
</html>

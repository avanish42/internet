<!DOCTYPE html>
<html lang="en">
<head>
    <title></title>
    <meta charset="utf-8">
    {{--<meta name="viewport" content="width=device-width, initial-scale=1">--}}
    {{--<link rel="stylesheet" href="css/bootstrap.min.css">--}}
    <link rel="stylesheet" type="text/css" href ={{URL::asset("bower_components/bootstrap/dist/css/bootstrap.css")}}
    ></link>
    <!-- jQuery -->
    <script src={{URL::asset("bower_components/jquery/dist/jquery.js")}} ></script>
{{--<script src={{URL::asset("vendors/bower_components/jquery/dist/jquery.min.js")}} ></script>--}}

<!-- Bootstrap Core JavaScript -->
    <script src={{URL::asset("bower_components/bootstrap/dist/js/bootstrap.js")}} ></script>
    <script src={{URL::asset("bower_components/jspdf/dist/jspdf.min.js")}} ></script>
    <script src={{URL::asset("bower_components/jspdf/dist/from_html.js")}} ></script>
    <script src={{URL::asset("bower_components/jspdf/dist/html2canvas.min.js")}} ></script>
    <script src={{URL::asset("bower_components/jspdf/dist/html.js")}} ></script>
    <script src={{URL::asset("bower_components/jspdf/dist/split_text_to_size.js")}} ></script>
    <script src={{URL::asset("bower_components/jspdf/dist/standard_fonts_metrics.js")}} ></script>






    <style>
        .largetext{font-weight:bold; font-size:24px; line-height:30px; font-family:calibri;}
        .mediumtext{font-weight:bold; font-size:20px; line-height:30px; font-family:calibri;}
        .mediumtextbrown{font-weight:bold; font-size:20px; line-height:30px; font-family:calibri; color:#630; text-align:right;}
        .smalltext{font-weight:bold; font-size:16px; line-height:30px; font-family:calibri;}
        .extrasmalltext{font-size:16px; line-height:30px; font-family:calibri;}
        .largeheadertext{font-size:24px; color:#fff; background:#3d6b86; padding-left:35px; text-align:left; width:50%;  }

        .box {
            width: 200px;
            float:left;
            border: 2px solid green;
            padding:0px;
            margin: 10px;
        }

    </style>



</head>
<body id="content">

<div class="container"  id="genrate-pdf" >

    <table class="table table-bordered " style="margin-top:50px;"  >
        <thead>
        <tr>

            <th colspan="8" class="h2 text-center" style="vertical-align:middle; font-weight:600; ">
                <img src={{url('docs/bill_image/logo.jpg')}} width="80" height="80" style="float:left;">THE STAR
                INTERNET<br
                /><small>Internet Service Provider</small></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td colspan="8" class="h4 mediumtextbrown">Tax Invoice</td>

        </tr>
        <tr>
            <td  colspan="5" class="extrasmalltext">GST NO : 27AAJFC1420H1ZT</td>
            <td colspan="3" class="extrasmalltext">GST No : 07DSTPS2856E1Z4</td>
        </tr>
        <tr>
            <td  colspan="5" class="mediumtext">CASTER PROJECT<br />
                8075 C8 VASANT KUNJ<br />
                Mail Id : premprakashcool92@gmail.com<br />
                Contact No : 9990283072</td>


            <td  colspan="3" class="mediumtext">Invoice No : S201811413<br />
                Invoice Date : 07 Jul 2018<br />
                Invoice period : 01 Jul 2018-31 Jul 2018<br />
                Due Date : 17 Jul 2018<br />
                User Id : caster<br />
                Customer Id : 8912001013</td>
        </tr>


        <tr>
            <td colspan="8"> <div class="largeheadertext" >Bill Summary</div></td>
        </tr>

        <tr>

            <td class="mediumtext  text-center">Previous Dues </td>
            <td rowspan="2" class="mediumtext text-center" style="color:#ff0000;">-</td>
            <td class="mediumtext text-center">Payment</td>
            <td rowspan="2" class="mediumtext" style="color:#00CC00;">+</td>
            <td class="mediumtext text-center"> Current Bill Amount</td>
            <td rowspan="2"  class="mediumtext text-center" style="color:#0000FF;">=</td>
            <td class="mediumtext text-center">Amount Payable By Due Date</td>
            <td class="mediumtext text-center" style="background:#ff0000;">Amount Payable After Due Date</td>
        </tr>
        <tr>
            <td class="mediumtext text-center">1,526.00 </td>

            <td class="mediumtext text-center"> 0.00</td>

            <td class="mediumtext text-center">  826.00</td>

            <td class="mediumtext text-center">2,352.00</td>
            <td class="mediumtext text-center" style="background:#ff0000;" >2,402.00</td>
        </tr>



        <tr>
            <td colspan="8"> <div class="largeheadertext" >CURRENT BILL DETAILS</div></td>
        </tr>

        <tr>

            <td colspan="5" class="mediumtext  text-center">Plane </td>

            <td colspan="1" class="mediumtext text-center">Amount(Rs.)</td>

            <td colspan="2" class="mediumtext text-center"> Paytm BarCode</td>

        </tr>

        <tr>

            <td colspan="5" class="mediumtext  ">Plane Name : <span class="extrasmalltext" style="font-weight:normal!important;">4 mbps Unlimited Plan</span><br />
                Discount<br />

                Installation<br />

                Taxbale Amount<br />
                <span class="extrasmalltext" style="font-weight:normal!important;">( SGST 9% )</span><br />
                <span class="extrasmalltext" style="font-weight:normal!important;">( CGST 9% )</span><br />
                <span class="extrasmalltext" style="font-weight:normal!important;">( IGST 9% )</span><br />

                Total Tax <br/>
                <hr>
                Current Bill Amount

            </td>

            <td colspan="1" class="mediumtext text-right">700<br />
                0.00<br />

                0.00<br />
                700.00<br />
                <span class="extrasmalltext" style="font-weight:normal!important;">63.00</span><br />
                <span class="extrasmalltext" style="font-weight:normal!important;">63.00</span><br />
                <span class="extrasmalltext" style="font-weight:normal!important;">63.00</span><br />
                <span class="extrasmalltext" style="font-weight:normal!important;">126.00</span><br />

                <hr>
                826.00</td>

            <td colspan="2" class="mediumtext text-center"><div style="width:80%; height:300px; border:2px solid
            #560000;color:#000; padding:20px; margin:auto;" >
                    <img src={{url('docs/bill_image/barcode.png')}} width="250" height="250"></div></td>

        </tr>


        <tr style="border-top:2px solid #000; ">
            <td colspan="6"  class="extrasmalltext" ><span class="mediumtext "> Guidelines:</span><br />
                1. For any quries and complaints call between 09:00 to 20:00 at tel. 9873135358.<br />
                2. Installation charges are non refundable.<br />
                3. Under any circumstances product(s) once Sold will not be accepted back for any replacement or refunds
                .<br />
                4. Price subjected to change without notice.<br /><br /><br />
                The Star Internet, B-117, Vasant Kunj Enclave , Near Vasant Hospital, Vasant Kunj New Delhi - 110070.
                Mob. - 9873135358 , 9910135358





            </td>
            <td colspan="2" class="extrasmalltext" style="background:#ff0;"><span class="mediumtext"> Company Bank
                    Details
                </span><br
                /><br />

                Bank Name:  HDFC Bank<br /><br />
                Account No : 50200022824516<br /><br />
                Branch : Vasant Kunj ,Delhi <br /><br />
                IFSC Code : HDFC0009117<br />





            </td>
        </tr>


        <tr>

            <td colspan="8" class="smalltext">*Crossed Cheque/DD should be drawn locally in favour of THE STAR INTERNET. Please mention your name, invoice no. on back of the cheque. </td>



        </tr>
        <tr>

            <td colspan="8" class="smalltext">*This is system generated Invoice.. </td>



        </tr>


        </tbody>
    </table>





</div>





</body>
</html>

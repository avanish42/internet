@extends('app')

@section('title','Client | Profile')

@section('content')




    <!-- Main Content -->
    <div class="page-wrapper">
        <div class="container-fluid pt-25">

            <!-- Row -->
            <div class="row">
                <div class="col-lg-3 col-xs-12">
                    <div class="panel panel-default card-view  pa-0">
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body  pa-0">
                                <div class="profile-box">
                                    <div class="profile-cover-pic">

                                        <div class="profile-image-overlay">
                                            <img src={{URL::asset('docs/bg_profile.jpg')}}>
                                        </div>
                                    </div>
                                    <div class="profile-info text-center">
                                        <div class="profile-img-wrap">
                                            <img class="inline-block mb-10" src="{{URL::asset('docs/dummy.png')}}"
                                                 alt="user"/>

                                        </div>
                                        <h5 class="block mt-10 mb-5 weight-500 capitalize-font
                                        txt-danger">{{$data->name}}</h5>
                                        <h6 class="block capitalize-font pb-20">{{$data->plan->name}}</h6>
                                    </div>
                                    <div class="social-info">
                                        <div class="row">
                                            <div class="col-xs-4 text-center">
                                                <span class="counts block head-font"><span
                                                            class="counter-anim">{{$pay}}</span></span>
                                                <span class="counts-text block">payement </span>
                                            </div>
                                            <div class="col-xs-4 text-center">
                                                <span class="counts block head-font"><span
                                                            class="counter-anim">{{$debit}}</span></span>
                                                <span class="counts-text block">Debit</span>
                                            </div>
                                            <div class="col-xs-4 text-center">
                                                <span class="counts block head-font"><span
                                                            class="counter-anim">{{$pay-$debit}}</span></span>
                                                <span class="counts-text block"> Dues</span>
                                            </div>
                                        </div>
                                        <button class="btn btn-default btn-block btn-outline btn-anim mt-30" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil"></i><span class="btn-text">edit profile</span></button>
                                        <div id="myModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                        <h5 class="modal-title" id="myModalLabel">Edit Profile</h5>
                                                    </div>
                                                    <div class="modal-body">
                                                        <!-- Row -->
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="">
                                                                    <div class="panel-wrapper collapse in">
                                                                        <div class="panel-body pa-0">
                                                                            <div class="col-sm-12 col-xs-12">
                                                                                <div class="form-wrap">
                                                                                    <form action="#">
                                                                                        <div class="form-body overflow-hide">
                                                                                            <div class="form-group">
                                                                                                <label class="control-label mb-10" for="exampleInputuname_1">Name</label>
                                                                                                <div class="input-group">
                                                                                                    <div class="input-group-addon"><i class="icon-user"></i></div>
                                                                                                    <input type="text" class="form-control" id="exampleInputuname_1" placeholder="willard bryant">
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label class="control-label mb-10" for="exampleInputEmail_1">Email address</label>
                                                                                                <div class="input-group">
                                                                                                    <div class="input-group-addon"><i class="icon-envelope-open"></i></div>
                                                                                                    <input type="email" class="form-control" id="exampleInputEmail_1" placeholder="xyz@gmail.com">
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label class="control-label mb-10" for="exampleInputContact_1">Contact number</label>
                                                                                                <label
                                                                                                        class="control-label mb-10" for="exampleInputContact_1">Contact number</label>down
                                                                                                <div class="input-group">
                                                                                                    <div class="input-group-addon"><i class="icon-phone"></i></div>
                                                                                                    <input type="email" class="form-control" id="exampleInputContact_1" placeholder="+102 9388333">
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label class="control-label mb-10" for="exampleInputpwd_1">Password</label>
                                                                                                <div class="input-group">
                                                                                                    <div class="input-group-addon"><i class="icon-lock"></i></div>
                                                                                                    <input type="password" class="form-control" id="exampleInputpwd_1" placeholder="Enter pwd" value="password">
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label class="control-label mb-10">Gender</label>
                                                                                                <div>
                                                                                                    <div class="radio">
                                                                                                        <input type="radio" name="radio1" id="radio_1" value="option1" checked="">
                                                                                                        <label for="radio_1">
                                                                                                            M
                                                                                                        </label>
                                                                                                    </div>
                                                                                                    <div class="radio">
                                                                                                        <input type="radio" name="radio1" id="radio_2" value="option2">
                                                                                                        <label for="radio_2">
                                                                                                            F
                                                                                                        </label>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label class="control-label mb-10">Country</label>
                                                                                                <select class="form-control" data-placeholder="Choose a Category" tabindex="1">
                                                                                                    <option value="Category 1">USA</option>
                                                                                                    <option value="Category 2">Austrailia</option>
                                                                                                    <option value="Category 3">India</option>
                                                                                                    <option value="Category 4">UK</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="form-actions mt-10">
                                                                                            <button type="submit" class="btn btn-success mr-10 mb-30">Update profile</button>
                                                                                        </div>
                                                                                    </form>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-success waves-effect" data-dismiss="modal">Save</button>
                                                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                                                    </div>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-xs-12">
                    <div class="panel panel-default card-view pa-0">
                        <div class="panel-wrapper collapse in">
                            <div  class="panel-body pb-0">
                                <div  class="tab-struct custom-tab-1">
                                    <ul role="tablist" class="nav nav-tabs nav-tabs-responsive" id="myTabs_8">
                                        <li  class="active" role="presentation" class="next"><a aria-expanded="true"
                                                                                  data-toggle="tab" role="tab" id="follo_tab_8" href="#follo_8"><span>Payment History <span class="inline-block"></span></span></a></li>
                                        <li role="presentation" class=""><a  data-toggle="tab" id="photos_tab_8"
                                                                             role="tab" href="#photos_8"
                                                                             aria-expanded="false"><span>Bills/Recepts</span></a></li>
                                        <li role="presentation" class=""><a  data-toggle="tab" id="earning_tab_8"
                                                                             role="tab" href="#earnings_8"
                                                                             aria-expanded="false"><span>Documets</span></a></li>
                                        <li role="presentation" class=""><a  data-toggle="tab" id="settings_tab_8"
                                                                             role="tab" href="#settings_8"
                                                                             aria-expanded="false"><span>Complain
                                                    history
                                                </span></a></li>
                                        
                                    </ul>
                                    <div class="tab-content" id="myTabContent_8">


                                        <div  id="follo_8" class="tab-pane fade active in" role="tabpanel">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <form id="example-advanced-form" action="#">
                                                        <div class="table-wrap">
                                                            <div class="table-responsive">
                                                                <table class="table jsgrid-table table-striped display
                                                                product-overview" id="datable_3">
                                                                    <thead>
                                                                    <tr>
                                                                        <th>S no</th>
                                                                        <th>Exicutive Name</th>
                                                                        <th>credit</th>
                                                                        <th>debit</th>
                                                                        <th>Remark</th>
                                                                        <th>date</th>
                                                                    </tr>
                                                                    </thead>

                                                                    <tbody>
                                                                    @foreach($payment as $k =>$v)
                                                                        <?php
                                                                        if ($v->credit)
                                                                        {
                                                                            $row_class ="green" ;

                                                                        }
                                                                        else
                                                                        {
                                                                            $row_class="red";
                                                                        }
                                                                        ?>
                                                                        <tr style="color: <?php echo $row_class ?>">
                                                                            <th>{{$k}}</th>
                                                                            <th >{{$v->exicutive->name}}</th>
                                                                            <th >&#x20B9;{{$v->credit?$v->credit:"N/A"}}</th>
                                                                            <th >&#x20B9;{{$v->debit?$v->debit:"N/A"}}</th>
                                                                            <th>{{isset($v->paybill)
                                                                            ?$v->paybill->remark:"N/A"}}</th>
                                                                            <th
                                                                            >{{$v->payment_date?
                                                                            (date('d-M-y',strtotime($v->payment_date)))
                                                                            :"N/A"}}</th>

                                                                        </tr>
                                                                    @endforeach

                                                                    </tbody>
                                                                    <tfoot>
                                                                    <tr>
                                                                        <th></th>
                                                                        <th>Total</th>
                                                                        <th>&#x20B9;{{$pay}}</th>
                                                                        <th>&#x20B9;{{$debit}}</th>
                                                                        <th></th>
                                                                        <th></th>
                                                                    </tr>
                                                                    </tfoot>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div  id="photos_8" class="tab-pane fade" role="tabpanel">
                                            <!-- Row -->
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <form id="example-advanced-form" action="#">
                                                        <div class="table-wrap">
                                                            <div class="table-responsive">
                                                                <table class="table table-striped display product-overview" id="datable_1">
                                                                    <thead>
                                                                    <tr>
                                                                        <th>Date</th>
                                                                        <th>invoce no</th>
                                                                        <th>Amount</th>
                                                                        <th>Bill</th>
                                                                        <th>show</th>
                                                                        <th>Recept</th>
                                                                        <th>Send mail</th>
                                                                    </tr>
                                                                    </thead>

                                                                    <tbody>
                                                                    @foreach($bills as $bill)
                                                                    <tr>
                                                                        <td>{{ date('d-M-y',strtotime($bill->bill_date))  }}</td>

                                                                        <td>{{$bill->bill_no}}</td>
                                                                        <td>&#x20B9;
                                                                        {{$bill->amount}}
                                                                        </td>
                                                                        <td><a href={{$bill->bill_path}} class="button" download><i class="fa fa-download"></i>Download </a>  </td>
                                                                        <td>abc</td>
                                                                        <td><a href={{$bill->bill_path}}
                                                                                    class="button" target="_blank"><i
                                                                                        class="fa
                                                                                    fa-download"></i>Show </a>  </td>
                                                                        <td> <a href={{asset('bill/resend-bill/'.$bill->id
                                                                        )
                                                                        }}>Send Mail</a>
                                                                        </td>

                                                                    </tr>
                                                                        @endforeach

                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div  id="earnings_8" class="tab-pane fade" role="tabpanel">
                                            <!-- Row -->
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <form id="example-advanced-form" action="#">
                                                        <div class="table-wrap">
                                                            <div class="table-responsive">
                                                                <table class="table table-striped display product-overview" id="datable_1">
                                                                    <thead>
                                                                    <tr>
                                                                        <th>Date</th>
                                                                        <th>Item Sales Colunt</th>
                                                                        <th>Earnings</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tfoot>
                                                                    <tr>
                                                                        <th colspan="2">total:</th>
                                                                        <th></th>
                                                                    </tr>
                                                                    </tfoot>
                                                                    <tbody>
                                                                    <tr>
                                                                        <td>monday, 12</td>
                                                                        <td>
                                                                            3
                                                                        </td>
                                                                        <td>$400</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>tuesday, 13</td>
                                                                        <td>
                                                                            2
                                                                        </td>
                                                                        <td>$400</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>wednesday, 14</td>
                                                                        <td>
                                                                            3
                                                                        </td>
                                                                        <td>$420</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>thursday, 15</td>
                                                                        <td>
                                                                            5
                                                                        </td>
                                                                        <td>$500</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>friday, 15</td>
                                                                        <td>
                                                                            3
                                                                        </td>
                                                                        <td>$400</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>saturday, 16</td>
                                                                        <td>
                                                                            3
                                                                        </td>
                                                                        <td>$400</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>sunday, 17</td>
                                                                        <td>
                                                                            3
                                                                        </td>
                                                                        <td>$400</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>monday, 18</td>
                                                                        <td>
                                                                            3
                                                                        </td>
                                                                        <td>$500</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>tuesday, 19</td>
                                                                        <td>
                                                                            3
                                                                        </td>
                                                                        <td>$400</td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div  id="settings_8" class="tab-pane fade" role="tabpanel">
                                            <!-- Row -->
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="">
                                                        <div class="panel-wrapper collapse in">
                                                            <div class="panel-body pa-0">
                                                                <div class="col-sm-12 col-xs-12">
                                                                    <div class="form-wrap">
                                                                        <form action="#">
                                                                            <div class="form-body overflow-hide">
                                                                                <div class="form-group">
                                                                                    <label class="control-label mb-10" for="exampleInputuname_01">Name</label>
                                                                                    <div class="input-group">
                                                                                        <div class="input-group-addon"><i class="icon-user"></i></div>
                                                                                        <input type="text" class="form-control" id="exampleInputuname_01" placeholder="willard bryant">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label class="control-label mb-10" for="exampleInputEmail_01">Email address</label>
                                                                                    <div class="input-group">
                                                                                        <div class="input-group-addon"><i class="icon-envelope-open"></i></div>
                                                                                        <input type="email" class="form-control" id="exampleInputEmail_01" placeholder="xyz@gmail.com">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label class="control-label mb-10" for="exampleInputContact_01">Contact number</label>
                                                                                    <div class="input-group">
                                                                                        <div class="input-group-addon"><i class="icon-phone"></i></div>
                                                                                        <input type="email" class="form-control" id="exampleInputContact_01" placeholder="+102 9388333">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label class="control-label mb-10" for="exampleInputpwd_01">Password</label>
                                                                                    <div class="input-group">
                                                                                        <div class="input-group-addon"><i class="icon-lock"></i></div>
                                                                                        <input type="password" class="form-control" id="exampleInputpwd_01" placeholder="Enter pwd" value="password">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label class="control-label mb-10">Gender</label>
                                                                                    <div>
                                                                                        <div class="radio">
                                                                                            <input type="radio" name="radio1" id="radio_01" value="option1" checked="">
                                                                                            <label for="radio_01">
                                                                                                M
                                                                                            </label>
                                                                                        </div>
                                                                                        <div class="radio">
                                                                                            <input type="radio" name="radio1" id="radio_02" value="option2">
                                                                                            <label for="radio_02">
                                                                                                F
                                                                                            </label>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label class="control-label mb-10">Country</label>
                                                                                    <select class="form-control" data-placeholder="Choose a Category" tabindex="1">
                                                                                        <option value="Category 1">USA</option>
                                                                                        <option value="Category 2">Austrailia</option>
                                                                                        <option value="Category 3">India</option>
                                                                                        <option value="Category 4">UK</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-actions mt-10">
                                                                                <button type="submit" class="btn btn-success mr-10 mb-30">Update profile</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
            <!-- /Row -->



        </div>
        <!-- Footer -->
       @include('layout.footer')
        <!-- /Footer -->

    </div>
    <!-- /Main Content -->



@endsection

@push('pagescript')

    <script src={{URL::asset("vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js")}} ></script>
    <script src={{URL::asset("vendors/bower_components/datatables.net-buttons/js/buttons.flash.min.js")}} ></script>
    <script src={{URL::asset("vendors/bower_components/jszip/dist/jszip.min.js")}} ></script>
    <script src={{URL::asset("vendors/bower_components/pdfmake/build/pdfmake.min.js")}} ></script>
    <script src={{URL::asset("vendors/bower_components/pdfmake/build/vfs_fonts.js")}} ></script>
    <script src={{URL::asset("vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js")}} ></script>
    <script src={{URL::asset("vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js")}} ></script>
    <script src={{URL::asset("dist/js/export-table-data.js")}} ></script>
    <script>


        $(document).ready(function() {

            // $('#example').DataTable();



            // $( ".datepicker" ).datepicker();
            // $( "#datepicker" ).datepicker();



            $('#datable_3').DataTable(
                {
                    dom: 'lBfrtip',
                    "lengthMenu": [[100, 200, 300, -1], [100, 200, 300, "All"]],
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]

                }
            );


        } );

    </script>

@endpush
@extends('app')

@section('title','Clients')

@section('content')
    <!-- Main Content -->
    <div class="page-wrapper clearfix">

        <div class="container-fluid">

            <!-- Title -->
            <div class="row heading-bg">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h5 class="txt-dark">All Clients</h5>
                </div>
                <!-- Breadcrumb -->
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="index.html">Dashboard</a></li>
                        <li class="active"><span>clients</span></li>
                        {{--<li><a href="#"><span>Zone</span></a></li>--}}
                    </ol>
                </div>
                <!-- /Breadcrumb -->
            </div>
            <!-- /Title -->





            <!-- Row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h6 class="panel-title txt-dark">All Clients</h6>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="table-wrap">
                                    <div class="table-responsive">
                                        <table id="client-table-yajra" class="table jsgrid-table table-hover display
                                        pb-30" >
                                            <thead>
                                            <tr>
                                                {{--<th>S No</th>--}}
                                                <th>Name</th>



                                                {{--<th>Customer Id</th>--}}
                                                {{--<th>Card Id</th>--}}
                                                <th>User Id</th>
                                                <th>Mobile</th>
                                                <th>Address</th>

                                                {{--<th>Created /Updated </th>--}}
                                                <th>Action</th>


                                            </tr>
                                            </thead>

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Row -->




            @include('layout.footer')



        </div>
        <!-- /Main Content -->
    </div>

@endsection

<script type="text/javascript">
    function click_js(url) {
        window.open(APP_URL+'/'+url);
        // console.log(APP_URL);
    }

    function  click_js_same(url) {

        window.location.href=(APP_URL+'/'+url);


    }

    var pagewrapper = document.getElementsByClassName("page-wrapper");
    // element.classList.remove("mystyle");
    // pagewrapper.style.height=null;

</script>

@push('pagescript')
<script src={{asset('/js/usertable.js')}}>



</script>
    @endpush
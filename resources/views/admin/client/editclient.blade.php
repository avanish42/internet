@extends('app')

@section('title','Client | Edit')

@section('content')
    <!-- Main Content -->
    <div class="page-wrapper">
        <div class="container-fluid">

            <!-- Title -->
            <div class="row heading-bg">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h5 class="txt-dark">Client Master</h5>
                </div>
                <!-- Breadcrumb -->
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="index.html">Dashboard</a></li>
                        <li><span>Client</span></li>
                        <li><a class="active" href="#"><span>Edit</span></a></li>
                    </ol>
                </div>
                <!-- /Breadcrumb -->
            </div>
            <!-- /Title -->

            <!-- Row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default card-view" style="background-color: lightgrey">
                        <div class="panel-heading">
                            <div class="pull-left" >
                                <h6 class="panel-title txt-dark">Edit Client  Details</h6>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="form-wrap">
                                    <form class="form-horizontal" method="post" action="{{URL::asset('update-client')
                                    }}">
                                        <div class="form-group mb-0">
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    @csrf
                                                    <input id="client_id" type="hidden" name="id" value={{$data->id}}>

                                                    <div class="col-sm-4">
                                                        <label class="control-label mb-10">Select Plan*</label>
                                                        <select name="plan_id" class="form-control rounded-input" required>
                                                            <option selected value={{$data->plan->id}}
                                                            >{{$data->plan->name}} - {{$data->plan->price}}</option>
                                                            <option disabled >-- Select Plan --</option>
                                                            @foreach($plans as $plan)
                                                                <option value="{{$plan->id}}">{{$plan->name}} - {{$plan->price}}</option>
                                                            @endforeach

                                                        </select>
                                                    </div>

                                                    <div class="col-sm-4">
                                                        <label class="control-label mb-10">Select Zone*
                                                        </label>
                                                        <select id="zone-select"  name="zone_id" class="form-control
                                                        rounded-input"
                                                                required>
                                                            @if(isset($data->zone->id))
                                                            <option selected value={{$data->zone->id}}
                                                                    value>{{$data->zone->name}}</option>
                                                            @endif
                                                            <option disabled >-- Select zone --</option>
                                                            @foreach($zones as $zone)
                                                                <option value="{{$zone->id}}">{{$zone->name}}</option>
                                                            @endforeach

                                                        </select>
                                                    </div>

                                                    <div class="col-sm-4">
                                                        <label class="control-label mb-10">Select Area*
                                                        </label>
                                                        <select id="area-select" name="area_id" class="form-control
                                                        rounded-input"
                                                                required>
                                                            @if(isset($data->area->id))
                                                            <option selected value={{$data->area->id}}
                                                                    value>{{$data->area->name}}</option>
                                                            @endif
                                                            <option disabled >-- Select Area --</option>

                                                        </select>
                                                    </div>



                                                    <div class="col-sm-4">
                                                        <label class="control-label mb-10">Name*</label>
                                                        <input type="text" name="name" class="form-control filled-input
                                                        rounded-input" placeholder="Name of client..." name="name"
                                                               value= '{{$data->name }}'  required>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <label class="control-label mb-10">Email</label>
                                                        <input type="text" class="form-control
                                                        filled-input
                                                        rounded-input" placeholder="Client email..." name="email"
                                                               value={{$data->email}} >
                                                    </div>


                                                    <div class="col-sm-4">
                                                        <label class="control-label mb-10"> Customer Type*
                                                        </label>
                                                        <select id="area-select" name="coustmer_type" class="form-control
                                                        rounded-input"
                                                                required>

                                                            <option selected value={{$data->coustmer_type}}  >{{$data->coustmer_type}}</option>
                                                            <option disabled >-- Select Type --</option>
                                                            <option value="prepaid">Pre-Paid</option>
                                                            <option value="postpaid">Post-Paid</option>


                                                        </select>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <label id="custmor_id_lable" class="control-label mb-10">Customer ID*</label>
                                                        <input type="text" id="customer-id-check" name="customer_id" class="form-control
                                                        filled-input
                                                        rounded-input" placeholder="Customer ID...
                                                        ."  value={{$data->customer_id}} required>
                                                    </div>


                                                    <div class="col-sm-4">
                                                        <label class="control-label mb-10">User ID*</label>
                                                        <input type="text" name="user_id" class="form-control
                                                        filled-input
                                                        rounded-input" placeholder="User Id..
                                                        ."  value={{$data->user_id}}  required>
                                                    </div>

                                                    <div class="col-sm-4">
                                                        <label id="card_id_lable" class="control-label mb-10">Card ID</label>
                                                        <input id="card-id-check" type="text" name="card_id" class="form-control
                                                        filled-input rounded-input" value= {{$data->card_id}}
                                                                 >
                                                    </div>


                                                    <div class="col-sm-2">
                                                        <label class="control-label mb-10">Joing Date*</label>
                                                        <div class='input-group date' id='datetimepicker' required>

                                                            <input type="date" name="joining_date" class="form-control
                                                        filled-input
                                                        rounded-input" value={{$data->joining_date}} placeholder="Select Joing
                                                                   date..
                                                        ." >
                                                            <span class="input-group-addon">
																	<span class="fa fa-calendar"></span>
																</span>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-2">
                                                        <label class="control-label mb-10">Billing Date*</label>
                                                        <div class='input-group date' id='datetimepicker' required>

                                                            <input type="date" name="billing_date" class="form-control
                                                        filled-input
                                                        rounded-input" value={{$data->billing_date}}
                                                                    placeholder= "Select Billing date..." >
                                                            <span class="input-group-addon">
																	<span class="fa fa-calendar"></span>
																</span>
                                                        </div>
                                                    </div>




                                                    <div class="col-sm-4">
                                                        <label class="control-label mb-10">GST Number</label>
                                                        <input type="text" name="gst_no" class="form-control
                                                        filled-input
                                                        rounded-input" placeholder="GST No..
                                                        ."  value={{$data->gst_no}}>
                                                    </div>



                                                    <div class="col-sm-4">
                                                        <label class="control-label mb-10">Mobile*</label>
                                                        <input type="text" name="mobile" class="form-control
                                                        filled-input
                                                        rounded-input" placeholder="Mobile No..
                                                        ."    value={{$data->mobile}} required>
                                                    </div>

                                                </div>
                                                <div class="row">

                                                    <div class="col-sm-4">
                                                        <label class="control-label mb-10">Discount</label>
                                                        <input type="text" name="discount" class="form-control
                                                        filled-input
                                                        rounded-input" placeholder="Any Discunt in rs..
                                                        ."  value={{$data->discount}} >
                                                    </div>


                                                    <div class="col-sm-4">
                                                        <label class="control-label mb-10">Installment Amount</label>
                                                        <input type="text" name="installment_amount" class="form-control
                                                        filled-input
                                                        rounded-input" placeholder="Installment Amount..
                                                        ."   value={{$data->installment_amount}}>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <label class="control-label mb-10"> Street Address</label>
                                                        <input type="text" name="street" class="form-control
                                                        filled-input
                                                        rounded-input" placeholder="Street Address..."   value='{{$data->street}}'>
                                                    </div>

                                                    <div class="col-sm-4">
                                                        <label class="control-label mb-10">Pincode</label>
                                                        <input type="text" name="postal_code" class="form-control
                                                        filled-input
                                                        rounded-input" placeholder="Pincode..." value="110070"
                                                               >
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <label class="control-label mb-10">City</label>
                                                        <input type="text" name="city" class="form-control
                                                        filled-input
                                                        rounded-input" value="Vasant Kunj" placeholder="City
                                                        .."  >
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <label class="control-label mb-10">State</label>
                                                        <input type="text" name="state" class="form-control
                                                        filled-input
                                                        rounded-input" placeholder="State..." value="New Delhi" >
                                                    </div>




                                                    <div class="col-sm-4">
                                                        <div class="col-sm-4">

                                                            <?php
                                                            $is_gst_class= $data->is_gst?"bs-switch":"bs-switch-false";
                                                            $is_pdf_class= $data->is_pdf?"bs-switch":"bs-switch-false";
                                                            $is_auto_class=$data->auto_bill?"bs-switch":"bs-switch-false";
                                                            $is_igst_class=$data->is_igst?"bs-switch":"bs-switch-false";
                                                            $is_gst_in_plan_class=$data->is_gst_in_plan?"bs-switch":"bs-switch-false";
                                                            ?>



                                                        </div>

                                                        <div class="col-sm-4">


                                                            <label class="control-label mb-10"
                                                                   style="padding-left:45px;">Bill</label>
                                                            <div>
                                                                <input id="check_box_switch" type="checkbox"
                                                                       data-off-text="Text" data-on-text="PDF"
                                                                       class={{$is_pdf_class}} name="is_pdf">

                                                            </div>

                                                        </div>

                                                        <div class="col-sm-4">


                                                            <label class="control-label mb-10" style="padding-left:30px;">
                                                                Auto Bill </label>
                                                            <div>
                                                                <input id="check_box_switch" type="checkbox"
                                                                       data-off-text="Cust." data-on-text="Auto"
                                                                       class={{$is_auto_class}} name="auto_bill">

                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="col-sm-4">

                                                        <div class="col-sm-4">


                                                            <label class="control-label mb-10" style="padding-left:30px;">
                                                                IGST</label>
                                                            <div>
                                                                <input id="check_box_switch" type="checkbox"
                                                                       data-off-text="False" data-on-text="iGST"
                                                                       class={{$is_igst_class}} name="is_igst">

                                                            </div>

                                                        </div>

                                                        <div class="col-sm-8">


                                                            <label class="control-label mb-10" style="padding-left:30px;">
                                                                GST Include</label>
                                                            <div>
                                                                <input id="check_box_switch" type="checkbox"
                                                                       data-off-text="Excl" data-on-text="Incl"
                                                                       class={{$is_gst_in_plan_class}} name="is_gst_in_plan">

                                                            </div>

                                                        </div>



                                                    </div>











                                                </div>
                                                <br>
                                                <br>
                                                <br>
                                                <div class="row">
                                                    <div class="col-sm-4">

                                                    </div>

                                                    <div class="col-sm-2">
                                                        <div class="">
                                                            <label class="control-label mb-10"></label>
                                                            <input id="submit-button" type="submit" value="Update" class="form-control
                                                        rounded-input btn btn-success" placeholder="Any
                                                        Remark/Description/Details.
                                                        ..">
                                                            {{--<button type="submit" class="btn btn-success btn-anim"><i--}}
                                                            {{--class="icon-arrow-right"></i><span class="btn-text">Add</span></button>--}}

                                                        </div>
                                                    </div>
                                                    {{--<div class="col-sm-2">--}}
                                                        {{--<div class="">--}}
                                                            {{--<label class="control-label mb-10"></label>--}}
                                                            {{--<input type="reset" value="Reset" class="form-control--}}
                                                        {{--rounded-input btn btn-danger" placeholder="Any--}}
                                                        {{--Remark/Description/Details.--}}
                                                        {{--..">--}}
                                                            {{--<button type="submit" class="btn btn-success btn-anim"><i--}}
                                                            {{--class="icon-arrow-right"></i><span class="btn-text">Add</span></button>--}}

                                                        {{--</div>--}}
                                                    {{--</div>--}}

                                                    <div class="col-sm-4">

                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Row -->

            @include('layout.footer')

        </div>
        <!-- /Main Content -->
    </div>

@endsection

@push('pagescript')
    <script type="text/javascript" >



        $(document).ready(function() {

            var client_id=$('#client_id').val();


            $("#customer-id-check").keyup(function(){

                var customer_id=$(this).val();




              console.log(client_id);


                $.ajax({
                    url: APP_URL + "/check-customer-id",
                    type: "POST",
                    data: {
                        id: customer_id,
                        client_id:client_id,
                        _token: token
                    },

                    dataType: "json",

                    success: function (data) {
                        console.log(data)
                        // $("#resultarea").text(data);



                        // console.log(data);
                        if (data.length!=0) {



                            //   console.log('found')


                            $('#custmor_id_lable').css('color', 'red');



                            $('#submit-button').prop('disabled', true);
                            $('#reset-button').prop('disabled', true);








                        }
                        else {
                            //  console.log('not found');
                            //$('#customer-id-check').css('border-color', 'green');
                            $('#custmor_id_lable').css('color', 'green');
                            $('#reset-button').prop('disabled', false);
                            $('#submit-button').prop('disabled', false);



                        }






                    }


                })

            });





            $("#card-id-check").keyup(function(){

                var card_id=$(this).val();


                $.ajax({
                    url: APP_URL + "/check-card-id",
                    type: "POST",
                    data: {
                        id: card_id,
                        clint_id:client_id,
                        _token: token
                    },

                    dataType: "json",

                    success: function (data) {
                        //console.log(data)
                        // $("#resultarea").text(data);



                        // console.log(data);
                        if (data.length!=0) {



                            //   console.log('found')


                            $('#card_id_lable').css('color', 'red');



                            $('#submit-button').prop('disabled', true);
                            $('#reset-button').prop('disabled', true);








                        }
                        else {
                            //  console.log('not found');
                            //$('#customer-id-check').css('border-color', 'green');
                            $('#card_id_lable').css('color', 'green');
                            $('#reset-button').prop('disabled', false);
                            $('#submit-button').prop('disabled', false);



                        }






                    }


                })

            });




        });

    </script>
@endpush
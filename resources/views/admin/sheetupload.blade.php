@extends('app')

@section('title','Dashboard')

@section('content')
    <!-- Main Content -->
    <div class="page-wrapper">
        <div class="container-fluid">

            <!-- Title -->
            <div class="row heading-bg">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h5 class="txt-dark">Zone Master</h5>
                </div>
                <!-- Breadcrumb -->
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="index.html">Dashboard</a></li>
                        <li class="active"><span>Zone</span></li>
                        {{--<li><a href="#"><span>Zone</span></a></li>--}}
                    </ol>
                </div>
                <!-- /Breadcrumb -->
            </div>
            <!-- /Title -->

            <!-- Row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">
                            <div class="pull-left">
                                {{--<h6 class="panel-title txt-dark">inline form</h6>--}}
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="form-wrap">
                                    <h5>New Data upload</h5>
                                    <form  method="post" enctype='multipart/form-data' action={{URL::asset('sheet-upload')}} class="form-inline">
                                        @csrf

                                        <div class="form-group mr-15">
                                            <label class="control-label mr-10" for="email_inline">Name*:</label>
                                            <input type="file" name="client_sheet" class="form-control" id="email_inline">
                                        </div>


                                        <button type="submit" class="btn btn-success btn-anim"><i
                                                    class="icon-arrow-right"></i><span
                                                    class="btn-text">Upload</span></button>


                                    </form>
                                </div>
                                <br>
                                <hr>



                                    <div class="form-wrap">
                                        <h5>Data Update By sheet</h5>
                                        <form  method="post" enctype='multipart/form-data' action={{URL::asset('update-data-by-sheet')}} class="form-inline">
                                            @csrf

                                            <div class="form-group mr-15">
                                                <label class="control-label mr-10" for="email_inline">Name*:</label>
                                                <input type="file" name="client_sheet_update" class="form-control"
                                                       id="email_inline">
                                            </div>


                                            <button type="submit" class="btn btn-success btn-anim"><i
                                                        class="icon-arrow-right"></i><span
                                                        class="btn-text">Upload</span></button>


                                        </form>
                                </div>

                                <div class="form-wrap">
                                    <h5>Area and Zone</h5>
                                    <form  method="post" enctype='multipart/form-data' action={{URL::asset
                                    ('update-area-zone')}} class="form-inline">
                                        @csrf

                                        <div class="form-group mr-15">
                                            <label class="control-label mr-10" for="email_inline">Name*:</label>
                                            <input type="file" name="client_sheet_area" class="form-control"
                                                   id="email_inline">
                                        </div>


                                        <button type="submit" class="btn btn-success btn-anim"><i
                                                    class="icon-arrow-right"></i><span
                                                    class="btn-text">Upload</span></button>


                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Row -->








            @include('layout.footer')



        </div>
        <!-- /Main Content -->
    </div>

@endsection
@extends('app')

@section('title','Change | Password')

@section('content')
    <!-- Main Content -->
    <div class="page-wrapper">
        <div class="container-fluid">

            <!-- Title -->
            <div class="row heading-bg">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h5 class="txt-dark">Exicutive Master</h5>
                </div>
                <!-- Breadcrumb -->
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="index.html">Dashboard</a></li>
                        <li class="active"><span>Exicutive</span></li>
                        {{--<li><a href="#"><span>Zone</span></a></li>--}}
                    </ol>
                </div>
                <!-- /Breadcrumb -->
            </div>
            <!-- /Title -->
            <!-- Row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default card-view" style="background-color: lightgrey">
                        <div class="panel-heading">
                            <div class="pull-left" >
                                <h6 class="panel-title txt-dark">Add New Exicutive</h6>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="form-wrap">
                                    <form class="form-horizontal" method="post" action="{{url
                                    ('exicutive/update-password')}}">
                                        <div class="form-group mb-0">
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    @csrf
                                                    <input type="hidden" name="id" value="{{$exicutive_data->id}}" >





                                                    <div class="col-sm-4">
                                                        <label class="control-label mb-10">Name*</label>
                                                        <input type="text" name="name" class="form-control filled-input
                                                        rounded-input" placeholder="Name of Exicutive..." name="name"
                                                               value="{{$exicutive_data->name}}" readonly required>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <label class="control-label mb-10">Email</label>
                                                        <input type="text" value="{{$exicutive_data->email}}"

                                                                class="form-control filled-input rounded-input"
                                                               placeholder="Exicutive email..." name="email"  readonly>
                                                    </div>











                                                    <div class="col-sm-4">
                                                        <label class="control-label mb-10">New Password</label>
                                                        <input type="text" name="password"
                                                                class="form-control
                                                        filled-input
                                                        rounded-input" placeholder=" New Password...
                                                        ." >
                                                    </div>
                                                    <br><br>


                                                    </div>


                                                </div>

                                                <br>
                                                <br>
                                                <br>
                                                <div class="row">
                                                    <div class="col-sm-4">

                                                    </div>

                                                    <div class="col-sm-2">
                                                        <div class="">
                                                            <label class="control-label mb-10"></label>
                                                            <input type="submit" value="Change" class="form-control
                                                        rounded-input btn btn-success" placeholder="Any
                                                        Remark/Description/Details.
                                                        ..">
                                                            {{--<button type="submit" class="btn btn-success btn-anim"><i--}}
                                                            {{--class="icon-arrow-right"></i><span class="btn-text">Add</span></button>--}}

                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <div class="">
                                                            <label class="control-label mb-10"></label>
                                                            <input type="reset" value="Reset" class="form-control
                                                        rounded-input btn btn-danger" placeholder="Any
                                                        Remark/Description/Details.
                                                        ..">
                                                            {{--<button type="submit" class="btn btn-success btn-anim"><i--}}
                                                            {{--class="icon-arrow-right"></i><span class="btn-text">Add</span></button>--}}

                                                        </div>
                                                    </div>

                                                    <div class="col-sm-4">

                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Row -->

            @include('layout.footer')

        </div>
        <!-- /Main Content -->
    </div>

@endsection
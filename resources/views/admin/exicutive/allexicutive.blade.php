@extends('app')

@section('title','Dashboard')

@section('content')
    <!-- Main Content -->
    <div class="page-wrapper">
        <div class="container-fluid">

            <!-- Title -->
            <div class="row heading-bg">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h5 class="txt-dark">Zone Master</h5>
                </div>
                <!-- Breadcrumb -->
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="index.html">Dashboard</a></li>
                        <li class="active"><span>Zone</span></li>
                        {{--<li><a href="#"><span>Zone</span></a></li>--}}
                    </ol>
                </div>
                <!-- /Breadcrumb -->
            </div>
            <!-- /Title -->




            <!-- Row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h6 class="panel-title txt-dark">All Exicutive</h6>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="table-wrap">
                                    <div class="table-responsive">
                                        <table id="datable_1" class="table jsgrid-table table-hover display  pb-30" >
                                            <thead>
                                            <tr>
                                                <th>S No</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Zone</th>
                                                <th>Role</th>
                                                <th>Action</th>
                                                <th>Password</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            @foreach($users as $v)
                                                <tr>
                                                    <td>{{$v->id}}</td>
                                                    <td>{{$v->name}}</td>
                                                    <td>{{$v->email}}</td>
                                                    <td>
                                                        <ul>
                                                            @if(count($v->zones))
                                                        @foreach($v->zones as  $k =>$val)
                                                            <li><span class="label label-success">
                                                                    {{$val->name}}</span></li>
                                                        @endforeach
                                                            @else
                                                            <li><span class="label label-danger"> N/A</span></li>
                                                        @endif
                                                        </ul>




                                                    </td>
                                                    <td> @if($v->role==1)
                                                            <i class="fa fa-user-circle-o" style="font-size:48px;color:red">
                                                        @elseif($v->role==0)
                                                            <i class="fa fa-user-circle-o" style="font-size:48px;
                                                            color:lawngreen">

                                                        @endif

                                                    </td>


                                                    <td >

                                                        {{--<button data-button='{"id": "{{$v->id}}"}' class="btn btn-primary--}}
                                                    {{--btn-circle edit-zone " id="edit-zone"--}}
                                                                {{--data-toggle="modal" data-target="#exampleModal"--}}
                                                                {{--data-whatever="@mdo"><i class="fa fa-pencil-square-o"></i></button>--}}



                                                        {{--<button class="btn btn-circle btn-primary"><i class="fa fa-pencil-square-o"></i> </button>--}}
                                                        <a  onclick="return confirm('Are you sure you want to delete ' +
                                                         'this item?');" href={{URL::asset('exicutive/delete-exicutive/'.$v->id)}}
                                                                alt="alert"

                                                            class= "  btn
                                                    btn-circle btn-danger  img-responsive model_img ". <?php if
                                                        ($v->role==1) echo "disabled" ?>><i
                                                    class="fa
                                                    fa-archive"></i></a>
                                                    </td>


                                                    <td>


                                                        {{--<button class="btn btn-circle btn-primary"><i class="fa fa-pencil-square-o"></i> </button>--}}
                                                        <a  onclick="return confirm('Are you sure you want to chage ' +
                                                         'password');" href={{URL::asset('exicutive/change-password/'
                                                         .$v->id)}}
                                                                alt="alert"

                                                            class= "  btn
                                                    btn-default  img-responsive model_img ">
                                                                change    </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Row -->

            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="display: none;">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color: slategrey">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h5 class="modal-title" id="exampleModalLabel1">Edit Zone Detials</h5>
                        </div>
                        <div class="modal-body">
                            <form method="post" action="{{URL::asset('update-zone')}}">
                                @csrf
                                {{--<input type="hidden" name="id" id="zone-id" >--}}


                                <div class="form-group">
                                    <label for="recipient-name" class="control-label mb-10">New Password:</label>
                                    <input type="text" name="name" class="form-control" id="zone-name">
                                </div>





                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>

                            </form>
                        </div>

                    </div>
                </div>
            </div>



            @include('layout.footer')



        </div>
        <!-- /Main Content -->
    </div>

@endsection
@extends('app')

@section('title','Expenses')

@section('content')
    <!-- Main Content -->
    <div class="page-wrapper">
        <div class="container-fluid">

            <!-- Title -->
            <div class="row heading-bg">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h5 class="txt-dark">Expense Management</h5>
                </div>
                <!-- Breadcrumb -->
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="index.html">Dashboard</a></li>
                        <li class="active"><span>expense</span></li>
                        {{--<li><a href="#"><span>Zone</span></a></li>--}}
                    </ol>
                </div>
                <!-- /Breadcrumb -->
            </div>
            <!-- /Title -->

            <!-- Row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">
                            <div class="pull-left">
                                {{--<h6 class="panel-title txt-dark">inline form</h6>--}}
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="form-wrap">
                                    <form  method="post" action={{URL::asset('expense/store-expense')}}
                                            class="form-inline">
                                        @csrf

                                        <div class="form-group mr-15">
                                            <label class="control-label mr-10" for="email_inline">Amount*:</label>
                                            <input type="number" name="amount" class="form-control" id="email_inline">
                                        </div>





                                        <div class="form-group mr-15">
                                            <label class="control-label mb-10">Remark</label>
                                            <select name="remark" class="form-control " required>
                                                <option disabled selected value>-- Select Remark --</option>
                                                <option>Petrol</option>
                                                <option value="Bike Mant." >Bike Mant.</option>
                                                <option value="Salary">Salary</option>
                                                <option value="Stationary">Stationary</option>
                                                <option  value="Cash">Cash</option>
                                                <option  value="Misc">Misc</option>


                                            </select>
                                        </div>

                                        <div class="form-group mr-15">
                                            <label class="control-label mr-10" for="pwd_inline">Description:</label>
                                            <input type="text" name="description" class="form-control"
                                                   id="pwd_inline" required>
                                        </div>

                                        <button type="submit" class="btn btn-success btn-anim"><i
                                                    class="icon-arrow-right"></i><span class="btn-text">Add</span></button>

                                        <button type="Reset" class="btn btn-danger btn-anim"><i
                                                    class="icon-arrow-right"></i><span
                                                    class="btn-text">Reset</span></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Row -->



            <!-- Row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h6 class="panel-title txt-dark">All Zones</h6>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="table-wrap">
                                    <div class="table-responsive">
                                        <table id="datable_1" class="table jsgrid-table table-hover display  pb-30" >
                                            <thead>
                                            <tr>
                                                <th>S No</th>
                                                <th>Exicutive name</th>
                                                <th>Amount</th>
                                                <th>Remark</th>
                                                <th>Description</th>
                                                <th>Date</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            @foreach($result as $k=>$v)
                                              <tr>
                                                  <td>{{$v->id}}</td>
                                                  <td>{{isset($v->exicutive->name)?$v->exicutive->name:"N/A" }}</td>
                                                  <td>{{$v->amount}}</td>
                                                  <td>{{$v->remark}}</td>
                                                  <td>{{$v->description}}</td>
                                                  <td>{{ date('d-M-Y',strtotime($v->created_at)) }}</td>

                                                  <td style="width: 90px;">

                                                      <button  class="btn btn-success btn-icon-anim btn-square
                                                    btn-xs"

                                                               data-whatever="@mdo"><a href={{url
                                                               ('expense/delete-expenses')
                                                                .'/'.$v->id}}><i class="fa fa-times"></i></a> </button>




                                                  </td>


                                              </tr>

                                               @endforeach

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Row -->




            @include('layout.footer')



        </div>
        <!-- /Main Content -->
    </div>

@endsection
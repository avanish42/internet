@extends('app')

@section('title','Dashboard')

@section('content')
    <!-- Main Content -->
    <div class="page-wrapper">
        <div class="container-fluid">

            <!-- Title -->
            <div class="row heading-bg">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h5 class="txt-dark">Plan Master</h5>
                </div>
                <!-- Breadcrumb -->
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="index.html">Dashboard</a></li>
                        <li class="active"><span>Plan</span></li>
                        {{--<li><a href="#"><span>Zone</span></a></li>--}}
                    </ol>
                </div>
                <!-- /Breadcrumb -->
            </div>
            <!-- /Title -->

            <!-- Row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default card-view" >
                        <div class="panel-heading">
                            <div class="pull-left" >
                                <h6 class="panel-title txt-dark">Add New Plan</h6>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="form-wrap">
                                    <form class="form-horizontal" method="post" action="{{URL::asset('save-plan')}}">
                                        <div class="form-group mb-0">
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    @csrf



                                                    <div class="col-sm-4">
                                                        <label class="control-label mb-10">Name*</label>
                                                        <input type="text" name="name" class="form-control filled-input
                                                        rounded-input" placeholder="Name of Plan..." required>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <label class="control-label mb-10">Validity*</label>
                                                        <input type="text"  name="validity" class="form-control
                                                        filled-input
                                                        rounded-input" value="30" placeholder="Validity in days..."
                                                               required>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <label class="control-label mb-10">Price*</label>
                                                        <input type="text" class="form-control filled-input
                                                        rounded-input" name="price" placeholder="Price of plan... " required>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <label class="control-label mb-10">Remark</label>
                                                        <input type="text" name="remark" class="form-control
                                                        filled-input
                                                        rounded-input" placeholder="Remark ..." >
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <label class="control-label mb-10">Description</label>
                                                        <input type="text" name="description" class="form-control
                                                        filled-input
                                                        rounded-input" placeholder="Any Description/Details...">
                                                    </div>

                                                    <div class="col-sm-2">
                                                        <div class="">
                                                            <label class="control-label mb-10">.</label>
                                                            <input type="submit" value="Add" class="form-control
                                                        rounded-input btn btn-success" placeholder="Any
                                                        Remark/Description/Details.
                                                        ..">

                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <div class="">
                                                            <label class="control-label mb-10">.</label>
                                                            <input type="reset" value="Reset" class="form-control
                                                        rounded-input btn btn-danger" placeholder="Any
                                                        Remark/Description/Details.
                                                        ..">

                                                        </div>
                                                    </div>

                                                    <div class="col-sm-4">

                                                    </div>



                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Row -->



            <!-- Row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h6 class="panel-title txt-dark">All Plans</h6>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="table-wrap">
                                    <div class="table-responsive">
                                        <table id="datable_1" class="table jsgrid-table table-hover display  pb-30" >
                                            <thead>
                                            <tr>
                                                <th>S No</th>
                                                <th>Name</th>
                                                <th>price</th>
                                                <th>Validity</th>
                                                <th>Remark</th>
                                                <th>Description</th>
                                                <th>Date</th>
                                                <th>Created /Updated </th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            @foreach($data as $v)
                                                <tr>
                                                    <td>{{$v->id}}</td>
                                                    <td>{{$v->name}}</td>
                                                    <td>{{$v->price}} Rs</td>
                                                    <td>{{$v->validity}} Days</td>
                                                    <td>{{$v->remark}} </td>
                                                    <td>{{$v->description}}</td>
                                                    <td>{{ date('d-M-Y',strtotime($v->created_at)) }}</td>
                                                    <td>{{ \Carbon\Carbon::parse($v->created_at)->diffForHumans()
                                                    }}/ {{ \Carbon\Carbon::parse($v->updated_at)->diffForHumans()
                                                    }}</td>
                                                    <td >

                                                        <button data-button='{"plan_id": "{{$v->id}}"}' class="btn
                                                        btn-primary
                                                    btn-circle edit-plan " id="edit-plan"
                                                                data-toggle="modal" data-target="#exampleModal"
                                                                data-whatever="@mdo"><i class="fa fa-pencil-square-o"></i></button>



                                                        {{--<button class="btn btn-circle btn-primary"><i class="fa fa-pencil-square-o"></i> </button>--}}
                                                        <a  onclick="return confirm('Are you sure you want to delete ' +
                                                         'this item?');" href={{URL::asset('delete-plan/'.$v->id)}}
                                                                alt="alert"
                                                            class="btn
                                                    btn-circle btn-danger img-responsive model_img"><i class="fa
                                                    fa-archive"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Row -->

            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="display: none;">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color: slategrey">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h5 class="modal-title" id="exampleModalLabel1">Edit Zone Detials</h5>
                        </div>
                        <div class="modal-body">
                            <form method="post" action="{{URL::asset('update-plan')}}">
                                @csrf
                                {{--<input type="hidden" name="id" id="zone-id" >--}}
                                <div class="form-group">
                                    <label for="recipient-name" class="control-label mb-10">ID:</label>
                                    <input type="text" name="id" class="form-control" id="plan-id" readonly>
                                </div>

                                <div class="form-group">
                                    <label for="recipient-name" class="control-label mb-10">Name:</label>
                                    <input type="text" name="name" class="form-control" id="plan-name">
                                </div>


                                <div class="form-group">
                                    <label for="recipient-name" class="control-label mb-10">Price:</label>
                                    <input type="text" name="price" class="form-control" id="plan-price">
                                </div>



                                <div class="form-group">
                                    <label for="recipient-name" class="control-label mb-10">Validity</label>
                                    <input type="text" name="validity" class="form-control" id="plan-validity">
                                </div>

                                <div class="form-group">
                                    <label for="recipient-name" class="control-label mb-10">Remark</label>
                                    <input type="text" name="remark" class="form-control" id="plan-remark">
                                </div>

                                <div class="form-group">
                                    <label for="recipient-name" class="control-label mb-10">Description</label>
                                    <input type="text" name="description" class="form-control" id="plan-description">
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>

                            </form>
                        </div>

                    </div>
                </div>
            </div>



            @include('layout.footer')



        </div>
        <!-- /Main Content -->
    </div>

@endsection
        @extends('app')

@section('title','Current | Generate')

@section('content')
    <!-- Main Content -->
    <div class="page-wrapper">
        <div class="container-fluid">

            <!-- Title -->
            <div class="row heading-bg">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h5 class="txt-dark"> <b style="background-color: #d2f281">{{$month}} </b>Current Generated Bills
                    </h5>
                </div>
                <!-- Breadcrumb -->
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="index.html">Dashboard</a></li>
                        <li class="active"><span>Zone</span></li>
                        {{--<li><a href="#"><span>Zone</span></a></li>--}}
                    </ol>
                </div>
                <!-- /Breadcrumb -->
            </div>
            <!-- /Title -->





            <!-- Row -->

            <form  method="post" action={{URL::asset('bill/cancle-generate')}} class="form-inline">
                @csrf
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <label  class="panel-title txt-dark">Select month :</label>

                                    <select id="select-month">
                                        <option value=''>--Select Month--</option>
                                        <option value='1'>Janaury</option>
                                        <option value='2'>February</option>
                                        <option value='3'>March</option>
                                        <option value='4'>April</option>
                                        <option value='5'>May</option>
                                        <option value='6'>June</option>
                                        <option value='7'>July</option>
                                        <option value='8'>August</option>
                                        <option value='9'>September</option>
                                        <option value='10'>October</option>
                                        <option value='11'>November</option>
                                        <option value='12'>December</option>
                                    </select>


                                </div>


                                <div class="pull-right">
                                    <div style="display: inline" class="checkbox">
                                        <input  id="checkbox-paprent" type="checkbox" onchange="checkAll(this)">
                                        <label for="checkbox-parent">ALL
                                        </label>
                                    </div>

                                    {{--<div style="display: inline" class="checkbox">--}}
                                        {{--<input  id="checkbox2" name="sms_checkbox" value="1" type="checkbox" >--}}
                                        {{--<label for="checkbox2">SMS--}}
                                        {{--</label>--}}
                                    {{--</div>--}}

                                    <button type="submit" class="btn btn-success">Cancle</button>
                                    {{--<button  type="submit" class="btn btn-primary">generate-send</button>--}}

                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="table-wrap">
                                        <div class="table-responsive">



                                            <table class="table jsgrid-table table-hover display  pb-30" id="example-bill"
                                                   class="display" style="width:100%">
                                                <thead>
                                                <tr>

                                                    <th>id</th>
                                                    <th>Select</th>
                                                    <th>Name</th>
                                                    <th>Mobile</th>
                                                    <th>Email</th>
                                                    <th>Billing Date</th>
                                                    <th>Last-G-Bill</th>
                                                    <th>Plan</th>
                                                    <th>Start date</th>
                                                    {{--<th>payment date</th>--}}
                                                    <th>Amount</th>
                                                    <th>Discount</th>
                                                    <th>Status</th>


                                                </tr>
                                                </thead>
                                                <tbody>

                                                @foreach($clients as $k =>$v)

                                                    <tr>
                                                        <td>{{$v->id}}</td>
                                                        <th><div class="checkbox">
                                                                <input class="all-checkbox" id={{"checkbox1".$v->id}}
                                                                        type="checkbox" name='bill_ids[]'
                                                                       value={{$v->generatedBill[0]->id}}>
                                                                <label for={{"checkbox1".$v->generatedBill[0]->id}}>
                                                                </label>
                                                            </div></th>
                                                        <th>{{$v->name}}</th>
                                                        <th>{{$v->mobile}}</th>

                                                        <td> <?php echo isset($v->email)?"<span class='label
                                                    label-success'> .</span> ":"<span class='label
                                                    label-danger'> .</span> "?></td>
                                                        <th><?php echo date('d-M-y',strtotime($v->billing_date)); ?></th>


                                                        <th>
                                                        <?php $count =count($v->generatedBill);
                                                        $amount=0;
                                                        if($count)
                                                        {
                                                            echo date('d-M-y',strtotime($v->generatedBill[$count-1]->created_at));
//                                                                       echo $v->generatedBill[$count-1]->created_at;
                                                                    $amount=$v->generatedBill[$count-1]->amount;
                                                        }
                                                        else{

                                                            echo'  <span class="label
                                                                      label-warning">N/A</span>';
                                                        }
                                                        ?>

                                                        <!--                                                        --><?php //echo date('d-M-y',strtotime($v->billing_date)); ?>

                                                        </th>

                                                        <th>{{$v->plan->name}}</th>
                                                        <td style="width: 137px;">



                                                            <?php
                                                            $gst_class=($v->is_gst)?"label-success":"label-danger";
                                                            $pdf_class=($v->is_pdf)?"label-success":"label-danger"  ;
                                                            $auto_class=($v->auto_bill) ?"label-success":"label-danger" ;
                                                            //                                                        $igst_class=($v->is_igst) ?"label-success":"label-danger" ;
                                                            $gstinclude_class=($v->is_gst_in_plan) ? "label-success":"label-danger" ;
                                                            ?>

                                                            <span class="label <?php echo $pdf_class;  ?>">PDF</span>
                                                            / <span class="label <?php echo $gstinclude_class;?>">GST Include</span>


                                                        </td>

                                                        <th> <strong style="color: black; font-size: large ">&#8377;</strong>
                                                            {{$amount}}</th>
                                                        <th>{{$v->discount?$v->discount:"N/A"}}</th>
                                                        <td><span class="label label-success">abc</span></td>
                                                    </tr>
                                                @endforeach
                                                </tbody>

                                            </table>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <!-- /Row -->




            @include('layout.footer')



        </div>
        <!-- /Main Content -->
    </div>

@endsection

@push('pagescript')
    <script type="text/javascript" >




        // $(document).ready(function() {
        //     $('#example').DataTable();


        $(document).ready(function() {
            $('#example-bill').DataTable( {
                dom: 'lBfrtip',
                "lengthMenu": [[20, 50, 100, -1], [20, 50, 100, "All"]],
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]

            } );


            $('#select-month').change(function () {
                console.log(this.value);
                var url={!! json_encode(url('/')) !!}

                    // console.log(url);

                    document.location = url+'/bill/current-generate-bills?month=' + this.value;
            })

        } );




        function checkAll(ele) {
            var checkboxes = document.getElementsByClassName('all-checkbox');
            if (ele.checked) {
                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].type == 'checkbox') {
                        checkboxes[i].checked = true;
                    }
                }
            } else {
                for (var i = 0; i < checkboxes.length; i++) {
                    console.log(i)
                    if (checkboxes[i].type == 'checkbox') {
                        checkboxes[i].checked = false;
                    }
                }
            }
        }


    </script>
@endpush
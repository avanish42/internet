@extends('app')

@section('title','Bill | Generate')

@section('content')
    <!-- Main Content -->
    <div class="page-wrapper">
        <div class="container-fluid">

            <!-- Title -->
            <div class="row heading-bg">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h5 class="txt-dark"> <?php echo date('F', mktime(0, 0, 0, $month, 10)); ?> Non Generate
                        Bills</h5>
                </div>
                <!-- Breadcrumb -->
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="index.html">Dashboard</a></li>
                        <li class="active"><span>Generate</span></li>
                        {{--<li><a href="#"><span>Zone</span></a></li>--}}
                    </ol>
                </div>
                <!-- /Breadcrumb -->
            </div>
            <!-- /Title -->




            <!-- Row -->

            <form  method="post" action={{URL::asset('bill/manual-generate')}} class="form-inline">
                @csrf
                <input type="hidden" name="month" value={{$month}}>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default card-view">
                            <div class="panel-heading">
                                <div class="pull-left">

                                    <label  class="panel-title txt-dark">Select month :</label>

                                    <select id="select-month">
                                        <option value=''>--Select Month--</option>
                                        <option  value='1'>Janaury</option>
                                        <option value='2'>February</option>
                                        <option value='3'>March</option>
                                        <option value='4'>April</option>
                                        <option value='5'>May</option>
                                        <option value='6'>June</option>
                                        <option value='7'>July</option>
                                        <option value='8'>August</option>
                                        <option value='9'>September</option>
                                        <option value='10'>October</option>
                                        <option value='11'>November</option>
                                        <option value='12'>December</option>
                                    </select>

                                </div>



                                <div class="pull-right">
                                    <div style="display: inline" class="checkbox">
                                        <input  id="checkbox-paprent" type="checkbox" onchange="checkAll(this)">
                                        <label for="checkbox-parent">ALL
                                        </label>
                                    </div>

                                    <div style="display: inline" class="checkbox">
                                        <input  id="checkbox2" name="sms_checkbox" value="1" type="checkbox" >
                                        <label for="checkbox2">SMS
                                        </label>
                                    </div>

                                    <button type="submit" class="btn btn-success">generate</button>
                                    <button  type="submit" class="btn btn-primary">generate-send</button>

                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="table-wrap">
                                        <div class="table-responsive">



                                            <table class="table jsgrid-table table-hover display  pb-30" id="example-bill"
                                                   class="display" style="width:100%">
                                                <thead>
                                                <tr>

                                                    {{--<th>id</th>--}}
                                                    <th>Select</th>
                                                    <th>Name</th>
                                                    <th>Mobile</th>
                                                    <th>Email</th>
                                                    <th>Address</th>

                                                    <th>Billing Date</th>
                                                    <th>Last-G-Bill</th>
                                                    <th>Plan</th>
                                                    <th>Status</th>
                                                    {{--<th>payment date</th>--}}
                                                    <th>Amount</th>
                                                    {{--<th>Discount</th>--}}
                                                    {{--<th>Status</th>--}}


                                                </tr>
                                                </thead>
                                                <tbody>

                                                @foreach($clients as $k =>$v)

                                                    <tr>
                                                        <th><div class="checkbox">
                                                                <input class="all-checkbox" id={{"checkbox1".$v->id}}
                                                                        type="checkbox" name='client_ids[]' value={{$v->id}}>
                                                                <label for={{"checkbox1".$v->id}}>
                                                                </label>
                                                            </div></th>
                                                        <th>{{$v->name}}</th>
                                                        <th>{{substr($v->mobile,0,11) }}</th>

                                                        <td> <?php echo isset($v->email)?"<span class='label
                                                    label-success'> .</span> ":"<span class='label
                                                    label-danger'> .</span> "?></td>

                                                        <td width="200px">{{ substr($v->street,0,20)}}</td>
                                                        <th><?php echo date('d-M-y',strtotime($v->billing_date)); ?></th>


                                                        <th>
                                                        <?php $count =count($v->generatedBill);
                                                        if($count)
                                                        {
                                                            echo date('d-M-y',strtotime($v->generatedBill[$count-1]->created_at));
//                                                                       echo $v->generatedBill[$count-1]->created_at;
                                                        }
                                                        else{

                                                            echo'  <span class="label
                                                                      label-warning">N/A</span>';
                                                        }
                                                        ?>

                                                        <!--                                                        --><?php //echo date('d-M-y',strtotime($v->billing_date)); ?>

                                                        </th>

                                                        <th width="50px">{{isset($v->plan)? trim(substr($v->plan->name,0,5))
                                                    :"N/A"}}</th>
                                                        <td style="width: 137px;">



                                                            <?php
                                                            $gst_class=($v->is_gst)?"label-success":"label-danger";
                                                            $pdf_class=($v->is_pdf)?"label-success":"label-danger"  ;
                                                            $auto_class=($v->auto_bill) ?"label-success":"label-danger" ;
                                                            $igst_class=($v->is_igst) ?"label-success":"label-danger" ;
                                                            $gstinclude_class=($v->is_gst_in_plan) ? "label-success":"label-danger" ;
                                                            ?>

                                                            <span class="label <?php echo $pdf_class;  ?>">PDF</span>

                                                            / <span class="label <?php echo $gstinclude_class;?>">GST Include</span>


                                                        </td>

                                                        <th>&#x20B9;{{isset($v->plan->price)?$v->plan->price:"N/A"}}</th>
                                                        {{--<th>{{$v->discount?$v->discount:"N/A"}}</th>--}}
                                                    </tr>
                                                @endforeach
                                                </tbody>

                                            </table>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <!-- /Row -->




            @include('layout.footer')



        </div>
        <!-- /Main Content -->
    </div>

@endsection

@push('pagescript')
    <script type="text/javascript" >





        $(document).ready(function() {
            $('#example-bill').DataTable( {
                dom: 'lBfrtip',
                "lengthMenu": [[20, 50, 100, -1], [20, 50, 100, "All"]],
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]

            } );

            $('#select-month').change(function () {
                console.log(this.value);
                var url={!! json_encode(url('/')) !!}

               // console.log(url);

               document.location = url+'/bill/non-generated-bill?month=' + this.value;
            })


        });




        function checkAll(ele) {
            var checkboxes = document.getElementsByClassName('all-checkbox');
            if (ele.checked) {
                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].type == 'checkbox') {
                        checkboxes[i].checked = true;
                    }
                }
            } else {
                for (var i = 0; i < checkboxes.length; i++) {
                    console.log(i)
                    if (checkboxes[i].type == 'checkbox') {
                        checkboxes[i].checked = false;
                    }
                }
            }
        }


    </script>
@endpush
@extends('app')

@section('title','Dashboard')

@section('content')
    <!-- Main Content -->
    <div class="page-wrapper">
        <div class="container-fluid">

            <!-- Title -->
            <div class="row heading-bg">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h5 class="txt-dark">Create Zone</h5>
                </div>
                <!-- Breadcrumb -->
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="index.html">Dashboard</a></li>
                        <li class="active"><span>Zone</span></li>
                        {{--<li><a href="#"><span>Zone</span></a></li>--}}
                    </ol>
                </div>
                <!-- /Breadcrumb -->
            </div>
            <!-- /Title -->

            <!-- Row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">
                            <div class="pull-left">
                                {{--<h6 class="panel-title txt-dark">inline form</h6>--}}
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="form-wrap">
                                    <form  method="post" action={{URL::asset('saveZone')}} class="form-inline">
                                      @csrf

                                        <div class="form-group mr-15">
                                            <label class="control-label mr-10" for="email_inline">Name*:</label>
                                            <input type="text" name="name" class="form-control" id="email_inline">
                                        </div>
                                        <div class="form-group mr-15">
                                            <label class="control-label mr-10" for="pwd_inline">Code*:</label>
                                            <input type="text" name="code" class="form-control" id="pwd_inline">
                                        </div>
                                        <div class="form-group mr-15">
                                            <label class="control-label mr-10" for="pwd_inline">Description:</label>
                                            <input type="text" name="description" class="form-control" id="pwd_inline">
                                        </div>

                                        <button type="submit" class="btn btn-success btn-anim"><i
                                                    class="icon-arrow-right"></i><span class="btn-text">Add</span></button>

                                        <button type="Reset" class="btn btn-danger btn-anim"><i
                                                    class="icon-arrow-right"></i><span
                                                    class="btn-text">Reset</span></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Row -->



            <!-- Row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h6 class="panel-title txt-dark">All Zones</h6>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="table-wrap">
                                    <div class="table-responsive">
                                        <table id="datable_1" class="table jsgrid-table table-hover display  pb-30" >
                                            <thead>
                                            <tr>
                                                <th>S No</th>
                                                <th>Name</th>
                                                <th>Code</th>
                                                <th>Description</th>
                                                <th>Date</th>
                                                <th>created At</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            @foreach($data as $v)
                                            <tr>
                                                <td>{{$v->id}}</td>
                                                <td>{{$v->name}}</td>
                                                <td>{{$v->code}}</td>
                                                <td>{{$v->description}}</td>
                                                <td>{{ date('d-M-Y',strtotime($v->created_at)) }}</td>
                                                <td>{{ \Carbon\Carbon::parse($v->created_at)->diffForHumans() }}</td>
                                                <td>

                                                    <button data-button='{"id": "{{$v->id}}"}' class="btn btn-primary
                                                    btn-circle edit-zone " id="edit-zone"
                                                            data-toggle="modal" data-target="#exampleModal"
                                                            data-whatever="@mdo"><i class="fa fa-pencil-square-o"></i></button>



                                                    {{--<button class="btn btn-circle btn-primary"><i class="fa fa-pencil-square-o"></i> </button>--}}
                                                    <a  onclick="return confirm('Are you sure you want to delete this item?');" href={{URL::asset('delete-zone/'.$v->id)}} alt="alert"
                                                        class="btn
                                                    btn-circle btn-danger img-responsive model_img"><i class="fa
                                                    fa-archive"></i></a>
                                                </td>
                                            </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Row -->

            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="display: none;">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color: slategrey">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h5 class="modal-title" id="exampleModalLabel1">Edit Zone Detials</h5>
                        </div>
                        <div class="modal-body">
                            <form method="post" action="{{URL::asset('update-zone')}}">
                                @csrf
                                {{--<input type="hidden" name="id" id="zone-id" >--}}
                                <div class="form-group">
                                    <label for="recipient-name" class="control-label mb-10">ID:</label>
                                    <input type="text" name="id" class="form-control" id="zone-id" readonly>
                                </div>

                                <div class="form-group">
                                    <label for="recipient-name" class="control-label mb-10">Name:</label>
                                    <input type="text" name="name" class="form-control" id="zone-name">
                                </div>



                                <div class="form-group">
                                    <label for="recipient-name" class="control-label mb-10">Code</label>
                                    <input type="text" name="code" class="form-control" id="zone-code">
                                </div>

                                <div class="form-group">
                                    <label for="recipient-name" class="control-label mb-10">Description</label>
                                    <input type="text" name="description" class="form-control" id="zone-description">
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>

                            </form>
                        </div>

                    </div>
                </div>
            </div>



           @include('layout.footer')



        </div>
        <!-- /Main Content -->
    </div>

@endsection
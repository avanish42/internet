@extends('app')

@section('title','Dashboard')


@section('content')

    <style>
        body {
            background: #34495e;
            color: #ecf0f1;
            margin: 0;
            padding: 0;
        }

        .banner {
            display: block;
            min-height: 200px;
            width: 100%;
            position: relative;
        }
        .typed_wrap {
            display: block;
            border: 4px solid #ecf0f1;
            width: 700px;
            height: auto;
            padding: 30px;

            /*centers it in the .banner*/
            position: absolute;
            top: 50%;
            left: 50%;
            -webkit-transform: translate(-50%,-50%);
            -moz-transform: translate(-50%,-50%);
            -ms-transform: translate(-50%,-50%);
            -o-transform: translate(-50%,-50%);
            transform: translate(-50%,-50%);
        }

        .typed_wrap h1 {
            display: inline;
        }

        /*Add custom cursor so it auto inherits font styles*/
        .typed::after {
            content: '|';
            display: inline;
            -webkit-animation: blink 0.7s infinite;
            -moz-animation: blink 0.7s infinite;
            animation: blink 0.7s infinite;
        }

        /*Removes cursor that comes with typed.js*/
        .typed-cursor{
            opacity: 0;
            display: none;
        }
        /*Custom cursor animation*/
        @keyframes blink{
            0% { opacity:1; }
            50% { opacity:0; }
            100% { opacity:1; }
        }
        @-webkit-keyframes blink{
            0% { opacity:1; }
            50% { opacity:0; }
            100% { opacity:1; }
        }
        @-moz-keyframes blink{
            0% { opacity:1; }
            50% { opacity:0; }
            100% { opacity:1; }
        }

        .btns {
            display: block;
            width: 400px;
            margin: 0;
            padding: 30px 0 0 30px;
        }
        .btns a {
            display: inline-block;
            margin-left: 5px;
        }
        .btns a:first-child{margin-left:0}
        .btn {
            font-family: sans-serif;
            font-size: 14px;
            font-weight: 600;
            color: #ecf0f1;
            text-decoration: none;
        }
        a .btn {
            cursor: pointer;
            border: 1.5px solid #ecf0f1;
            border-radius: 5px;
            display: inline-block;
            padding: 10px;
            list-style-type: none;
            transition: all .3s;
        }

        .btn:hover {
            background: #ecf0f1;
            color: #34495e;
        }
    </style>

    <!-- Main Cont
    ent -->
    {{--<script type="text/javascript" src="//100widgets.com/js_data.php?id=199"></script>--}}
    <div class="page-wrapper">
        <div class="container-fluid pt-25">

            <!-- Row -->
            <div class="row">
                <div class="banner">
                    <div class="typed_wrap">
                        <h1>The Star <span class="typed"></span></h1>
                    </div>
                </div>
            <!-- /Row -->
            {{--<iframe src="http://openspeedtest.com/Get-widget.php" width="735" scrolling="no" height="490" frameborder="0"></iframe>--}}

                {{--<h1 class="text-center">Welcome to Star Internet</h1>--}}
            </div>



        </div>

        <!-- Footer -->
        @include('layout.footer');
        <!-- /Footer -->

    </div>
    <!-- /Main Content -->


@endsection
@push('pagescript')

    <script src='https://cdnjs.cloudflare.com/ajax/libs/typed.js/2.0.9/typed.js' ></script>
    {{--F:\lalit\InternetProvider\public\bower_components\typed.js\src\typed.js--}}
    <script>

    var typed = new Typed('.typed', {
        strings: ["Boradband.", "Network.", "Internet."],
        // Optionally use an HTML element to grab strings from (must wrap each string in a <p>)
        stringsElement: null,
        // typing speed
        typeSpeed: 30,
        // time before typing starts
        startDelay: 1200,
        // backspacing speed
        backSpeed: 20,
        // time before backspacing
        backDelay: 500,
        // loop
        loop: true,
        // false = infinite
        loopCount: 5,
        // show cursor
        showCursor: false,
        // character for cursor
        cursorChar: "|",
        // attribute to type (null == text)
        attr: null,
        // either html or text
        contentType: 'html',
        // call when done callback function
        callback: function() {},
        // starting callback function before each string
        preStringTyped: function() {},
        //callback for every typed string
        onStringTyped: function() {},
        // callback for reset
        resetCallback: function() {}
    });

    // $(function(){
    //     $(".typed").typed({
    //         strings: ["Developers.", "Designers.", "People."],
    //         // Optionally use an HTML element to grab strings from (must wrap each string in a <p>)
    //         stringsElement: null,
    //         // typing speed
    //         typeSpeed: 30,
    //         // time before typing starts
    //         startDelay: 1200,
    //         // backspacing speed
    //         backSpeed: 20,
    //         // time before backspacing
    //         backDelay: 500,
    //         // loop
    //         loop: true,
    //         // false = infinite
    //         loopCount: 5,
    //         // show cursor
    //         showCursor: false,
    //         // character for cursor
    //         cursorChar: "|",
    //         // attribute to type (null == text)
    //         attr: null,
    //         // either html or text
    //         contentType: 'html',
    //         // call when done callback function
    //         callback: function() {},
    //         // starting callback function before each string
    //         preStringTyped: function() {},
    //         //callback for every typed string
    //         onStringTyped: function() {},
    //         // callback for reset
    //         resetCallback: function() {}
    //     });
    // });


    </script>




@endpush
@extends('app')

<style>
    #select-exicutive
    {
        width:250px;
        height: 30px;
    }
    #select-exicutive option
    {
        width:150px;

    }

</style>
@section('title','Dashboard')

@section('content')
    <!-- Main Content -->
    <div class="page-wrapper">
        <div class="container-fluid">

            <!-- Title -->
            <div class="row heading-bg">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h5 class="txt-dark">Pending Payments</h5>
                </div>
                <!-- Breadcrumb -->
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="index.html">Dashboard</a></li>
                        <li class="active"><span>Zone</span></li>
                        {{--<li><a href="#"><span>Zone</span></a></li>--}}
                    </ol>
                </div>
                <!-- /Breadcrumb -->
            </div>
            <!-- /Title -->




            <!-- Row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h6 class="panel-title txt-dark">All Pending Payments</h6>
                                <h6 class="panel-title txt-dark">Total :&#8377;<span class="label label-danger "
                                                                               style="font-size: 20px">
                                        {{$total}}</span> <strong style="color: black; font-size: large ">
                                     </strong>
                                </h6>

                            </div>
                            <div class="pull-right">

                                <div style="display: inline" class="checkbox">
                                    <input  id="checkbox_sms"   type="checkbox"  >
                                    <label for="checkbox2">SMS
                                    </label>
                                </div>


                                <label  class="panel-title txt-dark">Select Exicutive :</label>

                            <select id="select-exicutive">
                                <option class="panel-title txt-dark">--Select Exicutive--</option>
                                @foreach($allexictive as $k =>$v)
                                <option class="panel-title txt-dark" value={{$v->id}}>{{$v->name}}</option>
                                @endforeach
                            </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="table-wrap">
                                    <div class="table-responsive">
                                        <table id="datable_1" class="table jsgrid-table table-hover display  pb-30" >
                                            <thead>
                                            <tr>
                                                <th>S No</th>
                                                <th>Ex.Name</th>
                                                <th>Client</th>
                                                <th>Address</th>
                                                <th>zone</th>
                                                <th>amount</th>


                                                {{--<th>Date</th>--}}
                                                <th>Action</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            @foreach($pending_payment as $k=> $v)

                                                    <tr class="row_class">
                                                        <form method="post" action={{asset('payment/approve-payment')}}>
                                                            @csrf
                                                    <input type="hidden" name="id" value={{$v->id}}>
                                                            <input class="input_sms" name="sms_checkbox" type="hidden"
                                                                   value=0 >
                                                    <td>{{++$k}}123</td>
                                                    <td>{{$v->exicutive->name}}</td>
                                                    <td>{{$v->client->name}}</td>
                                                    <td>{{$v->client->street}}</td>
                                                    <td>
                                                        <ul>
                                                            @if(count($v->exicutive->zones))
                                                                @foreach($v->exicutive->zones as  $key =>$value)
                                                                    <li><span class="label label-success">
                                                                    {{$value->name}}</span></li>
                                                                @endforeach
                                                            @else
                                                                <li><span class="label label-danger"> N/A</span></li>
                                                            @endif
                                                        </ul>
                                                    </td>
                                                    <td>&#x20B9;{{$v->pay_amount}}</td>
                                                    <td >

                                                        {{--<a onclick="return confirm('Are you sure you want to save ' +--}}
                                                         {{--'this item?');"  href={{URL::asset('payment/approve-payment/'--}}
                                                         {{--.$v->id)}} alt="alert"--}}
                                                            {{--><span style="font-size:40px; color: lime "><i  class="fa--}}
                                                             {{--fa-check"></i></span> </a>--}}
                                                        <button type="submit"><span style="font-size:40px; color: lime "><i  class="fa
                                                             fa-check"></i></span></button>



                                                        {{--<button class="btn btn-circle btn-primary"><i class="fa fa-pencil-square-o"></i> </button>--}}
                                                        <a onclick="return confirm('Are you sure you want to delete ' +
                                                         'this item?');" href={{URL::asset('payment/cancle-payment/'
                                                         .$v->id)
                                                         }} alt="alert"
                                                        ><span style="font-size:40px; color: red; margin-left: 10px ">
                                                              <i class="fa fa-times"> </i>
                                                            </span> </a>
                                                    </td>


                                                        </form>

                                                    </tr>

                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Row -->




            @include('layout.footer')



        </div>
        <!-- /Main Content -->
    </div>

@endsection

@push('pagescript')

    <script>
        $(document).ready(function() {
            console.log('test');
            $('#select-exicutive').change(function () {
                console.log(this.value);
               var url={!! json_encode(url('/')) !!}

                console.log(url);

                document.location = url+'/payment/pending-payment?eid=' + this.value;
            })

            $("#checkbox_sms").change(function() {
                if(this.checked) {
                    //Do stuff
                    console.log('check')
                  //  var checkbox='<input class="checkbox_sms" name="sms_checkbox" type="hidden" value="1"  >';

                     $('.input_sms').val(1);
                }
                else
                {
                    $('.input_sms').val(0);

                }
            });
        });




    </script>


    @endpush
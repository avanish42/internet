@extends('app')

@section('title','Payment | Reminder')

@section('content')
    <!-- Main Content -->
    <div class="page-wrapper">
        <div class="container-fluid">

            <!-- Title -->
            <div class="row heading-bg">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h5 class="txt-dark">Payment Reminder</h5>
                </div>
                <!-- Breadcrumb -->
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="index.html">Dashboard</a></li>
                        <li class="active"><span>Payment-reminder</span></li>
                        {{--<li><a href="#"><span>Zone</span></a></li>--}}
                    </ol>
                </div>
                <!-- /Breadcrumb -->
            </div>
            <!-- /Title -->




            <form  method="post" action={{URL::asset('payment/send-reminder')}} class="form-inline">
            @csrf
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-dark">Message Details</h6>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="form-wrap">




                                        <div class="form-group">
                                            <label  class="control-label mb-25 text-left">
                                                </label>
                                            <textarea name="message" class="form-control " style="width: 500px;
                                            @import "
                                                      rows="3"></textarea>
                                        </div>





                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- Row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h6 class="panel-title txt-dark">All panding payments</h6>
                                @if(isset($total))  <h6 class="panel-title txt-dark">Total :: <span style="font-family: bold; font-size:
                                30px;"
                                    > {{$total}}</span></h6>@endif

                            </div>
                            <div class="pull-right">
                                <div style="display: inline" class="checkbox">
                                    <input  id="checkbox-paprent" type="checkbox" onchange="checkAll(this)">
                                    <label for="checkbox-parent">ALL
                                    </label>
                                </div>
                                <button type="submit" class="btn btn-success">Send</button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="table-wrap">
                                    <div class="table-responsive">


                                        <table class="table jsgrid-table table-hover display  pb-30" id="example" class="display" style="width:100%">
                                            <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Name</th>
                                                <th>card id</th>
                                                <th>Cust id</th>
                                                <th>Mobile</th>
                                                <th>Add</th>

                                                <th>Plan</th>
                                                <th>Last payment date</th>

                                                <th>Pay Amount</th>






                                            </tr>
                                            </thead>
                                            <tbody>

                                            @foreach($clients as $k =>$v)

                                                <tr>

                                                    <th><div class="checkbox">
                                                            <input class="all-checkbox" id={{"checkbox1".$v->id}}
                                                                    type="checkbox" name='client_ids[]'
                                                                   value={{$v->id}}>
                                                            <label for={{"checkbox1".$v->id}}>
                                                            </label>
                                                        </div>
                                                    </th>
                                                    <th>{{$v->name}}</th>
                                                        <th>{{$v->card_id	}}</th>
                                                        <th>{{$v->customer_id}}</th>
                                                        <th>{{$v->mobile}}</th>
                                                        <th>{{$v->street}}</th>



                                                        <th width="40px">{{trim(substr($v->plan->name,0,3))}}Mbps</th>


                                                        <th>
                                                            <?php $count =count($v->amount);
                                                            $key_for_credit=0;
                                                            foreach ($v->amount as $key=>$value)
                                                            {
                                                                if($value->credit)
                                                                {
                                                                    $key_for_credit=$key;

                                                                }

                                                            }

                                                            if($key_for_credit)
                                                            {
                                                                echo   date('d-M-y',strtotime
                                                                ($v->amount[$key_for_credit]->created_at));
//                                                                       echo $v->generatedBill[$count-1]->created_at;
                                                            }
                                                            else{

                                                                echo'  <span class="label
                                                                      label-warning">N/A</span>';
                                                            }
                                                            ?>


                                                        </th>






                                                        <th> <?php if($v->outstanding>0){
                                                                echo '<span class="label label-success"
                                                                    style="font-size: 15px; !important;"  ">'
                                                                    .$v->outstanding.'</span>';
                                                            }
                                                            elseif($v->outstanding<0){
                                                                echo '<span class="label label-danger" style="font-size: 15px; !important;">'.$v->outstanding
                                                                    .'</span>';

                                                            }
                                                            ?> </th>

                                                        {{--<td>--}}
                                                        {{--<button data-button='{"amount_id": "{{$v->id}}"}'--}}
                                                        {{--class="btn btn-default--}}
                                                        {{--btn-circle show-ledger " data-toggle="modal" id="show-ledger"><i--}}
                                                        {{--class="fa fa-pencil-square-o"></i></button>--}}
                                                        {{--</td>--}}

                                                    </form>
                                                </tr>
                                            @endforeach
                                            </tbody>

                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Row -->



            </form>




            @include('layout.footer')



        </div>
        <!-- /Main Content -->
    </div>

@endsection

@push('pagescript')
    <script type="text/javascript" >



        $(document).ready(function() {

            $('#example').DataTable();

            $(document).on('click','#show-ledger',function (e) {
                e.preventDefault();
                var data = $.parseJSON($(this).attr('data-button'));

                $.ajax({
                    url: APP_URL + "/payment/show-ledger",
                    type: "POST",
                    data: {
                        id: data.amount_id,
                        _token: token
                    },
                    // cache: false,
                    dataType: "json",
                    success: function (data) {
                        var temp = [];
                        $.each(data,function (index,value) {
                            var x = {s_no:index+1,remark:value.remark}
                            temp.push(x)
                        })
                        table = $('#datable_1').dataTable();
                        oSettings = table.fnSettings();

                        table.fnClearTable(this);

                        for (var i=0; i<temp.length; i++)
                        {
                            table.oApi._fnAddData(oSettings, temp[i]);
                        }

                        oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
                        table.fnDraw();
                        $('#exampleModal').modal('show');

                    }
                });
            })

            $( ".datepicker" ).datepicker();


        } );


        function checkAll(ele) {
            var checkboxes = document.getElementsByClassName('all-checkbox');
            if (ele.checked) {
                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].type == 'checkbox') {
                        checkboxes[i].checked = true;
                    }
                }
            } else {
                for (var i = 0; i < checkboxes.length; i++) {
                    console.log(i)
                    if (checkboxes[i].type == 'checkbox') {
                        checkboxes[i].checked = false;
                    }
                }
            }
        }


    </script>
@endpush
@extends('app')

@section('title','Payment | client')

@section('content')
    <!-- Main Content -->
    <div class="page-wrapper">
        <div class="container-fluid">

            <!-- Title -->
            <div class="row heading-bg">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h5 class="txt-dark">Payment Client</h5>
                </div>
                <!-- Breadcrumb -->
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="index.html">Dashboard</a></li>
                        <li class="active"><span>Payment</span></li>
                        {{--<li><a href="#"><span>Zone</span></a></li>--}}
                    </ol>
                </div>
                <!-- /Breadcrumb -->
            </div>
            <!-- /Title -->





            <!-- Row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h6 class="panel-title txt-dark">All panding payments</h6>
                                @if(isset($total))  <h6 class="panel-title txt-dark">Total :: <span style="font-family: bold; font-size:
                                30px;"
                                    > {{$total}}</span></h6>@endif

                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="table-wrap">
                                    <div class="table-responsive">


                                        <table class="table jsgrid-table table-hover display  pb-30" id="example" class="display" style="width:100%">
                                            <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>card id</th>
                                                <th>Cust id</th>
                                                <th>Add</th>

                                                <th>Plan</th>
                                                <th>Last payment date</th>
                                                <th>payment date</th>
                                                <th>Payment Mode</th>
                                                <th>Reference No</th>
                                                <th>Outstnding</th>
                                                <th>Pay Amount</th>



                                                <th>Action</th>



                                            </tr>
                                            </thead>
                                            <tbody>

                                            @foreach($clients as $k =>$v)

                                                <tr>
                                                    <form method="post" action={{url('payment/process-payment')}}>
                                                        @csrf
                                                        <input type="hidden" name="client_id" value={{$v->id}}  >
                                                        {{--<input type="hidden" name="exicutive_id" value={{Auth::user()}} >--}}
                                                    <th>{{$v->name}}</th>
                                                    <th>{{$v->card_id	}}</th>
                                                    <th>{{$v->customer_id}}</th>
                                                    <th>{{$v->street}}</th>



                                                    <th width="40px">{{trim(substr($v->plan->name,0,3))}}Mbps</th>


                                                    <th>
                                                    <?php $count =count($v->amount);
                                                            $key_for_credit=0;
                                                        foreach ($v->amount as $key=>$value)
                                                            {
                                                                if($value->credit)
                                                                    {
                                                                        $key_for_credit=$key;

                                                                    }

                                                            }

                                                    if($key_for_credit)
                                                    {
                                                        echo   date('d-M-y',strtotime
                                                        ($v->amount[$key_for_credit]->created_at));
//                                                                       echo $v->generatedBill[$count-1]->created_at;
                                                    }
                                                    else{

                                                        echo'  <span class="label
                                                                      label-warning">N/A</span>';
                                                    }
                                                    ?>


                                                    </th>

                                                   <td>
                                                       <input style="width:90px;" type="text" name="payment_date"
                                                              value="<?php echo date
                                                       ('m/d/Y');  ?>"
                                                              class="datepicker">
                                                   </td>

                                                        <td style="width: 100px">

                                                            <select  name="pay_chanel" class=""
                                                                     required>
                                                                <option value="cash" >Cash</option>

                                                                    <option value="paytm">Paytm</option>
                                                                    <option value="card">card</option>
                                                                    <option value="netbanking">netbaning</option>
                                                                    <option value="portal">Portal</option>
                                                                    <option value="cheque">Cheque</option>
                                                            </select>

                                                        </td>
                                                        <td><input style="width:90px;" type=text name="reference_no"
                                                                   placeholder="Reference No" style="width: 124px;"></td>



                                                    <th> <?php if($v->outstanding>0){
                                                                    echo '<span class="label label-success"
                                                                    style="font-size: 15px; !important;"  ">'
                                                                        .$v->outstanding.'</span>';
                                                        }
                                                        elseif($v->outstanding<0){
                                                            echo '<span class="label label-danger" style="font-size: 15px; !important;">'.$v->outstanding
                                                                .'</span>';

                                                        }
                                                        ?> </th>
                                                    <th><input style="width:70px;" name="credit" type="number"
                                                               placeholder="type here
                                                    paymemnt"
                                                               required
                                                        ></th>
                                                    <td><button class="btn btn-small btn-primary ">Pay</button></td>
                                                        {{--<td>--}}
                                                            {{--<button data-button='{"amount_id": "{{$v->id}}"}'--}}
                                                                    {{--class="btn btn-default--}}
                                                                        {{--btn-circle show-ledger " data-toggle="modal" id="show-ledger"><i--}}
                                                                        {{--class="fa fa-pencil-square-o"></i></button>--}}
                                                        {{--</td>--}}

                                                    </form>
                                                </tr>
                                            @endforeach
                                            </tbody>

                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Row -->

            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="display: none;">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color: slategrey">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h5 class="modal-title" id="exampleModalLabel1">Ledger Details</h5>
                        </div>
                        <div class="modal-body">
                            <div class="table-wrap">
                                <div class="table-responsive">
                                    <table id="datable_1" class="table jsgrid-table table-hover display  pb-30" >
                                        <thead>
                                        <tr>
                                            <th>S No</th>
                                            <th>Remark</th>

                                        </tr>
                                        </thead>

                                        <tbody>


                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>



            @include('layout.footer')



        </div>
        <!-- /Main Content -->
    </div>

@endsection

@push('pagescript')
    <script type="text/javascript" >



        $(document).ready(function() {

           $('#example').DataTable();

        $(document).on('click','#show-ledger',function (e) {
            e.preventDefault();
            var data = $.parseJSON($(this).attr('data-button'));

            $.ajax({
                url: APP_URL + "/payment/show-ledger",
                type: "POST",
                data: {
                    id: data.amount_id,
                    _token: token
                },
                // cache: false,
                dataType: "json",
                success: function (data) {
                    var temp = [];
                    $.each(data,function (index,value) {
                        var x = {s_no:index+1,remark:value.remark}
                        temp.push(x)
                    })
                    table = $('#datable_1').dataTable();
                    oSettings = table.fnSettings();

                    table.fnClearTable(this);

                    for (var i=0; i<temp.length; i++)
                    {
                        table.oApi._fnAddData(oSettings, temp[i]);
                    }

                    oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
                    table.fnDraw();
                    $('#exampleModal').modal('show');

                }
            });
        })

            $( ".datepicker" ).datepicker();


        } );



    </script>
@endpush
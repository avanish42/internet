@extends('app')

@section('title','Recent | Payment')

@section('content')
    <!-- Main Content -->
    <div class="page-wrapper">
        <div class="container-fluid">

            <!-- Title -->
            <div class="row heading-bg">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h5 class="txt-dark">Recent Payment</h5>
                </div>
                <!-- Breadcrumb -->
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="index.html">Dashboard</a></li>
                        <li class="active"><span>Zone</span></li>
                        {{--<li><a href="#"><span>Zone</span></a></li>--}}
                    </ol>
                </div>
                <!-- /Breadcrumb -->
            </div>
            <!-- /Title -->





            <!-- Row -->

            <form  method="post" action={{URL::asset('bill/cancle-generate')}} class="form-inline">
                @csrf
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-dark">All Recent Payments</h6>

                                </div>


                                <div class="pull-right">
                                    <div style="display: inline" class="checkbox">
                                        <input  id="checkbox-paprent" type="checkbox" onchange="checkAll(this)">
                                        <label for="checkbox-parent">ALL
                                        </label>
                                    </div>

                                    {{--<div style="display: inline" class="checkbox">--}}
                                    {{--<input  id="checkbox2" name="sms_checkbox" value="1" type="checkbox" >--}}
                                    {{--<label for="checkbox2">SMS--}}
                                    {{--</label>--}}
                                    {{--</div>--}}

                                    <button type="submit" class="btn btn-success">Cancle</button>
                                    {{--<button  type="submit" class="btn btn-primary">generate-send</button>--}}

                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="table-wrap">
                                        <div class="table-responsive">



                                            <table class="table jsgrid-table table-hover display  pb-30" id="example-bill"
                                                   class="display" style="width:100%">
                                                <thead>
                                                <tr>

                                                    <th>id</th>
                                                    <th>Name</th>
                                                    <th>Customer Id</th>
                                                    <th>Card Id</th>
                                                    <th>User ID</th>
                                                    <th>Address</th>
                                                    <th>Amount</th>
                                                    <th>Payment Date</th>

                                                    {{--<th>payment date</th>--}}



                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($results as $k=> $v)
                                                    <tr>

                                                        <th>{{++$k}}</th>
                                                        <th>{{$v->client->name}}</th>
                                                        <th>{{$v->client->customer_id}}</th>
                                                        <th>{{$v->client->card_id}}</th>
                                                        <th>{{$v->client->user_id}}</th>
                                                        <th>{{$v->client->street}}</th>
                                                        <th>{{ $v->credit}}</th>
                                                        <th>{{ date('d-M-y',strtotime($v->payment_date))}}</th>


                                                    </tr>

                                                @endforeach



                                                </tbody>

                                            </table>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <!-- /Row -->




            @include('layout.footer')



        </div>
        <!-- /Main Content -->
    </div>

@endsection

@push('pagescript')
    <script type="text/javascript" >




        // $(document).ready(function() {
        //     $('#example').DataTable();


        $(document).ready(function() {
            $('#example-bill').DataTable( {
                dom: 'lBfrtip',
                "lengthMenu": [[20, 50, 100, -1], [20, 50, 100, "All"]],
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]

            } );
        } );




        function checkAll(ele) {
            var checkboxes = document.getElementsByClassName('all-checkbox');
            if (ele.checked) {
                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].type == 'checkbox') {
                        checkboxes[i].checked = true;
                    }
                }
            } else {
                for (var i = 0; i < checkboxes.length; i++) {
                    console.log(i)
                    if (checkboxes[i].type == 'checkbox') {
                        checkboxes[i].checked = false;
                    }
                }
            }
        }


    </script>
@endpush
@extends('app')

@section('title','Dashboard')

@section('content')

    <!-- Main Cont
    ent -->
    <div class="page-wrapper">
        <div class="container-fluid pt-25">

            <!-- Row -->

            <!-- /Row -->

            <!-- Row -->
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <div class="panel panel-default card-view pa-0">
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body pa-0">
                                <div class="sm-data-box bg-blue">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-xs-6 text-center pl-0 pr-0 data-wrap-left">
                                                <span class="txt-light block counter">&#8377 <span
                                                            class="counter-anim">{{$dashboard_result['total_payment_as_per_db']}} </span> </span>
                                                <span class="weight-500 uppercase-font txt-light block">Total
                                                    Payment(DB)</span>
</span>
                                            </div>
                                            {{--<div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">--}}
                                                {{--<i class="zmdi zmdi-male-female txt-light data-right-rep-icon"></i>--}}
                                            {{--</div>--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <div class="panel panel-default card-view pa-0">
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body pa-0">
                                <div class="sm-data-box bg-yellow">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-xs-6 text-center pl-0 pr-0 data-wrap-left">
                                                <span class="txt-light block counter">&#8377<span
                                                            class="counter-anim">{{$dashboard_result['total_payment_as_per_bill']}}</span></span>
                                                <span class="weight-500 uppercase-font txt-light block">Total
                                                    Payment(Bills)</span>

                                            </div>
                                            {{--<div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">--}}
                                                {{--<i class="zmdi zmdi-redo txt-light data-right-rep-icon"></i>--}}
                                            {{--</div>--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <div class="panel panel-default card-view pa-0">
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body pa-0">
                                <div class="sm-data-box bg-green">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-xs-6 text-center pl-0 pr-0 data-wrap-left">
                                                <span class="txt-light block counter">&#8377 <span
                                                            class="counter-anim">{{$dashboard_result['total_payment_receive']}}</span></span>
                                                <span class="weight-500 uppercase-font txt-light block">Payment
                                                    Received</span>
                                            </div>
                                            {{--<div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">--}}
                                                {{--<i class="zmdi zmdi-file txt-light data-right-rep-icon"></i>--}}
                                            {{--</div>--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <div class="panel panel-default card-view pa-0">
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body pa-0">
                                <div class="sm-data-box bg-red">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-xs-6 text-center pl-0 pr-0 data-wrap-left">
                                                <span class="txt-light block counter">&#8377 <span
                                                            class="counter-anim">{{$dashboard_result['remaining_payment']}}</span></span>
                                                <span class="weight-500 uppercase-font txt-light block">Remaining</span>
                                            </div>
                                            {{--<div class="col-xs-6 text-center  pl-0 pr-0 pt-25  data-wrap-right">--}}
                                                {{--<div id="sparkline_4" style="width: 100px; overflow: hidden; margin: 0px auto;"></div>--}}
                                            {{--</div>--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Row -->

            <!-- Row -->
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                    <div class="panel panel-default card-view panel-refresh">
                        <div class="refresh-container">
                            <div class="la-anim-1"></div>
                        </div>
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h6 class="panel-title txt-dark">User Statics</h6>
                            </div>
                            <div class="pull-right">
                                <a href="#" class="pull-left inline-block refresh mr-15">
                                    <i class="zmdi zmdi-replay"></i>
                                </a>
                                <a href="#" class="pull-left inline-block full-screen mr-15">
                                    <i class="zmdi zmdi-fullscreen"></i>
                                </a>
                                <div class="pull-left inline-block dropdown">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false" role="button"><i class="zmdi zmdi-more-vert"></i></a>
                                    <ul class="dropdown-menu bullet dropdown-menu-right"  role="menu">
                                        <li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Edit</a></li>
                                        <li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-share" aria-hidden="true"></i>Delete</a></li>
                                        <li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-trash" aria-hidden="true"></i>New</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body row pa-0">
                                <div class="table-wrap">
                                    <div class="table-responsive">
                                        <table class="table table-hover mb-0">
                                            <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Current Month</th>
                                                <th>Last month</th>


                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td><span class="label label-primary">TOTAL USER </span></td>
                                                <td>{{ $dashboard_result['total_user_till_current_month']}}</td>
                                                <td>{{ $dashboard_result['total_user_till_last_month']}}</td>


                                            </tr>
                                            <tr>
                                                <td><span class="label label-danger">ACTIVE USER</span></td>
                                                <td>{{ $dashboard_result['total_active_client_till_current_month']}}</td>
                                                <td>{{ $dashboard_result['total_active_client_till_last_month']}}</td>

                                            </tr>
                                            <tr>
                                                <td><span class="label label-default">SUSPENDED USER </span></td>
                                                <td>{{ $dashboard_result['total_suspended_client_till_current_month']}}</td>
                                                <td>{{ $dashboard_result['total_suspended_client_till_last_month']}}</td>

                                            </tr>
                                            <tr>
                                                <td><span class="label label-default">NEW CONNECTION</span></td>
                                                <td>{{ $dashboard_result['this_month_new_user']}}</td>
                                                <td>{{ $dashboard_result['last_month_new_user']}}</td>


                                            </tr>


                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                    <div class="panel panel-default card-view panel-refresh">
                        <div class="refresh-container">
                            <div class="la-anim-1"></div>
                        </div>
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h6 class="panel-title txt-dark">Payment Status Zone wise</h6>
                            </div>

                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body row pa-0">
                                <div class="table-wrap">
                                    <div class="table-responsive">
                                        <table class="table table-hover mb-0">
                                            <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Total (DB)</th>
                                                <th>Total (generate)</th>
                                                <th>Received</th>
                                                <th>Pending</th>


                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($dashboard_result['all_zone'] as $k =>$v)
                                                <tr>
                                                    <td>{{$v->name}}</td>
                                                    <td>{{$v->total}}</td>
                                                    <td>{{$v->total_generate}}</td>
                                                    <td>{{$v->total_paid}}</td>
                                                    <td>{{$v->total_remaining}}</td>



                                                </tr>

                                            @endforeach



                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Row -->





            <!-- Row -->
            <div class="row">


                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                    <div class="panel panel-default card-view panel-refresh">
                        <div class="refresh-container">
                            <div class="la-anim-1"></div>
                        </div>
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h6 class="panel-title txt-dark">Payment Status Exicutive  wise</h6>
                            </div>

                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body row pa-0">
                                <div class="table-wrap">
                                    <div class="table-responsive">
                                        <table class="table table-hover mb-0">
                                            <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Total (DB)</th>
                                                <th>Total (generate)</th>
                                                <th>Received</th>
                                                <th>Pending</th>


                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($dashboard_result['all_zone'] as $k =>$v)
                                                <tr>
                                                    <td>{{$v->name}}</td>
                                                    <td>{{$v->total}}</td>
                                                    <td>{{$v->total_generate}}</td>
                                                    <td>{{$v->total_paid}}</td>
                                                    <td>{{$v->total_remaining}}</td>



                                                </tr>

                                            @endforeach



                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Row -->
        </div>

        <!-- Footer -->
      @include('layout.footer');
        <!-- /Footer -->

    </div>
    <!-- /Main Content -->


@endsection
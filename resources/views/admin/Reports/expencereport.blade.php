@extends('app')

@section('title','Expence | Report')

@section('content')
    <!-- Main Content -->
    <div class="page-wrapper">
        <div class="container-fluid">

            <!-- Title -->
            <div class="row heading-bg">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h5 class="txt-dark">Area Master</h5>
                </div>
                <!-- Breadcrumb -->
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="index.html">Dashboard</a></li>
                        <li class="active"><span>Area</span></li>
                        {{--<li><a href="#"><span>Zone</span></a></li>--}}
                    </ol>
                </div>
                <!-- /Breadcrumb -->
            </div>
            <!-- /Title -->

            <!-- Row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default card-view" >
                        <div class="panel-heading">
                            <div class="pull-left" >
                                <h6 class="panel-title txt-dark">Add New Area</h6>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="form-wrap">
                                    <form class="form-horizontal" method="post" action="{{URL::asset('reports/expence-report')
                                    }}">
                                        <div class="form-group mb-0">
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    @csrf

                                                    <div class="col-sm-3">
                                                        <label class="control-label mb-10">From*</label>

                                                        <input  type="text" name="from"
                                                                class="form-control datepicker rounded-input
                                                    " autocomplete="off" autocomplete="off">
                                                    </div>

                                                    <div class="col-sm-3">
                                                        <label class="control-label mb-10">To*</label>

                                                        <input id="datepicker" type="text" name="to"
                                                               class="form-control datepicker rounded-input
                                                    " autocomplete="off" autocomplete="off">
                                                    </div>



                                                    {{--<div class="col-sm-3">--}}
                                                    {{--<label class="control-label mb-10">Select Area*</label>--}}
                                                    {{--<select id="zone-select"  name="zone_id" class="form-control--}}
                                                    {{--rounded-input"--}}
                                                    {{-->--}}
                                                    {{--<option disabled selected value>-- Select zone --</option>--}}
                                                    {{--@foreach($zones as $zone)--}}
                                                    {{--<option value="{{$zone->id}}">{{$zone->name}}</option>--}}
                                                    {{--@endforeach--}}

                                                    {{--</select>--}}
                                                    {{--</div>--}}

                                                    {{--<div class="col-sm-3">--}}
                                                    {{--<label class="control-label mb-10">Select Area*</label>--}}
                                                    {{--<select id="area-select" name="area_id" class="form-control--}}
                                                    {{--rounded-input"--}}
                                                    {{-->--}}
                                                    {{--<option disabled selected value>-- Select Area --</option>--}}

                                                    {{--</select>--}}
                                                    {{--</div>--}}



                                                    <div class="col-sm-2">
                                                        <div class="">
                                                            <label class="control-label mb-10"></label>
                                                            <input type="submit" value="Search" class="form-control
                                                        rounded-input btn btn-success" placeholder="Any
                                                        Remark/Description/Details.
                                                        ..">
                                                            {{--<button type="submit" class="btn btn-success btn-anim"><i--}}
                                                            {{--class="icon-arrow-right"></i><span class="btn-text">Add</span></button>--}}

                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <div class="">
                                                            <label class="control-label mb-10"></label>
                                                            <input type="reset" value="Reset" class="form-control
                                                        rounded-input btn btn-danger" placeholder="Any
                                                        Remark/Description/Details.
                                                        ..">
                                                            {{--<button type="submit" class="btn btn-success btn-anim"><i--}}
                                                            {{--class="icon-arrow-right"></i><span class="btn-text">Add</span></button>--}}

                                                        </div>
                                                    </div>

                                                    <div class="col-sm-4">

                                                    </div>



                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Row -->



            <!-- Row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h6 class="panel-title txt-dark">Total Expence :<span style="font-size: 15px"
                                                                                      class="label
                                label-warning">{{isset($expence)?$expence:"N/A"}}</span> </h6>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="table-wrap">
                                    <div class="table-responsive">
                                        <table id="datable_12" class="table jsgrid-table table-hover display  pb-30" >
                                            <thead>
                                            <tr>
                                                <th>S No</th>
                                                <th>Name</th>
                                                <th>remark</th>
                                                <th>description</th>
                                                <th>Amount</th>
                                                <th>Date</th>


                                            </tr>
                                            </thead>

                                            <tbody>
                                            @if(isset($data))

                                                @foreach($data as $k => $v)
                                                    <tr>
                                                        <td>{{++$k}}</td>
                                                        <td>{{$v->exicutive->name?$v->exicutive->name:"N/A"}}</td>
                                                        <td>{{$v->remark?$v->remark:"N/A"}}</td>
                                                        <td>{{$v->description?$v->description:"N/A"}}</td>
                                                        <td>{{$v->amount?$v->amount:"N/A"}}</td>
                                                        <td>{{$v->created_at?$v->created_at:"N/A"}}</td>
                                                    </tr>

                                                @endforeach
                                            @endif

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Row -->

            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="display: none;">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color: slategrey">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h5 class="modal-title" id="exampleModalLabel1">Edit Zone Detials</h5>
                        </div>
                        <div class="modal-body">
                            <form method="post" action="{{URL::asset('update-area')}}">
                                @csrf
                                {{--<input type="hidden" name="id" id="zone-id" >--}}
                                <div class="form-group">
                                    <label for="recipient-name" class="control-label mb-10">ID:</label>
                                    <input type="text" name="id" class="form-control" id="area-id" readonly>
                                </div>

                                <div class="form-group">
                                    <label for="recipient-name" class="control-label mb-10">Name:</label>
                                    <input type="text" name="name" class="form-control" id="area-name">
                                </div>



                                <div class="form-group">
                                    <label for="recipient-name" class="control-label mb-10">Code</label>
                                    <input type="text" name="code" class="form-control" id="area-code">
                                </div>

                                <div class="form-group">
                                    <label for="recipient-name" class="control-label mb-10">Description</label>
                                    <input type="text" name="remark" class="form-control" id="area-description">
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </div>

                            </form>
                        </div>

                    </div>
                </div>
            </div>



            @include('layout.footer')



        </div>
        <!-- /Main Content -->
    </div>

@endsection



@push('pagescript')

    <script src={{URL::asset("vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js")}} ></script>
    <script src={{URL::asset("vendors/bower_components/datatables.net-buttons/js/buttons.flash.min.js")}} ></script>
    <script src={{URL::asset("vendors/bower_components/jszip/dist/jszip.min.js")}} ></script>
    <script src={{URL::asset("vendors/bower_components/pdfmake/build/pdfmake.min.js")}} ></script>
    <script src={{URL::asset("vendors/bower_components/pdfmake/build/vfs_fonts.js")}} ></script>
    <script src={{URL::asset("vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js")}} ></script>
    <script src={{URL::asset("vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js")}} ></script>
    <script src={{URL::asset("dist/js/export-table-data.js")}} ></script>
    <script type="text/javascript" >



        $(document).ready(function() {

            // $('#example').DataTable();



            $( ".datepicker" ).datepicker();
            $( "#datepicker" ).datepicker();



            $('#datable_12').DataTable(
                {
                    dom: 'lBfrtip',
                    "lengthMenu": [[100, 200, 300, -1], [100, 200, 300, "All"]],
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]

                }
            );


        } );



    </script>
@endpush



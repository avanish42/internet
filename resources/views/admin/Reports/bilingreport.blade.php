@extends('app')

@section('title','Bill | Report')

@section('content')
    <!-- Main Content -->
    <div class="page-wrapper">
        <div class="container-fluid">

            <!-- Title -->
            <div class="row heading-bg">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h5 class="txt-dark">Area Master</h5>
                </div>
                <!-- Breadcrumb -->
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="index.html">Dashboard</a></li>
                        <li class="active"><span>Area</span></li>
                        {{--<li><a href="#"><span>Zone</span></a></li>--}}
                    </ol>
                </div>
                <!-- /Breadcrumb -->
            </div>
            <!-- /Title -->

            <!-- Row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default card-view" >
                        <div class="panel-heading">
                            <div class="pull-left" >
                                <h6 class="panel-title txt-dark">Add New Area</h6>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="form-wrap">
                                    <form class="form-horizontal" method="post" action="{{URL::asset('reports/billing-report')
                                    }}">
                                        <div class="form-group mb-0">
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    @csrf

                                                    <div class="col-sm-3">
                                                    <label class="control-label mb-10">From*</label>

                                                    <input  type="text" name="from"
                                                    class="form-control datepicker rounded-input
                                                    " autocomplete="off" autocomplete="off">
                                                    </div>

                                                    <div class="col-sm-3">
                                                    <label class="control-label mb-10">To*</label>

                                                    <input id="datepicker" type="text" name="to"
                                                    class="form-control datepicker rounded-input
                                                    " autocomplete="off" autocomplete="off">
                                                    </div>



                                                    {{--<div class="col-sm-3">--}}
                                                        {{--<label class="control-label mb-10">Select Area*</label>--}}
                                                        {{--<select id="zone-select"  name="zone_id" class="form-control--}}
                                                        {{--rounded-input"--}}
                                                        {{-->--}}
                                                            {{--<option disabled selected value>-- Select zone --</option>--}}
                                                            {{--@foreach($zones as $zone)--}}
                                                                {{--<option value="{{$zone->id}}">{{$zone->name}}</option>--}}
                                                            {{--@endforeach--}}

                                                        {{--</select>--}}
                                                    {{--</div>--}}

                                                    {{--<div class="col-sm-3">--}}
                                                        {{--<label class="control-label mb-10">Select Area*</label>--}}
                                                        {{--<select id="area-select" name="area_id" class="form-control--}}
                                                        {{--rounded-input"--}}
                                                        {{-->--}}
                                                            {{--<option disabled selected value>-- Select Area --</option>--}}

                                                        {{--</select>--}}
                                                    {{--</div>--}}



                                                    <div class="col-sm-2">
                                                        <div class="">
                                                            <label class="control-label mb-10"></label>
                                                            <input type="submit" value="Search" class="form-control
                                                        rounded-input btn btn-success" placeholder="Any
                                                        Remark/Description/Details.
                                                        ..">
                                                            {{--<button type="submit" class="btn btn-success btn-anim"><i--}}
                                                            {{--class="icon-arrow-right"></i><span class="btn-text">Add</span></button>--}}

                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <div class="">
                                                            <label class="control-label mb-10"></label>
                                                            <input type="reset" value="Reset" class="form-control
                                                        rounded-input btn btn-danger" placeholder="Any
                                                        Remark/Description/Details.
                                                        ..">
                                                            {{--<button type="submit" class="btn btn-success btn-anim"><i--}}
                                                            {{--class="icon-arrow-right"></i><span class="btn-text">Add</span></button>--}}

                                                        </div>
                                                    </div>

                                                    <div class="col-sm-4">

                                                    </div>



                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Row -->



            <!-- Row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h6 class="panel-title txt-dark">All Zones</h6>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="table-wrap">
                                    <div class="table-responsive">
                                        <table id="datable_12" class="table jsgrid-table table-hover display  pb-30" >
                                            <thead>
                                            <tr>
                                                <th>S No</th>
                                                <th>Name</th>
                                                <th>type</th>
                                                <th>client id</th>
                                                <th>Card ID</th>
                                                <th>User Id</th>
                                                <th>Phone </th>
                                                <th>Tax</th>
                                                <th>Address</th>
                                                <th>Plan</th>
                                                <th>Plan Price</th>

                                                <th>Bill No</th>
                                                <th>Bill Amount</th>

                                            </tr>
                                            </thead>

                                            <tbody>
                                            @if(isset($data))

                                            @foreach($data as $k => $v)
                                                <tr>
                                                    <td>{{++$k}}</td>
                                                    <td>{{$v->client->name?$v->client->name:"N/A"}}</td>
                                                    <td>{{$v->client->coustmer_type?$v->client->coustmer_type:"N/A"}}</td>
                                                    <td>{{$v->client->customer_id?$v->client->customer_id:"N/A"}}</td>
                                                    <td>{{$v->client->card_id?$v->client->card_id:"N/A"}}</td>
                                                    <td>{{$v->client->user_id?$v->client->user_id:"N/A"}}</td>
                                                    <td>{{$v->client->Phone?$v->client->Phone:"N/A"}}</td>


                                                    <td style="width: 137px;">



                                                        <?php
                                                        $gst_class=($v->client->is_gst)?"label-success":"label-danger";
                                                        $pdf_class=($v->client->is_pdf)?"label-success":"label-danger"  ;
                                                        $auto_class=($v->client->auto_bill) ?"label-success":"label-danger" ;
                                                        $igst_class=($v->client->is_igst) ?"label-success":"label-danger" ;
                                                        $gstinclude_class=($v->client->is_gst_in_plan) ?
                                                            "label-success":"label-danger" ;
                                                        ?>

                                                        <span class="label <?php echo $pdf_class;  ?>">PDF</span>

                                                        / <span class="label <?php echo $gstinclude_class;?>">GST Include</span>
                                                        / <span class="label <?php echo $auto_class;?>">Auto </span>
                                                        / <span class="label <?php echo $igst_class;?>">IGST</span>
                                                        / <span class="label <?php echo $gst_class;?>">GST</span>


                                                    </td>
                                                    <td>{{$v->client->street?$v->client->street:"N/A"}}</td>
                                                    <td>{{$v->client->plan->name?$v->client->plan->name:"N/A"}}</td>
                                                    <td>{{$v->client->plan->price?$v->client->plan->price:"N/A"}}</td>
                                                    <td>{{$v->amount?$v->amount:"N/A"}}</td>
                                                    <td>{{$v->bill_no?$v->bill_no:"N/A"}}</td>





                                                </tr>

                                            @endforeach
                                                @endif

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Row -->

            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="display: none;">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color: slategrey">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h5 class="modal-title" id="exampleModalLabel1">Edit Zone Detials</h5>
                        </div>
                        <div class="modal-body">
                            <form method="post" action="{{URL::asset('update-area')}}">
                                @csrf
                                {{--<input type="hidden" name="id" id="zone-id" >--}}
                                <div class="form-group">
                                    <label for="recipient-name" class="control-label mb-10">ID:</label>
                                    <input type="text" name="id" class="form-control" id="area-id" readonly>
                                </div>

                                <div class="form-group">
                                    <label for="recipient-name" class="control-label mb-10">Name:</label>
                                    <input type="text" name="name" class="form-control" id="area-name">
                                </div>



                                <div class="form-group">
                                    <label for="recipient-name" class="control-label mb-10">Code</label>
                                    <input type="text" name="code" class="form-control" id="area-code">
                                </div>

                                <div class="form-group">
                                    <label for="recipient-name" class="control-label mb-10">Description</label>
                                    <input type="text" name="remark" class="form-control" id="area-description">
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </div>

                            </form>
                        </div>

                    </div>
                </div>
            </div>



            @include('layout.footer')



        </div>
        <!-- /Main Content -->
    </div>

@endsection



@push('pagescript')

    <script src={{URL::asset("vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js")}} ></script>
    <script src={{URL::asset("vendors/bower_components/datatables.net-buttons/js/buttons.flash.min.js")}} ></script>
    <script src={{URL::asset("vendors/bower_components/jszip/dist/jszip.min.js")}} ></script>
    <script src={{URL::asset("vendors/bower_components/pdfmake/build/pdfmake.min.js")}} ></script>
    <script src={{URL::asset("vendors/bower_components/pdfmake/build/vfs_fonts.js")}} ></script>
    <script src={{URL::asset("vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js")}} ></script>
    <script src={{URL::asset("vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js")}} ></script>
    <script src={{URL::asset("dist/js/export-table-data.js")}} ></script>
    <script type="text/javascript" >



        $(document).ready(function() {

            // $('#example').DataTable();



            $( ".datepicker" ).datepicker();
            $( "#datepicker" ).datepicker();



            $('#datable_12').DataTable(
                {
                    dom: 'lBfrtip',
                    "lengthMenu": [[100, 200, 300, -1], [100, 200, 300, "All"]],
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]

                }
            );


        } );



    </script>
@endpush



@extends('app')

@section('title','Filter-Client')

@section('content')
    <!-- Main Content -->
    <div class="page-wrapper">
        <div class="container-fluid">

            <!-- Title -->
            <div class="row heading-bg">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h5 class="txt-dark">Apply Filters </h5>
                </div>



                <!-- /Breadcrumb -->
            </div>
            <!-- /Title -->

            <!-- Row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default card-view" >
                        <div class="panel-heading">
                            <div class="pull-left" >
                                <h6 class="panel-title txt-dark">Search The  Client by using filters</h6>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="form-wrap">
                                    <form class="form-horizontal" method="post" action="{{URL::asset
                                    ('notification/search-client-by-area')}}">
                                        <div class="form-group mb-0">
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    @csrf

                                                    {{--<div class="col-sm-3">--}}
                                                        {{--<label class="control-label mb-10">From*</label>--}}

                                                        {{--<input  type="text" name="from"--}}
                                                                {{--class="form-control datepicker rounded-input--}}
                                                        {{--" autocomplete="off" autocomplete="off">--}}
                                                    {{--</div>--}}

                                                    {{--<div class="col-sm-3">--}}
                                                        {{--<label class="control-label mb-10">To*</label>--}}

                                                        {{--<input id="datepicker" type="text" name="to"--}}
                                                               {{--class="form-control datepicker rounded-input--}}
                                                        {{--" autocomplete="off" autocomplete="off">--}}
                                                    {{--</div>--}}

                                                    <div class="col-sm-3">
                                                        <label class="control-label mb-10">Select Zone*</label>
                                                        <select name="zone_id" class="form-control rounded-input"
                                                                required>
                                                            <option disabled selected value>-- Select Zone --</option>
                                                            @foreach($zones as $zone)
                                                                <option
                                                                        value="{{$zone->id}}">{{$zone->name}}
                                                                </option>
                                                            @endforeach

                                                        </select>
                                                    </div>



                                                    <div class="col-sm-3">
                                                        <label class="control-label mb-10">Select Area*</label>
                                                        <select name="area_id" class="form-control rounded-input"
                                                                >
                                                            <option disabled selected value>-- Select Plan --</option>
                                                            @foreach($areas as $area)
                                                            <option
                                                            value="{{$area->id}}">{{$area->name}}
                                                           </option>
                                                            @endforeach

                                                        </select>
                                                    </div>





                                                    <div class="col-sm-2">
                                                        <div class="">
                                                            <label class="control-label mb-10"></label>
                                                            <input type="submit" value="Search" class="form-control
                                                        rounded-input btn btn-success" placeholder="Any
                                                        Remark/Description/Details.
                                                        ..">
                                                            {{--<button type="submit" class="btn btn-success btn-anim"><i--}}
                                                            {{--class="icon-arrow-right"></i><span class="btn-text">Add</span></button>--}}

                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <div class="">
                                                            <label class="control-label mb-10"></label>
                                                            <input type="reset" value="Reset" class="form-control
                                                        rounded-input btn btn-danger" placeholder="Any
                                                        Remark/Description/Details.
                                                        ..">
                                                            {{--<button type="submit" class="btn btn-success btn-anim"><i--}}
                                                            {{--class="icon-arrow-right"></i><span class="btn-text">Add</span></button>--}}

                                                        </div>
                                                    </div>

                                                    <div class="col-sm-4">

                                                    </div>



                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- /Row -->


            <!-- Row -->
            <form  method="post" action={{URL::asset('notification/send-area-notifications')}} class="form-inline">
                @csrf
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h6 class="panel-title txt-dark">Message Details</h6>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="form-wrap">


                                        <div class="form-group">
                                            <label class="control-label mb-10 text-left">Title <span class="help">
                                                    </span></label>
                                            <input type="text" class="form-control" name="title" value="Notification">
                                        </div>

                                        <div class="form-group">
                                            <label  class="control-label mb-25 text-left">Text
                                                area</label>
                                            <textarea name="message" class="form-control " style="width: 500px;
                                            @import "
                                                      rows="2"></textarea>
                                        </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Row -->


            <!-- Row -->

            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h6 class="panel-title txt-dark">Filtered Clients</h6>


                            </div>
                            <div class="pull-right">
                                <div style="display: inline" class="checkbox">
                                    <input  id="checkbox-paprent" type="checkbox" onchange="checkAll(this)">
                                    <label for="checkbox-parent">ALL
                                    </label>
                                </div>
                                <div style="display: inline" class="checkbox">
                                    <input  id="checkbox2" name="email_checkbox" value="1" type="checkbox" >
                                    <label for="checkbox2">Email
                                    </label>
                                </div>

                                <div style="display: inline" class="checkbox">
                                    <input  id="checkbox2" name="sms_checkbox" value="1" type="checkbox" >
                                    <label for="checkbox2">SMS
                                    </label>
                                </div>

                                <button type="submit" class="btn btn-success">Send</button>
                                {{--<button  type="submit" class="btn btn-primary">generate-send</button>--}}

                            </div>
                            <div class="clearfix"></div>



                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="table-wrap">
                                    <div class="table-responsive">
                                        <table id="datable_123" class="table jsgrid-table table-hover display  pb-30" >
                                            <thead>
                                            <tr>
                                                <th>Select</th>
                                                <th>S No</th>
                                                <th>Name</th>
                                                <th>Email</th>

                                                <th>Mobile</th>
                                                <th>Address</th>


                                            </tr>
                                            </thead>

                                            <tbody>
                                            @foreach($clients as $k=> $client)
                                            <tr>
                                                <th><div class="checkbox">
                                                        <input class="all-checkbox" id={{"checkbox1".$client->id}}
                                                                type="checkbox" name='client_ids[]' value={{$client->id}}>
                                                        <label for={{"checkbox1".$client->id}}>
                                                        </label>
                                                    </div>
                                                </th>
                                                <td>{{++$k}}</td>
                                                <td>{{$client->name}}</td>
                                                <td> <?php echo isset($v->email)?"<span class='label
                                                    label-success'> .</span> ":"<span class='label
                                                    label-danger'> .</span> "?></td>
                                                <td>{{$client->mobile}}</td>
                                                <td>{{$client->street}}</td>


                                            </tr>

                                            @endforeach


                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </form>
            <!-- /Row -->

            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="display: none;">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color: slategrey">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h5 class="modal-title" id="exampleModalLabel1">Edit Zone Detials</h5>
                        </div>
                        <div class="modal-body">
                            <form method="post" action="{{URL::asset('update-area')}}">
                                @csrf
                                {{--<input type="hidden" name="id" id="zone-id" >--}}
                                <div class="form-group">
                                    <label for="recipient-name" class="control-label mb-10">ID:</label>
                                    <input type="text" name="id" class="form-control" id="area-id" readonly>
                                </div>

                                <div class="form-group">
                                    <label for="recipient-name" class="control-label mb-10">Name:</label>
                                    <input type="text" name="name" class="form-control" id="area-name">
                                </div>



                                <div class="form-group">
                                    <label for="recipient-name" class="control-label mb-10">Code</label>
                                    <input type="text" name="code" class="form-control" id="area-code">
                                </div>

                                <div class="form-group">
                                    <label for="recipient-name" class="control-label mb-10">Description</label>
                                    <input type="text" name="remark" class="form-control" id="area-description">
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </div>

                            </form>
                        </div>

                    </div>
                </div>
            </div>



            @include('layout.footer')



        </div>
        <!-- /Main Content -->
    </div>

@endsection



@push('pagescript')
    <script type="text/javascript" >



        $(document).ready(function() {

            // $('#example').DataTable();



            $( ".datepicker" ).datepicker();
            $( "#datepicker" ).datepicker();



            $('#datable_123 ').DataTable(
                    {
                    dom: 'lBfrtip',
                    "lengthMenu": [[10, 50, 200, -1], [10, 50, 200, "All"]],
                //     buttons: [
                //         'copy', 'csv', 'excel', 'pdf', 'print'
                //     ]
                //
                }
            );


        } );


        function checkAll(ele) {
            var checkboxes = document.getElementsByClassName('all-checkbox');
            if (ele.checked) {
                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].type == 'checkbox') {
                        checkboxes[i].checked = true;
                    }
                }
            } else {
                for (var i = 0; i < checkboxes.length; i++) {
                    console.log(i)
                    if (checkboxes[i].type == 'checkbox') {
                        checkboxes[i].checked = false;
                    }
                }
            }
        }



    </script>
@endpush



@extends('client.app')
@section('title','Home ')

@section('main-content')

    <!-- Content Start -->
    <!-- Content Start -->
    <div id="main">
        <!-- Slider Start-->
        <div class="fullwidthbanner-container">
            <div class="fullwidthbanner rslider">
                <ul>
                    <!-- THE FIRST SLIDE -->
                    <li data-transition="fade" data-slotamount="7" data-masterspeed="500"  data-saveperformance="on">
                        <img src={{asset("client/img/slider/slider-bg-1.jpg")}}   alt="Pixma"
                             data-lazyload={{asset("client/img/slider/slider-bg-1.jpg")}}
                                     data-bgposition="centertop" data-bgfit="cover" data-bgrepeat="no-repeat">

                        <div class="caption modern_big_bluebg h1 lft tp-caption start"
                             data-x="40"
                             data-y="85"
                             data-speed="700"
                             data-endspeed="800"
                             data-start="1000"
                             data-easing="easeOutQuint"
                             data-endeasing="easeOutQuint">
                            <h3>Your personal - speed </h3>
                        </div>
                        <div class="caption list_slide lfb tp-caption start"
                             data-easing="easeOutExpo"
                             data-start="1400"
                             data-speed="1050"
                             data-y="180"
                             data-x="40">
                            <div class="list-slide">
                                <!--i class="fa fa-camera slide-icon"></i>-->
                                <h5 class="dblue"> & quality home connection Starting @ Rs. 1000 / mo </h5>
                            </div>
                        </div>
                        <div class="caption list_slide lfb tp-caption start"
                             data-easing="easeOutExpo"
                             data-start="1800"
                             data-speed="1200"
                             data-y="220"
                             data-x="40">
                            <div class="list-slide">
                                <i class="fa fa-check slide-icon"></i>
                                <h5 class="dblue"> 80 GB Data Limit </h5>
                            </div>
                        </div>
                        <div class="caption list_slide lfb tp-caption start"
                             data-easing="easeOutExpo"
                             data-start="2200"
                             data-speed="1350"
                             data-y="260"
                             data-x="40">
                            <div class="list-slide">
                                <i class="fa fa-check slide-icon"></i>
                                <h5 class="dblue"> Unlimited Bandwidth </h5>
                            </div>
                        </div>
                        <div class="caption list_slide lfb tp-caption start"
                             data-easing="easeOutExpo"
                             data-start="2600"
                             data-speed="1500"
                             data-y="300"
                             data-x="40">
                            <div class="list-slide">
                                <i class="fa fa-check slide-icon"></i>
                                <h5 class="dblue"> Unlimited Bandwidth </h5>
                            </div>
                        </div>
                        <div class="caption list_slide lfb tp-caption start"
                             data-easing="easeOutExpo"
                             data-start="3000"
                             data-speed="1650"
                             data-y="340"
                             data-x="40">
                            <div class="list-slide">
                                <i class="fa fa-check slide-icon"></i>
                                <h5 class="dblue"> No Installation charges </h5>
                            </div>
                        </div>
                        <div class="caption lfb caption_button_1 fadeout tp-caption start"
                             data-x="40"
                             data-y="400"
                             data-speed="900"
                             data-endspeed="300"
                             data-start="4500"
                             data-hoffset="-70"
                             data-easing="easeOutExpo">
                            <a class="btn-special hidden-xs btn-info" href="#" style="color:#fff;">More Plan</a>
                        </div>
                        <div class="caption lfb caption_button_2 fadeout tp-caption start"
                             data-x="210"
                             data-y="400"
                             data-speed="1000"
                             data-endspeed="300"
                             data-start="5200"
                             data-hoffset="-70"
                             data-easing="easeOutExpo">
                            <a class="btn-special hidden-xs btn-danger" href="#" style="color:#fff;">Buy Now</a>
                        </div>
                    </li>
                    <!-- THE FIRST SLIDE -->

                    <li data-transition="fade" data-slotamount="7" data-masterspeed="500"  data-saveperformance="on">
                        <img src={{asset("client/img/slider/slider-bg-2.jpg")}}  alt="Pixma" data-lazyload=
                        {{asset("client/img/slider/slider-bg-2
                        .jpg" )}} data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">

                        <div class="caption modern_big_bluebg h1 lft tp-caption start"
                             data-x="40"
                             data-y="85"
                             data-speed="700"
                             data-endspeed="800"
                             data-start="1000"
                             data-easing="easeOutQuint"
                             data-endeasing="easeOutQuint">
                            <h3>Your personal - speed </h3>
                        </div>
                        <div class="caption list_slide lfb tp-caption start"
                             data-easing="easeOutExpo"
                             data-start="1400"
                             data-speed="1050"
                             data-y="180"
                             data-x="40">
                            <div class="list-slide">
                                <!--i class="fa fa-camera slide-icon"></i>-->
                                <h5 class="dblue"  style="color:#fff"> & quality home connection Starting @ Rs. 1000 / mo </h5>
                            </div>
                        </div>
                        <div class="caption list_slide lfb tp-caption start"
                             data-easing="easeOutExpo"
                             data-start="1800"
                             data-speed="1200"
                             data-y="220"
                             data-x="40">
                            <div class="list-slide">
                                <i class="fa fa-check slide-icon"></i>
                                <h5 class="dblue" style="color:#fff"> 80 GB Data Limit </h5>
                            </div>
                        </div>
                        <div class="caption list_slide lfb tp-caption start"
                             data-easing="easeOutExpo"
                             data-start="2200"
                             data-speed="1350"
                             data-y="260"
                             data-x="40">
                            <div class="list-slide">
                                <i class="fa fa-check slide-icon"></i>
                                <h5 class="dblue" style="color:#fff"> Unlimited Bandwidth </h5>
                            </div>
                        </div>
                        <div class="caption list_slide lfb tp-caption start"
                             data-easing="easeOutExpo"
                             data-start="2600"
                             data-speed="1500"
                             data-y="300"
                             data-x="40">
                            <div class="list-slide">
                                <i class="fa fa-check slide-icon"></i>
                                <h5 class="dblue" style="color:#fff"> Unlimited Bandwidth </h5>
                            </div>
                        </div>
                        <div class="caption list_slide lfb tp-caption start"
                             data-easing="easeOutExpo"
                             data-start="3000"
                             data-speed="1650"
                             data-y="340"
                             data-x="40">
                            <div class="list-slide">
                                <i class="fa fa-check slide-icon"></i>
                                <h5 class="dblue" style="color:#fff"> No Installation charges </h5>
                            </div>
                        </div>
                        <div class="caption lfb caption_button_1 fadeout tp-caption start"
                             data-x="40"
                             data-y="400"
                             data-speed="900"

                             data-endspeed="300"
                             data-start="4500"
                             data-hoffset="-70"
                             data-easing="easeOutExpo">
                            <a class="btn-special hidden-xs btn-info" href="#" style="color:#fff;">More Plan</a>
                        </div>
                        <div class="caption lfb caption_button_2 fadeout tp-caption start"
                             data-x="210"
                             data-y="400"
                             data-speed="1000"
                             data-endspeed="300"
                             data-start="5200"
                             data-hoffset="-70"
                             data-easing="easeOutExpo">
                            <a class="btn-special hidden-xs btn-danger" href="#" style="color:#fff;">Buy Now</a>
                        </div>
                    </li>
                    <!--<li data-transition="fade" data-slotamount="7" data-masterspeed="500"  data-saveperformance="on">
                        <img src="img/slider/slider-bg-2.jpg"  alt="Pixma" data-lazyload="img/slider/slider-bg-2.jpg" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                        <div class="caption large_text sft"
                             data-x="660"
                             data-y="54"
                             data-speed="300"
                             data-start="800"
                             data-easing="easeOutExpo"  >TOUCH ENABLED</div>
                        <div class="caption medium_text sfl"
                             data-x="689"
                             data-y="92"
                             data-speed="300"
                             data-start="1100"
                             data-easing="easeOutExpo"  >AND</div>
                        <div class="caption large_text sfr"
                             data-x="738"
                             data-y="88"
                             data-speed="300"
                             data-start="1100"
                             data-easing="easeOutExpo"  ><span class="dblue">RESPONSIVE</span></div>
                        <div class="caption list_slide lfb tp-caption start"
                             data-easing="easeOutExpo"
                             data-start="1400"
                             data-speed="1050"
                             data-y="180"
                             data-x="660">
                            <div class="list-slide">
                                <i class="fa fa-eye slide-icon"></i>
                                <h5> Retina Ready </h5>
                            </div>
                        </div>
                        <div class="caption list_slide lfb tp-caption start"
                             data-easing="easeOutExpo"
                             data-start="1800"
                             data-speed="1200"
                             data-y="220"
                             data-x="660">
                            <div class="list-slide">
                                <i class="fa fa-table slide-icon"></i>
                                <h5> Responsive pricing tables </h5>
                            </div>
                        </div>
                        <div class="caption list_slide lfb tp-caption start"
                             data-easing="easeOutExpo"
                             data-start="2200"
                             data-speed="1350"
                             data-y="260"
                             data-x="660">
                            <div class="list-slide">
                                <i class="fa fa-check slide-icon"></i>
                                <h5> Crossbrowser Compatible </h5>
                            </div>
                        </div>
                        <div class="caption list_slide lfb tp-caption start"
                             data-easing="easeOutExpo"
                             data-start="2600"
                             data-speed="1350"
                             data-y="300"
                             data-x="660">
                            <div class="list-slide">
                                <i class="fa fa-twitter slide-icon"></i>
                                <h5> Twitter 1.1 API Support </h5>
                            </div>
                        </div>
                        <div class="caption list_slide lfb tp-caption start"
                             data-easing="easeOutExpo"
                             data-start="3000"
                             data-speed="1350"
                             data-y="340"
                             data-x="660">
                            <div class="list-slide">
                                <i class="fa fa-th slide-icon"></i>
                                <h5> Bootstrap Framework </h5>
                            </div>
                        </div>
                        <div class="caption lfl"
                             data-x="53"
                             data-y="30"
                             data-speed="300"
                             data-start="1400"
                             data-easing="easeOutExpo">
                            <img src="img/slider/responsive-imac.png" alt="iMac Responsive">
                        </div>
                        <div class="caption lfl"
                             data-x="323"
                             data-y="145"
                             data-speed="300"
                             data-start="1500"
                             data-easing="easeOutExpo">
                            <img src="img/slider/responsive-ipad.png" alt="iPad Responsive">
                        </div>
                        <div class="caption lfl"
                             data-x="472"
                             data-y="253"
                             data-speed="300"
                             data-start="1600"
                             data-easing="easeOutExpo">
                            <img src="img/slider/responsive-iphone.png" alt="iPhone Responsive">
                        </div>
                    </li>-->
                </ul>
            </div>
        </div>



        <div class="content" style="background:#42a5f5; color:#fff; margin-bottom:50px;" >
            <div class="container" >
                <div class="row">
                    <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <article>
                            <h3 class="title" style="color:#fff;">Who We Are?</h3>
                            <div class="post-content">
                                <div class="img-content">
                                    <img src={{asset("client/img/team/team.jpg")}} alt="">
                                </div>
                                <p>
                                    Star Internet is Delhi fastest growing Internet Service Providers and the
                                    preferred choice for connectivity services for home & business, committed to enhancing the lives of its customers by providing world-class internet services.
                                </p>
                                <br>

                            </div>
                        </article>
                    </div>
                    <!-- Left Section End -->


                    <div class="clearfix"></div>
                </div>
                <div class="divider"></div>





            </div>
        </div>


        <!-- Recent works start-->
        <div class="bottom-pad">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 animate_afl d1">
                        <h3 class="title">Our Services</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="row portfolio-pad">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 item animate_afc d2">
                                <div class="portfolio-item">
                                    <a href={{asset("client/img/service1.jpg" )}}class="portfolio-item-link"
                                       data-rel="prettyPhoto" >
                                        <span class="portfolio-item-hover"></span><span class="fullscreen"><i
                                                    class="fa fa-search"></i></span><img src={{asset
                                                    ("client/img/service.jpg" )}}
                                                    alt=" "/>
                                    </a>
                                    <div class="portfolio-item-title">
                                        <a href="#">BROADBAND</a>
                                        <!--<p>
                                           Design / Development
                                        </p>-->
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 item animate_afc d3">
                                <div class="portfolio-item">
                                    <a href={{asset('client/img/service2.jpg')}} class="portfolio-item-link"
                                       data-rel="prettyPhoto" >
                                        <span class="portfolio-item-hover"></span><span class="fullscreen"><i
                                                    class="fa fa-search"></i></span><img src="client/img/service2.jpg"
                                                                                         alt=" "/>
                                    </a>
                                    <div class="portfolio-item-title">
                                        <a href="#">BEST SELLING PLANS</a>
                                        <!-- <p>
                                            Sound / Audio
                                         </p>-->
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 item animate_afc d4">
                                <div class="portfolio-item">
                                    <a href="img/service3.jpg" class="portfolio-item-link" data-rel="prettyPhoto" >
                                        <span class="portfolio-item-hover"></span><span class="fullscreen"><i
                                                    class="fa fa-search"></i></span><img src={{asset('client/img/service3.jpg')
                                                    }}
                                                    alt="img"/>
                                    </a>
                                    <div class="portfolio-item-title">
                                        <a href="#">WIFI ZONE</a>
                                        <!--<p>
                                           Apareal / Shoe
                                        </p>-->
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="row portfolio-pad">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 item animate_afc d2">
                                <div class="portfolio-item">
                                    <a href="img/service4.jpg" class="portfolio-item-link" data-rel="prettyPhoto" >
                                        <span class="portfolio-item-hover"></span><span class="fullscreen"><i
                                                    class="fa fa-search"></i></span><img src={{asset('client/img/service4.jpg')
                                                    }}
                                                    alt="IMG" />
                                    </a>
                                    <div class="portfolio-item-title">
                                        <a href="#">PAY BILLS (ALL USER)</a>
                                        <!--<p>
                                           Design / Development
                                        </p>-->
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 item animate_afc d3">
                                <div class="portfolio-item">
                                    <a href="img/service5.jpg" class="portfolio-item-link" data-rel="prettyPhoto" >
                                        <span class="portfolio-item-hover"></span><span class="fullscreen"><i
                                                    class="fa fa-search"></i></span><img src={{asset('client/img/service5.jpg')}}

                                        alt=" "/>
                                    </a>
                                    <div class="portfolio-item-title">
                                        <a href="#">NEW CONNECTION</a>
                                        <!-- <p>
                                            Sound / Audio
                                         </p>-->
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 item animate_afc d4">
                                <div class="portfolio-item">
                                    <a href="img/service6.jpg" class="portfolio-item-link" data-rel="prettyPhoto" >
                                        <span class="portfolio-item-hover"></span><span class="fullscreen"><i
                                                    class="fa fa-search"></i></span><img src={{asset('client/img/service6.jpg')
                                                    }}
                                                                                         alt=" "/>
                                    </a>
                                    <div class="portfolio-item-title">
                                        <a href="#">SECURE INTERNET</a>
                                        <!--<p>
                                           Apareal / Shoe
                                        </p>-->
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>








        <!-- Title, Breadcrumb End-->
        <!-- Main Content start-->
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h3 class="title">Our Pricing</h3>
                        <!-- pricing table -->
                        <div class="p_table">
                            <!-- column style 1 -->
                            <div class="column_1">
                                <ul>
                                    <!-- column header -->
                                    <li class="header_row_1 align_center">
                                        <h2 class="col1">Standard</h2>
                                    </li>
                                    <li class="header_row_2 align_center">
                                        <h1 class="col1"><span>500</span></h1>
                                        <h3 class="col1">per month</h3>
                                    </li>
                                    <!-- data rows -->
                                    <li class="row_style_3 align_center"><span>5 MBPS</span></li>
                                    <li class="row_style_1 align_center"><span>100GB Data</span></li>
                                    <li class="row_style_3 align_center"><span>1 Email Account</span></li>
                                    <li class="row_style_1 align_center"><span>1 MySQL Database</span></li>
                                    <li class="row_style_3 align_center"><span>24/7 Support</span></li>
                                    <li class="row_style_1 align_center"><span>99% Uptime Guarantee</span></li>
                                    <!-- column footer -->
                                    <li class="footer_row"><a href="#" class="sign_up btn-color btn-small">Sign up!</a></li>
                                </ul>
                            </div>
                            <!-- column style 2 -->
                            <div class="column_2">
                                <ul>
                                    <!-- column header -->
                                    <li class="header_row_1 align_center">
                                        <h2 class="col2">Premium</h2>
                                    </li>
                                    <li class="header_row_2 align_center">
                                        <h1 class="col2"><span>700</span></h1>
                                        <h3 class="col2">per month</h3>
                                    </li>
                                    <!-- data rows -->
                                    <li class="row_style_4 align_center"><span>20 MBPS Disk Space</span></li>
                                    <li class="row_style_2 align_center"><span>200GB Bandwidth</span></li>
                                    <li class="row_style_4 align_center"><span>10 Email Account</span></li>
                                    <li class="row_style_2 align_center"><span>10 MySQL Database</span></li>
                                    <li class="row_style_4 align_center"><span>24/7 Support</span></li>
                                    <li class="row_style_2 align_center"><span>99% Uptime Guarantee</span></li>
                                    <!-- column footer -->
                                    <li class="footer_row"><a href="#" class="sign_up btn-color btn-small">Sign up!</a></li>
                                </ul>
                            </div>
                            <!-- column style 3 -->
                            <div class="column_3">
                                <ul>
                                    <!-- column header -->
                                    <li class="header_row_1 align_center">
                                        <h2 class="col3">Professional</h2>
                                    </li>
                                    <li class="header_row_2 align_center">
                                        <h1 class="col3"><span>1000</span></h1>
                                        <h3 class="col3">per month</h3>
                                    </li>
                                    <!-- data rows -->
                                    <li class="row_style_3 align_center"><span>50 MBPS</span></li>
                                    <li class="row_style_1 align_center"><span>500GB Bandwidth</span></li>
                                    <li class="row_style_3 align_center"><span>50 Email Account</span></li>
                                    <li class="row_style_1 align_center"><span>50 MySQL Database</span></li>
                                    <li class="row_style_3 align_center"><span>24/7 Support</span></li>
                                    <li class="row_style_1 align_center"><span>99% Uptime Guarantee</span></li>
                                    <!-- column footer -->
                                    <li class="footer_row"><a href="#" class="sign_up btn-color btn-small">Sign up!</a></li>
                                </ul>
                            </div>
                            <!-- column style 4 -->
                            <div class="column_4">
                                <ul>
                                    <!-- column header -->
                                    <li class="header_row_1 align_center">
                                        <h2 class="col4">Ultimate</h2>
                                    </li>
                                    <li class="header_row_2 align_center">
                                        <h1 class="col4"><span>1500</span></h1>
                                        <h3 class="col4">per month</h3>
                                    </li>
                                    <!-- data rows -->
                                    <li class="row_style_4 align_center"><span>100 MBPS</span></li>
                                    <li class="row_style_2 align_center"><span>1000GB Bandwidth</span></li>
                                    <li class="row_style_4 align_center"><span>Unlimited Email</span></li>
                                    <li class="row_style_2 align_center"><span>Unlimited Database</span></li>
                                    <li class="row_style_4 align_center"><span>24/7 Support</span></li>
                                    <li class="row_style_2 align_center"><span>99% Uptime Guarantee</span></li>
                                    <!-- column footer -->
                                    <li class="footer_row"><a href="#" class="sign_up btn-color btn-small">Sign up!</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="divider"></div>
                <!-- 4 Column End -->

            </div>
        </div>
        <!-- Main Content end-->

    </div>
    <!-- Content End -->
@endsection
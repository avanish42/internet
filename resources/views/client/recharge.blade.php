@extends('client.app')
@section('title','Recharge')

@section('main-content')

    <!-- Content Start -->
    <div id="main">
        <!-- Title, Breadcrumb Start-->
        <div class="breadcrumb-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-xs-12 col-sm-6">
                        <h2 class="title">Quick Recharge Request</h2>
                    </div>
                    <div class="col-lg-6 col-md-6 col-xs-12 col-sm-6">
                        <div class="breadcrumbs pull-right">
                            <ul>
                                <li>You are here:</li>
                                <li><a href="index.html">Home</a></li>
                                <li>Quick Recharge Request</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Title, Breadcrumb End-->
        <!-- Main Content start-->
        <div class="content">
            <div class="container">
                <div class="row" >
                    <div class="col-md-8" id="contact-form" style="margin:auto!important; float:none!important; ">
                        <h3 class="title">Payment</h3>

                        <div class="divider"></div>
                        <form method="post" class="reply" id="contact" style="background:#fff; padding:20px;">
                            <fieldset>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <label>Customer ID </label>
                                        <input class="form-control" id="customer_id" placeholder="Enter Customer ID"  type="text" value="" required>

                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <label>Name </label>
                                        <input class="form-control" id="name" placeholder="Enter Name"  type="text" value="" required>
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <label>Phone No</label>
                                        <input class="form-control" id="phone_no" placeholder="Enter Phone No"  type="text" value="" required>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <label>Email ID </label>
                                        <input class="form-control" id="email" placeholder="Enter Email ID"  type="text" value="" required>
                                    </div>

                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <label>Due Amount </label>
                                        <input class="form-control" id="description" placeholder="Enter Due Amount"  type="text" value="" required>
                                    </div>





                                </div>






                            </fieldset>
                            <button class="btn btn-normal btn-color submit " type="submit">Pay</button>
                            <a href="index.html" style="color:#fff;"> <button class="btn btn-normal btn-danger " type="button">Back to Home</button></a>


                            <div class="clearfix">
                            </div>
                        </form>
                    </div>

                </div>

                <div class="divider"></div>
            </div>
        </div>
        <!-- Main Content end-->
    </div>
    <!-- Content End -->
@endsection


@extends('client.app')
@section('title','Abount')

@section('main-content')

    <!-- Content Start -->
    <div id="main">
        <!-- Title, Breadcrumb Start-->
        <div class="breadcrumb-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-xs-12 col-sm-6">
                        <h2 class="title">About Us</h2>
                    </div>
                    <div class="col-lg-6 col-md-6 col-xs-12 col-sm-6">
                        <div class="breadcrumbs pull-right">
                            <ul>
                                <li>You are here:</li>
                                <li><a href="index.html">Home</a></li>
                                <li>About Us</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Title, Breadcrumb End-->
        <!-- Main Content start-->
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <article>
                            <h3 class="title">Who We Are?</h3>
                            <div class="post-content">
                                <div class="img-content">
                                    {{--<img src="{{asset('client/img/team/team.jpg')}}" alt="">--}}
                                </div>
                                <p >
                                    Star Internet is Delhi fastest growing Internet Service Providers and the preferred choice for connectivity services for home & business, committed to enhancing the lives of its customers by providing world-class internet services.
                                </p>
                                <br>
                                <p >
                                    Our vision is to revolutionize broadband services in India by providing simple-to-understand yet state-of-the-art services that will enable people of our country to do so much more in their daily lives, thereby bringing them unprecedented advantage and joy, ultimately resulting in their success.
                                    We have served business and home consumers and have grown to deliver the most amazing internet experience today.
                                </p>


                                <br>
                                <p >
                                    Today, home consumers of Star Internet enjoy speed of 20 to 100 Mbps with genuine unlimited data usage that puts them among the top 1% of internet users. This makes us the network of choice for thousands of customers in Vasant Kunj Delhi.


                                </p>


                                <br>
                                <p>
                                    For our business users, our revolutionary and disruptive Business Internet Access (BIA) products and our Data Center Solutions offering metered power, deliver an unfair advantage of abundance enabling, them to do so much more within their budgets.
                                </p>

                            </div>
                        </article>
                    </div>
                    <!-- Left Section End -->
                    <!-- Skill Section Start -->

                    <!-- Skill Section Start -->
                    <div class="clearfix"></div>
                </div>
                <div class="divider"></div>





            </div>
        </div>
        <!-- Main Content end-->

    </div>
    <!-- Content End -->
@endsection
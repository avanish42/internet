@extends('client.app')
@section('title','Contect')

@section('main-content')

    <!-- Content Start -->
    <div id="main">
        <!-- Title, Breadcrumb Start-->
        <div class="breadcrumb-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-xs-12 col-sm-6">
                        <h2 class="title">Contact</h2>
                    </div>
                    <div class="col-lg-6 col-md-6 col-xs-12 col-sm-6">
                        <div class="breadcrumbs pull-right">
                            <ul>
                                <li>You are here:</li>
                                <li><a href="index.html">Home</a></li>
                                <li>Contact</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Title, Breadcrumb End-->
        <!-- Main Content start-->
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12" id="contact-form">
                        <h3 class="title">Send Us an Email</h3>
                        <p>
                            Feel free to talk to our online representative at any time you please using our Live Chat system on our website or one of the below instant messaging programs.
                        </p>
                        <p>
                            Please be patient while waiting for response. (24/7 Support!)
                        </p>
                        <div class="divider"></div>
                        <form method="post" class="reply" id="contact">
                            <fieldset>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <label>Name: <span>*</span></label>
                                        <input class="form-control" id="name" name="name" type="text" value="" required>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <label>Email: <span>*</span></label>
                                        <input class="form-control" type="email" id="email" name="email" value="" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <label>Subject: <span>*</span></label>
                                        <input class="form-control" id="subject" name="subject" type="text" value="" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <label>Message: <span>*</span></label>
                                        <textarea class="form-control" id="text" name="text" rows="3" cols="40" required></textarea>
                                    </div>
                                </div>
                            </fieldset>
                            <button class="btn btn-normal btn-color submit  bottom-pad disabled"  type="submit">Send</button>
                            <div class="success alert-success alert" style="display:none">Your message has been sent successfully.</div>
                            <div class="error alert-error alert" style="display:none">E-mail must be valid and message must be longer than 100 characters.</div>
                            <div class="clearfix">
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-4 col-md-4 col-xs-12 col-sm-6">
                        <div class="address widget">
                            <h3 class="title">Head Office</h3>


                            <ul class="contact-us">
                                <li>
                                    <i class="fa fa-map-marker"></i>
                                    <p>
                                        <strong class="contact-pad">Address:</strong> B-117, Vasant Kunj Enclave, <br>Vasant Kunj, New Delhi -110070
                                    </p>
                                </li>
                                <li>
                                    <i class="fa fa-phone"></i>
                                    <p><strong>Phone:</strong> +91 98731 35358, 99101 35358</p>
                                </li>
                                <li>
                                    <i class="fa fa-envelope"></i>
                                    <p><strong>Email:</strong><a href="mailto:starinternet12@gmail.com">starinternet12@gmail.com</a></p>
                                </li>
                            </ul>

                        </div>
                        <div class="contact-info widget">
                            <h3 class="title">Business Hour</h3>
                            <ul class="business-hour">
                                <li><i class="fa fa-clock-o"> </i>Monday - Friday 9am to 5pm </li>
                                <li><i class="fa fa-clock-o"> </i>Saturday - 9am to 2pm</li>
                                <li><i class="fa fa-times-circle-o"> </i>Sunday - Closed</li>
                            </ul>
                        </div>
                        <div class="follow widget">
                            <h3 class="title">Follow Us</h3>
                            <div class="member-social dark">
                                <a class="facebook" href="#"><i class="fa fa-facebook"></i></a>
                                <a class="twitter" href="#"><i class="fa fa-twitter"></i></a>
                                <a class="dribbble" href="#"><i class="fa fa-dribbble"></i></a>
                                <a class="gplus" href="#"><i class="fa fa-google-plus"></i></a>
                                <a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="divider"></div>
            </div>
        </div>
        <!-- Main Content end-->
    </div>
    <!-- Content End -->
@endsection


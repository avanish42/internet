<!DOCTYPE html>
<html>


@include('clientlayout.head')

<body class="home">
<div class="page-mask">
    {{--<div class="page-loader">--}}

        {{--<div class="spinner"></div>--}}
        {{--Loading...--}}
    {{--</div>--}}

</div>
<div class="wrap">
@include('clientlayout.header')


        @yield('main-content')

@include('clientlayout.footer')
    <a href="#" class="scrollup"><i class="fa fa-angle-up"></i></a>
</div>

@include('clientlayout.script')


</body>

<!-- Mirrored from demo.fifothemes.com/pixma/Flat/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 29 Aug 2018 11:11:49 GMT -->
</html>

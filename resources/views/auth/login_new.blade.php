<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>Internet Provider</title>
    {{--<meta name="description" content="Doodle is a Dashboard & Admin Site Responsive Template by hencework." />--}}
    {{--<meta name="keywords" content="admin, admin dashboard, admin template, cms, crm, Doodle Admin, Doodleadmin, premium admin templates, responsive admin, sass, panel, software, ui, visualization, web app, application" />--}}
    {{--<meta name="author" content="hencework"/>--}}


    <style>
        body
        {
            margin: 0;
            padding: 0;

        }
        #particles-js
        {
            height: 100%;
            width: 100%;
        }
    </style>

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- vector map CSS -->
    <link href= {{URL::asset("vendors/bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css")}}
          rel="stylesheet"
          type="text/css"/>



    <!-- Custom CSS -->
    <link href={{URL::asset("dist/css/style.css")}}  rel="stylesheet" type="text/css">
</head>
<body>
<!--Preloader-->
<div class="preloader-it">
    <div class="la-anim-1"></div>
</div>
<!--/Preloader-->

<div class="wrapper pa-0">
    <header class="sp-header">
        <div class="sp-logo-wrap pull-left">
            <a href="index.html">
                <img class="brand-img mr-10" src="dist/img/logo.png" alt="brand"/>
                <span class="brand-text">Star Internet</span>
            </a>
        </div>
        {{--<div class="form-group mb-0 pull-right">--}}
            {{--<span class="inline-block pr-10">Don't have an account?</span>--}}
            {{--<a class="inline-block btn btn-info btn-rounded btn-outline" href="signup.html">Sign Up</a>--}}
        {{--</div>--}}
        <div class="clearfix"></div>
    </header>

    <!-- Main Content -->
    <div class="page-wrapper pa-0 ma-0 auth-page">
        <div class="container-fluid">
            <!-- Row -->
            <div class="table-struct full-width full-height">
                <div class="table-cell vertical-align-middle auth-form-wrap">
                    <div class="auth-form  ml-auto mr-auto no-float">
                        <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                            @csrf
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="mb-30">
                                    <h4 class="text-center txt-dark mb-10">Login</h4>
                                    {{--<h6 class="text-center nonecase-font txt-grey">Enter your details below</h6>--}}
                                </div>
                                <div class="form-wrap">

                                        <div class="form-group">
                                            <label class="control-label mb-10" for="exampleInputEmail_2">Email address</label>
                                            <input type="email" name="email" class="form-control {{ $errors->has
                                            ('email') ? '
                                            is-invalid' : '' }}" required=""
                                                   value="{{ old('email') }}" id='email' placeholder="Enter email">

                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback" role="alert">
                                        <strong style="color: red">{{ $errors->first('email') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                    {{--<div class="form-group row">--}}
                                        {{--<label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>--}}

                                        {{--<div class="col-md-6">--}}
                                            {{--<input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>--}}

                                            {{--@if ($errors->has('email'))--}}
                                                {{--<span class="invalid-feedback" role="alert">--}}
                                        {{--<strong>{{ $errors->first('email') }}</strong>--}}
                                    {{--</span>--}}
                                            {{--@endif--}}
                                        {{--</div>--}}
                                    {{--</div>--}}



                                        <div class="form-group">
                                            <label class="pull-left control-label mb-10" for="exampleInputpwd_2">Password</label>
                                            {{--<a class="capitalize-font txt-primary block mb-10 pull-right font-12" href="forgot-password.html">forgot password ?</a>--}}
                                            <div class="clearfix"></div>
                                            <input type="password" class="form-control" name="password" required=""
                                                   id="password" placeholder="Enter pwd">

                                            @if ($errors->has('password'))
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                            @endif
                                        </div>


                                        <div class="form-group text-center">
                                            <button type="submit" class="btn btn-primary btn-rounded">sign in</button>
                                        </div>

                                </div>
                            </div>
                        </div>
                        </form>
                    </div>

                </div>
            </div>
            <!-- /Row -->
        </div>

    </div>
    <!-- /Main Content -->

    <div id="particles-js"></div>

</div>
<!-- /#wrapper -->

<!-- JavaScript -->

<!-- jQuery -->
<script src={{URL::asset("vendors/bower_components/jquery/dist/jquery.min.js")}} ></script>

<!-- Bootstrap Core JavaScript -->
<script src={{URL::asset("vendors/bower_components/bootstrap/dist/js/bootstrap.min.js")}} ></script>
<script src= {{URL::asset("vendors/bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js")}} ></script>

<!-- Slimscroll JavaScript -->
<script src={{URL::asset("dist/js/jquery.slimscroll.js")}} ></script>

<!-- Init JavaScript -->
<script src={{URL::asset("dist/js/init.js")}} ></script>
<script src={{URL::asset("js/particles.js")}} ></script>
<script src={{URL::asset("dist/js/app.js")}} ></script>

{{--<script src="dist/js/init.js"></script>--}}
</body>
</html>

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/','Admin\DashboardController@dashboard' )->middleware('auth');
//{
//    return view('auth.login_new');
//})->middleware('auth');
//
Route::namespace('Client')->group(function (){

    Route::get('about','MainController@aboutUs');
    Route::get('/','MainController@index');
    Route::get('contect','MainController@contactUs');
    Route::get('recharge','MainController@quickRecharge');
    Route::get('complain','MainController@complain');

});


Auth::routes();

Route::middleware('admin')->group(function(){


Route::get('/home', 'Admin\DashboardController@dashboard')->name('home');

Route::post('saveZone','Admin\ZoneController@store');
Route::get('zone','Admin\ZoneController@index');
Route::post('zone-edit-model','Admin\ZoneController@getModelDataZone');
Route::post('update-zone','Admin\ZoneController@updateZone');
Route::get('delete-zone/{id}','Admin\ZoneController@destroy');


//area Route

Route::get('area','Admin\AreaController@index');
Route::post('save-area','Admin\AreaController@saveArea');
Route::post('area-edit-model','Admin\AreaController@editArea');
Route::post('update-area','Admin\AreaController@updateArea');
Route::get('delete-area/{id}','Admin\AreaController@deleteAera');


//Plan route

Route::get('plan','Admin\PlanController@index');
Route::post('save-plan','Admin\PlanController@savePlan');

Route::post('plan-edit-model','Admin\PlanController@getModelDataPlan');

Route::post('update-plan','Admin\PlanController@updatePlan');
Route::get('delete-plan/{id}','Admin\PlanController@deletePlan');



//client route

Route::get('add-client','Admin\ClientController@addClient');
Route::post('add-client','Admin\ClientController@storeClient');
Route::post('ajax-area','Admin\ClientController@ajaxArea');
Route::get('all-clients','Admin\ClientController@allClient');

Route::get('edit-client/{id}','Admin\ClientController@editClient');
Route::post('update-client','Admin\ClientController@updateClient');

Route::get('client-bills','Admin\ClientController@clientBills');

Route::get('client-profile/{id}','Admin\ClientController@clientProfile');
Route::get('change-status/{id}','Admin\ClientController@changeStatus');

Route::get('filter-client','Admin\ClientController@filterClient');

//clent ajax call
Route::post('check-customer-id','Admin\ClientController@CheckCustomerId');
Route::post('check-card-id','Admin\ClientController@CheckCardtId');

Route::get('get-all-clients','Admin\ClientController@getAllClientForDatatable');


Route::get('sheet-upload','Admin\SheetUpload@showsheet');
Route::post('sheet-upload','Admin\SheetUpload@sheetUpload');
Route::post('update-data-by-sheet','Admin\SheetUpload@clientDataUpdateBySheet');
Route::post('update-area-zone','Admin\SheetUpload@areaUpdate');

});


//Dashboard
Route::namespace('Admin')->group(function (){


    Route::get('/dashboard','DashboardController@dashboard');
    Route::get('/star-5897100','DashboardController@index')->middleware('admin');


});


Route::get('sbill','Admin\PdfController@showBill');
Route::get('gbill','Admin\PdfController@generateBill');
Route::get('recept','Admin\PdfController@recept');

//Payemnt

Route::namespace('Admin')->middleware('admin')->group(function () {

        Route::get('payment','PaymentController@paymentPage');
        Route::post('payment','PaymentController@paymentProcess');
        Route::get('payment/response','PaymentController@responsePage');
        Route::get('payment-cancle','PaymentController@canclePage');

});

// generate bill controller
Route::namespace('Admin')->middleware('admin')->prefix('bill')->group(function (){

    Route::get('generate','GenerateBillController@generateBill');
    Route::post('generate','GenerateBillController@generateBills');
    Route::get('all-bill-list','GenerateBillController@allBillList');
    Route::post('manual-generate','GenerateBillController@manualGenerateBill');
    Route::get('current-generate-bills','GenerateBillController@currentGeneratedBills');
    Route::post('search-generate-bills','GenerateBillController@currentGeneratedBills');
    Route::post('cancle-generate','GenerateBillController@cancleGeneratedBills');

    Route::get('resend-bill/{id}','GenerateBillController@resendBill');

    Route::get('non-generated-bill','GenerateBillController@nonGeneratedBillByMonth');


});

Route::namespace('Admin')->middleware('admin')->prefix('exicutive')->group(function(){

    Route::get('add-exicutive','ExicutiveController@addExicutive');
    Route::post('add-exicutive','ExicutiveController@storeExicutive');
    Route::get('edit-exicutive','ExicutiveController@editExicutive');
    Route::get('delete-exicutive/{id}','ExicutiveController@deleteExicutive');
    Route::get('all-exicutive','ExicutiveController@allExicutive');
    Route::get('change-password/{id}','ExicutiveController@changePAssword');
    Route::post('update-password','ExicutiveController@updatePassword');
});


Route::namespace('Admin')->middleware('admin')->prefix('payment')->group(function(){

    Route::get('search-for-payment','PaymentController@payByWeb');
//    Route::post('process-payment','PaymentController@processPaymentByWeb');
    Route::post('payment-history','PaymentController@processPaymentByWeb');
    Route::get('recent-payment-history','PaymentController@recentPaymentHistory');
    Route::post('show-ledger','PaymentController@showLedger');
    Route::get('send-mail','PaymentController@sendMail');

    Route::get('pending-payment','PaymentController@pandingPayment');

    Route::post('approve-payment','PaymentController@approvedPayment');
    Route::get('cancle-payment/{id}','PaymentController@canclePayment');
    Route::get('payment-reminder','PaymentController@paymentReminder');
    Route::post('send-reminder','PaymentController@sendPaymentReminder');

});

Route::post('payment/process-payment','Admin\PaymentController@processPaymentByWeb');


Route::namespace('Admin')->middleware('admin')->prefix('expense')->group(function(){

    Route::get('all-expense','ExpenseController@index');
    Route::post('store-expense','ExpenseController@storeExpense');
    Route::get('delete-expenses/{id}','ExpenseController@deleteExpense');

});

Route::namespace('Admin')->middleware('admin')->prefix('reports')->group(function(){

    Route::get('user-reports','ReportsController@userReport');
    Route::post('user-reports','ReportsController@getUserReport');

    Route::get('billing-report','ReportsController@billingReport');
    Route::post('billing-report','ReportsController@generateBillingReport') ;

    Route::get('expence-report','ReportsController@expenceReport');
    Route::post('expence-report','ReportsController@generateExpenceReport');

    Route::get('payment-report','ReportsController@paymentReport');
    Route::post('payment-report','ReportsController@generatePaymentReport');





});

Route::namespace('SubAdmin')->prefix('exicutive')->group(function (){

            Route::get('search-for-payment','BillingController@searchingPayment');
            Route::get('recent-payment-history','BillingController@recenetPayment');
            Route::post('process-payment','BillingController@PaymentProcess');

});


Route::namespace('Admin')->middleware('admin')->prefix('notification')->group(function(){


    Route::get('select-user','NotificationController@selectUser');
    Route::post('search-client-by-area','NotificationController@filteredClientsByArea');
    Route::post('send-area-notifications','NotificationController@sendAreaNotification');





});

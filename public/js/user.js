$(document).ready(function () {

    if(status == 100){
        $.notify({
                title: '<strong>Success !! </strong>',
                message: message
            },
            {
                allow_dismiss: true,
                newest_on_top: true,
                timer: 1000,
                type: 'success',
                placement:{
                            from: 'top',
                        align: 'right'
    },
        animate: {
                 enter: 'animated fadeInDown',
                 exit: 'animated fadeOutUp'
        }

    });
}

if(status==200)

{
    $.notify({
            title: '<strong> Warning !! </strong>',
            message: message
        },
        {
            allow_dismiss: true,
            newest_on_top: true,
            timer: 1000,
            type: 'warning',
            placement:{
                from: 'top',
                align: 'right'
            },
            animate: {
                enter: 'animated fadeInDown',
                exit: 'animated fadeOutUp'
            }

        });
}


    if(status==400)

    {
        $.notify({
                title: '<strong> Danger !! </strong>',
                message: message
            },
            {
                allow_dismiss: true,
                newest_on_top: true,
                timer: 1000,
                type: 'danger',
                placement:{
                    from: 'top',
                    align: 'right'
                },
                animate: {
                    enter: 'animated fadeInDown',
                    exit: 'animated fadeOutUp'
                }

            });
    }






    $(".edit-zone").click(function () {
        // console.log('click on edit buttonn');

        var data = $.parseJSON($(this).attr('data-button'));

        // console.log(data);

        $.ajax({
            url: APP_URL + "/zone-edit-model",
            type: "POST",
            data: {
                id: data.id,
                _token: token
            },
            // cache: false,
            dataType: "json",
            success: function (data) {
                console.log(data)
                // $("#resultarea").text(data);
                $("#zone-id:text").val(data.id);
                $("#zone-name:text").val(data.name);
                $("#zone-code:text").val(data.code);
                $("#zone-description:text").val(data.description);
            }
        });
    });

    // dynamic zone select

    $('#zone-select').change(function () {

        $('#area-select').empty();
        var id = $(this).val();

        $.ajax({
            url: APP_URL + "/ajax-area",
            type: "POST",
            data: {
                id: id,
                _token: token
            },

            dataType: "json",

            success: function (data) {
               // console.log(data)
                // $("#resultarea").text(data);


                // console.log(data);
                if (data.length==0) {

                    $('#area-select').append(' <option >-- No area found in this zone --</option>');
                    // $('').addClass('disabeled')
                    $('#area-select').prop('disabled', true);





                }
                else {
                    $('#area-select').prop('disabled', false);
                    $('#area-select').append(' <option disabled selected value>-- Select Area --</option>');

                    $.each(data, function (i, item) {

                        $('#area-select').append($('<option>', {
                            value: item.id,
                            text: item.name
                        }))

                    });

                }






            }


        })

    });

    //Plan edit

    $(".edit-plan").click(function () {
        // console.log('click on edit buttonn');

        var data = $.parseJSON($(this).attr('data-button'));

        // console.log(data);

        $.ajax({
            url: APP_URL + "/plan-edit-model",
            type: "POST",
            data: {
                id: data.plan_id,
                _token: token
            },
            // cache: false,
            dataType: "json",
            success: function (data) {
                console.log(data)
                // $("#resultarea").text(data);
                $("#plan-id:text").val(data.id);
                $("#plan-name:text").val(data.name);
                $("#plan-price:text").val(data.price);
                $("#plan-validity:text").val(data.validity);
                $("#plan-remark:text").val(data.remark);
                $("#zone-description:text").val(data.description);
            }
        });
    });

    $(".edit-area").click(function () {
        // console.log('click on edit buttonn');

        var data = $.parseJSON($(this).attr('data-button'));

        // console.log(data);

        $.ajax({
            url: APP_URL + "/area-edit-model",
            type: "POST",
            data: {
                id: data.area_id,
                _token: token
            },
            // cache: false,
            dataType: "json",
            success: function (data) {
                console.log(data)
                // $("#resultarea").text(data);
                $("#area-id:text").val(data.id);
                $("#area-name:text").val(data.name);
                $("#area-code:text").val(data.code);
                $("#area-description:text").val(data.remark);
            }
        });
    });

    // show-ledger

    // $(".show-ledger").click(function (e) {
    //     //     // console.log('click on edit buttonn');
    //     //         e.preventDefault();
    //     //
    //     // });



});
$(document).ready(function () {

    $('#client-table-yajra').DataTable({
        processing: true,
        serverSide: true,
        ajax: APP_URL+'/get-all-clients',
        columns: [
            {data: 'name',name:"name"},
            {data: 'user_id',name:"user_id"},
            {data: 'mobile',name:'mobile'},
            {data: 'street',name:'street'},
            {data: 'Action',name:'Action'}
        ]
    });

})
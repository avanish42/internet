<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;

class CreateGenerateBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('generate_bills', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('client_id');
            $table->unsignedInteger('exicutive_id')->nullable();
            $table->string('bill_no');
            $table->string('bill_path')->nullable();
            $table->string('amount');
//            $table->string('');
            $table->string('Remark');

            $table->timestamp('bill_date')->nullable();
//

//            $table->timestamp('bill_date')->default(Carbon\Carbon::now()->getTimestamp();));

            $table->foreign('client_id')
                ->references('id')->on('clients')
                ->onDelete('cascade');

            $table->foreign('exicutive_id')
                ->references('id')->on('users')
                ->onDelete('set null');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('generate_bills');
    }


}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('zone_id')->nullable();
            $table->unsignedInteger('area_id')->nullable();
            $table->unsignedInteger('plan_id')->nullable();
            $table->string('name');
            $table->string('email')->nullable();
            $table->string('coustmer_type')->default('prepaid');
            $table->string('customer_id');
            $table->string('card_id')->nullable();
            $table->string('user_id');
            $table->string('mobile');
            $table->boolean('login_status')->default(1);
            $table->boolean('customer_status')->default(1);
            $table->string('password')->nullable();
            // date and basic profiles
            $table->timestamp('joining_date')->nullable();
            $table->timestamp('billing_date')->nullable();
            $table->boolean('is_pdf')->default(0);
            $table->boolean('auto_bill')->default(0);
            $table->boolean('is_gst')->default(0);
            $table->boolean('is_igst')->default(0);
            $table->boolean('is_gst_in_plan')->default(0);
            $table->float('discount')->nullable();
//            $table->float('opening_balance')->nullable();
            $table->float('installment_amount')->nullable();
            $table->string('gst_no')->nullable();
            $table->string('device_type')->nullable();
            $table->string('device_token')->nullable();
            $table->string('street')->nullable();
            $table->string('postal_code')->default('110070');
            $table->string('city')->default('Vasant Kunj');
            $table->string('state')->default('Delhi');



            $table->foreign('zone_id')
                ->references('id')->on('zones')
                ->onDelete('set null');
            $table->foreign('area_id')
                ->references('id')->on('areas')
                ->onDelete('set null');
            $table->foreign('plan_id')
                ->references('id')->on('plans')
                ->onDelete('set null');

          //  $table->timestamps();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }

}

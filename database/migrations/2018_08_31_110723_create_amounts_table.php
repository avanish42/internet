<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amounts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('client_id');
            $table->unsignedInteger('paybill_id')->nullable();
            $table->unsignedInteger('exicutive_id')->nullable();
            $table->unsignedInteger('generate_bill_id')->nullable();

            $table->timestamp('payment_date')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->string('remark')->nullable();
            $table->float('credit')->nullable();
            $table->float('debit')->nullable();


            $table->foreign('client_id')
                ->references('id')->on('clients')
                ->onDelete('cascade');

            $table->foreign('paybill_id')
                ->references('id')->on('pay_bills')
                ->onDelete('set null');

            $table->foreign('exicutive_id')
                ->references('id')->on('users')
                ->onDelete('set null');

            $table->foreign('generate_bill_id')
                ->references('id')->on('generate_bills')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('amounts');
    }
}
